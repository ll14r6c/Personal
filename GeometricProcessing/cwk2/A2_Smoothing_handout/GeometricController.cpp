///////////////////////////////////////////////////
//
//	Hamish Carr
//	February, 2019
//
//	------------------------
//	GeometricController.cpp
//	------------------------
//
///////////////////////////////////////////////////

#include "GeometricController.h"

// constructor
GeometricController::GeometricController(GeometricSurfaceDirectedEdge *Surface, GeometricWindow *Window)
	: surface(Surface), window(Window)
	{ // constructor
	QObject::connect(	window->smoothnessSlider,	SIGNAL(valueChanged(int)),
						this,						SLOT(smoothnessSliderChanged(int)));

	QObject::connect(	window->iterationsSlider,	SIGNAL(valueChanged(int)),
						this,						SLOT(iterationSliderChanged(int)));

	iterations = 1;
	lambda = 0;
	} // constructor

// slot for responding to slider changes
void GeometricController::smoothnessSliderChanged(int value)
	{ // smoothnessSliderChanged()
	lambda = value;
	surface->smooth(lambda, iterations);
	window->geometricWidget->updateGL();
	} // smoothnessSliderChanged()

// slot for responding to iterations slider changes
void GeometricController::iterationSliderChanged(int value)
{
		iterations = value;
		surface->smooth(lambda, iterations);
		window->geometricWidget->updateGL();
}
