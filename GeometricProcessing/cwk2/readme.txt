# University of Leeds 2018-2019
# COMP 5812M Assignment 2 Smoothing

To compile on the University Linux machines, you will need to do the following:

[userid@machine A2_Smoothing_handout]$ module add qt
[userid@machine A2_Smoothing_handout]$ qmake -project QT+=opengl LIBS+=-lglut LIBS+=-lGLU
[userid@machine A2_Smoothing_handout]$ qmake
[userid@machine A2_Smoothing_handout]$ make

You should see a compiler warning about an unused parameter, which can be ignored.

To execute the renderer:

[userid@machine A2_Smoothing_handout]$ ./A2_Smoothing_handout filename

For Example:
============
[userid@machine A2_Smoothing_handout]$ ./A2_Smoothing_handout ../diredgenormals/horse.diredgenormal
