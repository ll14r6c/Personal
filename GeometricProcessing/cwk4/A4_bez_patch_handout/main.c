#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
//	Ken Shoemake's ArcBall
#include "BallAux.h"
#include "Ball.h"

// V3 STRUCT
typedef struct v3 v3;
struct v3{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

v3 getNormal(v3 vec){
	v3 toReturn;
	// Calculate the length for normalisation
	GLfloat normalLength = sqrt(vec.x*vec.x+vec.y*vec.y+vec.z*vec.z);
	// Calculate the norm
	toReturn.x = vec.x/normalLength;
	toReturn.y = vec.y/normalLength;
	toReturn.z = vec.z/normalLength;
	return toReturn;
}

v3 getCross(GLfloat v1[3], GLfloat v2[3]){
	v3 toReturn = {	(v1[1]*v2[2])-(v2[1]*v1[2]),
									(v1[2]*v2[0])-(v2[2]*v1[0]),
									(v1[0]*v2[1])-(v2[0]*v1[1])};
	return toReturn;
}
// V3 STRUCT

#ifndef PI
#define PI 3.14159265358979
#endif

static GLfloat controlPoints[4][4][3] =
	{ // controlPoints
	-3.0,  3.0,  0.01,
	-1.0,  3.0,  0.01,
	 1.0,  3.0,  0.01,
	 3.0,  3.0,  0.01,

	-3.0,  1.0,  4.01,
	-1.0,  1.0,  4.01,
	 1.0,  1.0,  4.01,
	 3.0,  1.0,  4.01,

	-3.0, -1.0, -4.01,
	-1.0, -1.0, -4.01,
	 1.0, -1.0, -4.01,
	 3.0, -1.0, -4.01,

	-3.0, -3.0,  0.01,
	-1.0, -3.0,  0.01,
	 1.0, -3.0,  0.01,
	 3.0, -3.0,  0.01,
	}; // controlPoints

// width and height of window, plus initial value
int windowSize = 640;
int windowWidth, windowHeight;


#define MESHSIZE 30

// Array to store initial pass through values for each column for a given sizes
// eg 4x20 representing 4 control points and 20 MESHSIZE
GLfloat initialPassData[4][MESHSIZE+1][3];
// Array to store the vertex data for rendering
GLfloat renderData[MESHSIZE+1][MESHSIZE+1][3];
// Array to store the normal data for rendering
GLfloat normalData[MESHSIZE+1][MESHSIZE+1][3];



unsigned int * indicesArray;
int indicesArraySize;

// an arcball for rotation
BallData ball;

// which vertex is being actively manipulated
int activeVertex = 0;

// whether to show the reference planes
int showPlanes = 1;
// whether to show the control net
int showNet = 1;
// whether to show the control vertices
int showVertices = 1;
// whether to show the surface
int showBezier = 0;
// whether to show the sphere
int showSphere = 0;
// whether to use glutSpheres
int useGLUTSphere = 0;
// whether to use wireframe for glutSphere
int useWireframe = 1;
// stage1
int showCurves = 0;
// stage2
int showPoints = 0;
// stage3
int showWireframe = 0;
// stage4
int showFlatShadedSurface = 0;
// stage5
int showVertexShadedSurface = 0;

// initialize the interface
void initInterface();

// draw routine
void display();
// reshape routine
void reshape(int width, int height);
// mouse up / down routine
void mouse(int button, int state, int x, int y);
// mouse drag routine
void mouseDrag(int x, int y);
// key down
void keyDown(unsigned char key, int x, int y);
// special key down
void specialDown(int key, int x, int y);
// help text
void printHelp();

// lighting information
GLfloat ambient[] = {0.2, 0.2, 0.2, 1.0};
GLfloat position[] = {0.0, 0.3, -1.0, 0.0};
GLfloat mat_diffuse[] = {0.6, 0.6, 0.6, 1.0};
GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
GLfloat mat_shininess[] = {50.0};

// initialize GLUT
void initInterface()
	{ // initInterface()
	// set the window height and width
	windowWidth = windowSize;
	windowHeight = windowSize;
	// set the initial glut parameters
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Bezier Patch");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(mouseDrag);
	glutKeyboardFunc(keyDown);
	glutSpecialFunc(specialDown);
	glEnable(GL_DEPTH_TEST);

	// set the projection matrix to the identity
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5.0, 5.0, -5.0, 5.0, -5.0, 5.0);
	// set the modelview matrix to the identity
 	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// set up lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_POSITION, position);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	// rotate slightly to make grid visible
	glRotatef(7.0, 1.0, 1.0, 0.0);

	// initialize the arcball
	Ball_Init(&ball);
	Ball_Place(&ball, qOne, 0.75);


	printHelp();
	} //	end of initInterface()

// mouse down routine
void mouse(int button, int state, int x, int y)
	{ // mouse()
	// left button rotates
	if (button == GLUT_LEFT_BUTTON)
		{ // left button
		if (state == GLUT_DOWN)
			{ // left button down
			// convert the control input into the vector type used
			// by the arcball
			HVect vNow;
			vNow.x = (2.0 * x - glutGet(GLUT_WINDOW_WIDTH))/glutGet(GLUT_WINDOW_WIDTH);
			vNow.y = -(2.0 * y - glutGet(GLUT_WINDOW_HEIGHT))/glutGet(GLUT_WINDOW_HEIGHT);
			// update the arcball
			Ball_Mouse(&ball, vNow);
			Ball_BeginDrag(&ball);
			} // left button down
		else if (state == GLUT_UP)
			{ // left button up
			Ball_EndDrag(&ball);
			} // left button up
		} // left button
	// either way, force a redisplay
	glutPostRedisplay();
	} // mouse()

// mouse drag routine
void mouseDrag(int x, int y)
	{ // mouseDrag()
	// convert the control input into the vector type used
	// by the arcball
	HVect vNow;
	vNow.x = (2.0 * x - glutGet(GLUT_WINDOW_WIDTH))/glutGet(GLUT_WINDOW_WIDTH);
	vNow.y = -(2.0 * y - glutGet(GLUT_WINDOW_HEIGHT))/glutGet(GLUT_WINDOW_HEIGHT);
	// update the arcball
	Ball_Mouse(&ball, vNow);

	// and force a refresh to highlight it
	glutPostRedisplay();
	} // mouseDrag()

// window reshape routine
void reshape(int width, int height)
	{ // reshape()
	// set the projection matrix to a simple orthogonal projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5.0, 5.0, -5.0 * height/width, 5.0 * height/width, -5.0, 5.0);
	glViewport(0, 0, width, height);
	} // reshape()

// key down (for quit key)
void keyDown(unsigned char key, int x, int y)
	{ // keyDown()
	switch(key)
		{ // switch on key
		case '<':
			activeVertex = (activeVertex + 15) % 16;
			break;
		case '>':
			activeVertex = (activeVertex + 1) % 16;
			break;
		case '+':
			controlPoints[activeVertex/4][activeVertex%4][2] += 0.1;
			break;
		case '-':
			controlPoints[activeVertex/4][activeVertex%4][2] -= 0.1;
			break;
		case 'p':
			showPlanes = !showPlanes;
			break;
		case 'v':
			showVertices = !showVertices;
			break;
		case 'n':
			showNet = !showNet;
			break;
		case 'b':
			showBezier = !showBezier;
			break;
		case 's':
			showSphere = !showSphere;
			break;
		case 'g':
			useGLUTSphere = !useGLUTSphere;
			break;
		case 'w':
			useWireframe = !useWireframe;
			break;
		case '1':
			showCurves = !showCurves;
			break;
		case '2':
			showPoints = !showPoints;
			break;
		case '3':
			showWireframe = !showWireframe;
			break;
		case '4':
			showFlatShadedSurface = !showFlatShadedSurface;
			break;
		case '5':
			showVertexShadedSurface = !showVertexShadedSurface;
			break;
		// check for quit key
		case 'q': case 'Q':
			exit(0);
			break;
		} // switch on key
	// force a redisplay
	glutPostRedisplay();
	} // keyDown()

// special key down
void specialDown(int key, int x, int y)
	{ // specialDown()
	switch(key)
		{ // switch on key
		case GLUT_KEY_RIGHT:
			controlPoints[activeVertex/4][activeVertex%4][0] += 0.1;
			break;
		case GLUT_KEY_LEFT:
			controlPoints[activeVertex/4][activeVertex%4][0] -= 0.1;
			break;
		case GLUT_KEY_UP:
			controlPoints[activeVertex/4][activeVertex%4][1] += 0.1;
			break;
		case GLUT_KEY_DOWN:
			controlPoints[activeVertex/4][activeVertex%4][1] -= 0.1;
			break;
		} // switch on key
	// force a redisplay
	glutPostRedisplay();
	} // specialDown()

// t  = Time in the curve within range 0-1
// p1 = {x,y,z} start control point
// p2 = {x,y,z} control point
// p3 = {x,y,z} control point
// p4 = {x,y,z} end control point
v3 getPointAt(GLfloat t, GLfloat p1[3], GLfloat p2[3], GLfloat p3[3], GLfloat p4[3]) {
    GLfloat x = pow(1-t, 3) * p1[0] + 3 * t * pow(1-t, 2) * p2[0] + 3 * pow(t, 2) * (1-t) * p3[0] + pow(t, 3) * p4[0];
    GLfloat y = pow(1-t, 3) * p1[1] + 3 * t * pow(1-t, 2) * p2[1] + 3 * pow(t, 2) * (1-t) * p3[1] + pow(t, 3) * p4[1];
    GLfloat z = pow(1-t, 3) * p1[2] + 3 * t * pow(1-t, 2) * p2[2] + 3 * pow(t, 2) * (1-t) * p3[2] + pow(t, 3) * p4[2];
		v3 calculatedPoint = {x, y, z};
    return calculatedPoint;
}

void drawControlNet(){
	glColor3f(0.0,1.0,0.0);
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		for(int i = 0; i < 4-1 ; i++){
			glBegin(GL_QUAD_STRIP);
			for(int j = 0; j < 4 ; j++){
				glVertex3f(controlPoints[i][j][0],controlPoints[i][j][1],controlPoints[i][j][2]);
				glVertex3f(controlPoints[i+1][j][0],controlPoints[i+1][j][1],controlPoints[i+1][j][2]);
			}
			glEnd();
		}
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void drawCurves(){
	// Set colour to red for the curves
	glColor3f(1.0,0.0,0.0);
	// Loop through each column of control points
	for(int controlPointIndex = 0; controlPointIndex < 4; controlPointIndex++){
		// Draw the curve for that given control points column
		glBegin(GL_LINE_STRIP);
		for(int s = 0; s <= MESHSIZE; s++){
			double normS = (double)s/(double)MESHSIZE;
			v3 point = getPointAt(normS, controlPoints[0][controlPointIndex], controlPoints[1][controlPointIndex], controlPoints[2][controlPointIndex], controlPoints[3][controlPointIndex]);
			glVertex3f(point.x, point.y, point.z);
		}
		glEnd();
	}
}

void drawPoints(){
	// Set colour to red for the curves
	glColor3f(1.0,0.0,1.0);
	glPointSize(2);
	glBegin(GL_POINTS);
	// Loop through s,t
	for(int s = 0; s <= MESHSIZE; s++){
		for(int t = 0; t <= MESHSIZE; t++){
			// Calculate the interpolated s,t between 0 and 1
			double normS = (double)s/(double)MESHSIZE;
			double normT = (double)t/(double)MESHSIZE;

			// Loop through control points calculating new control points for  other direction
			for(int controlPointIndex = 0; controlPointIndex < 4; controlPointIndex++){
				v3 point = getPointAt(normS, controlPoints[0][controlPointIndex], controlPoints[1][controlPointIndex], controlPoints[2][controlPointIndex], controlPoints[3][controlPointIndex]);
				initialPassData[controlPointIndex][s][0] = point.x;
				initialPassData[controlPointIndex][s][1] = point.y;
				initialPassData[controlPointIndex][s][2] = point.z;
			}

			// Calculate the interpolated point in s,t
			v3 point = getPointAt(normT, initialPassData[0][s], initialPassData[1][s], initialPassData[2][s], initialPassData[3][s]);
			glVertex3f(point.x, point.y, point.z);
		}
	}
	glEnd();
}

// t  = Time in the curve within range 0-1
// p1 = {x,y,z} start control point
// p2 = {x,y,z} control point
// p3 = {x,y,z} control point
// p4 = {x,y,z} end control point
v3 getDerivativeAt(GLfloat t, GLfloat p1[3], GLfloat p2[3], GLfloat p3[3], GLfloat p4[3]) {
    GLfloat x = (3*pow(1-t, 2)*(p2[0]-p1[0])) + (6*(1-t)*t*(p3[0]-p2[0])) + (3*pow(t, 2)*(p4[0]-p3[0]));
    GLfloat y = (3*pow(1-t, 2)*(p2[1]-p1[1])) + (6*(1-t)*t*(p3[1]-p2[1])) + (3*pow(t, 2)*(p4[1]-p3[1]));
    GLfloat z = (3*pow(1-t, 2)*(p2[2]-p1[2])) + (6*(1-t)*t*(p3[2]-p2[2])) + (3*pow(t, 2)*(p4[2]-p3[2]));
		v3 calculatedPoint = {x, y, z};
    return calculatedPoint;
}

// t  = Time in the curve within range 0-1
// p1 = {x,y,z} start control point
// p2 = {x,y,z} control point
// p3 = {x,y,z} control point
// p4 = {x,y,z} end control point
v3 getSecondDerivativeAt(GLfloat t, GLfloat p1[3], GLfloat p2[3], GLfloat p3[3], GLfloat p4[3]) {
    GLfloat x = (6*(1-t)*(p3[0]-p2[0]+p1[0])) + (6*t*(p4[0]-(2*p3[0])+p2[0]));
    GLfloat y = (6*(1-t)*(p3[1]-p2[1]+p1[1])) + (6*t*(p4[1]-(2*p3[1])+p2[1]));
    GLfloat z = (6*(1-t)*(p3[2]-p2[2]+p1[2])) + (6*t*(p4[2]-(2*p3[2])+p2[2]));
		v3 calculatedPoint = {x, y, z};
    return calculatedPoint;
}

// Use code similar to the draw points method to get points across the bezier surface
// Use the points as vertices, and save these to a render array
void generateMeshData(){

	// Set colour to red for the curves
	glColor3f(1.0,0.0,1.0);
	glPointSize(2);
	glBegin(GL_POINTS);
	// Loop through s,t
	for(int s = 0; s <= MESHSIZE; s++){
		for(int t = 0; t <= MESHSIZE; t++){
			// Calculate the interpolated s,t between 0 and 1
			double normS = (double)s/(double)MESHSIZE;
			double normT = (double)t/(double)MESHSIZE;

			// Loop through control points calculating new control points for  other direction
			for(int controlPointIndex = 0; controlPointIndex < 4; controlPointIndex++){
				v3 point = getPointAt(normS, controlPoints[0][controlPointIndex], controlPoints[1][controlPointIndex], controlPoints[2][controlPointIndex], controlPoints[3][controlPointIndex]);
				initialPassData[controlPointIndex][s][0] = point.x;
				initialPassData[controlPointIndex][s][1] = point.y;
				initialPassData[controlPointIndex][s][2] = point.z;
			}

			// Calculate the interpolated point in s,t
			v3 point = getPointAt(normT, initialPassData[0][s], initialPassData[1][s], initialPassData[2][s], initialPassData[3][s]);
			renderData[s][t][0] = point.x;
			renderData[s][t][1] = point.y;
			renderData[s][t][2] = point.z;

			// Loop through horizonal control points and save to array
			GLfloat horizontalControls[4][3];
			for(int i = 0; i < 4; i++){
				v3 curvePoint = getPointAt(normT, controlPoints[0][i], controlPoints[1][i], controlPoints[2][i], controlPoints[3][i]);
				horizontalControls[i][0] = curvePoint.x;
				horizontalControls[i][1] = curvePoint.y;
				horizontalControls[i][2] = curvePoint.z;
			}

			// Loop through vertical control points and save to array
			GLfloat verticalControls[4][3];
			for(int i = 0; i < 4; i++){
				v3 curvePoint = getPointAt(normS, controlPoints[i][0], controlPoints[i][1], controlPoints[i][2], controlPoints[i][3]);
				verticalControls[i][0] = curvePoint.x;
				verticalControls[i][1] = curvePoint.y;
				verticalControls[i][2] = curvePoint.z;
			}

			// Get the derivatives at normS and normT
			v3 ds = getDerivativeAt(normT, horizontalControls[0], horizontalControls[1], horizontalControls[2], horizontalControls[3]);
			v3 dt = getDerivativeAt(normS, verticalControls[0]  , verticalControls[1]  , verticalControls[2]  , verticalControls[3]);

			// Cross product derivitves to get an unormalised normal
			v3 unormal;
			unormal.x = (dt.y*ds.z) - (ds.y*dt.z);
			unormal.y = (dt.z*ds.x) - (ds.z*dt.x);
			unormal.z = (dt.x*ds.y) - (ds.x*dt.y);

			// Calculate length to normalise the normal
			GLfloat nLength = sqrt(unormal.x*unormal.x + unormal.y*unormal.y + unormal.z*unormal.z);

			// Set the normal to the normalised values
			normalData[s][t][0] = unormal.x/nLength;
			normalData[s][t][1] = unormal.y/nLength;
			normalData[s][t][2] = -unormal.z/nLength;
		}
	}
	glEnd();
}

void drawWireFrame(){
	generateMeshData();
	glColor3f(0.0,0.0,1.0);
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		for(int i = 0; i < MESHSIZE ; i++){
			glBegin(GL_QUAD_STRIP);
			for(int j = 0; j <= MESHSIZE ; j++){
				glVertex3f(renderData[i][j][0],renderData[i][j][1],renderData[i][j][2]);
				glVertex3f(renderData[i+1][j][0],renderData[i+1][j][1],renderData[i+1][j][2]);
			}
			glEnd();
		}
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

// Calculate the flat normal by using the cross product of horizontal/vertical vectors
v3 calcFlatNormal(GLfloat v0[3], GLfloat v1[3], GLfloat v2[3]){

	GLfloat v1subv0[3] = {v1[0]-v0[0], v1[1]-v0[1], v1[2]-v0[2]};
	GLfloat v2subv0[3] = {v2[0]-v0[0], v2[1]-v0[1], v2[2]-v0[2]};
	// Calc the cross
	v3 faceNormal = getCross(v1subv0, v2subv0);
	// Calculate the norm
	v3 normalised = getNormal(faceNormal);

	return normalised;
}

void drawSurfaceFlat(){
	generateMeshData();
	glBegin(GL_TRIANGLES);
	for(int i = 0; i < MESHSIZE ; i++){
		for(int j = 0; j < MESHSIZE ; j++){
			// Traingle 1
			GLfloat v0_1[3] = {renderData[i][j][0],renderData[i][j][1],renderData[i][j][2]};
			GLfloat v1_1[3] = {renderData[i+1][j][0],renderData[i+1][j][1],renderData[i+1][j][2]};
			GLfloat v2_1[3] = {renderData[i][j+1][0],renderData[i][j+1][1],renderData[i][j+1][2]};
			v3 faceNormal = calcFlatNormal(v0_1, v1_1, v2_1);
			glNormal3f(faceNormal.x,faceNormal.y,-faceNormal.z);
			// Render vertex
			glVertex3f(v0_1[0],v0_1[1],v0_1[2]);
			glVertex3f(v1_1[0],v1_1[1],v1_1[2]);
			glVertex3f(v2_1[0],v2_1[1],v2_1[2]);

			// Triangle 2
			GLfloat v0_2[3] = {renderData[i+1][j][0],renderData[i+1][j][1],renderData[i+1][j][2]};
			GLfloat v1_2[3] = {renderData[i+1][j+1][0],renderData[i+1][j+1][1],renderData[i+1][j+1][2]};
			GLfloat v2_2[3] = {renderData[i][j+1][0],renderData[i][j+1][1],renderData[i][j+1][2]};
			v3 faceNormal_2 = calcFlatNormal(v0_2, v1_2, v2_2);
			glNormal3f(faceNormal_2.x,faceNormal_2.y,-faceNormal_2.z);
			// Render vertex
			glVertex3f(v0_2[0],v0_2[1],v0_2[2]);
			glVertex3f(v1_2[0],v1_2[1],v1_2[2]);
			glVertex3f(v2_2[0],v2_2[1],v2_2[2]);
		}
	}
	glEnd();
}

void drawSurfaceSmooth(){
	generateMeshData();
	for(int i = 0; i < MESHSIZE ; i++){
		glBegin(GL_TRIANGLE_STRIP);
		for(int j = 0; j <= MESHSIZE ; j++){
			glNormal3f(normalData[i][j][0],normalData[i][j][1],normalData[i][j][2]);
			glVertex3f(renderData[i][j][0],renderData[i][j][1],renderData[i][j][2]);
			glNormal3f(normalData[i+1][j][0],normalData[i+1][j][1],normalData[i+1][j][2]);
			glVertex3f(renderData[i+1][j][0],renderData[i+1][j][1],renderData[i+1][j][2]);
		}
		glEnd();
	}
}

// display routine
void display()
	{ // display()
	int i;
	// set the clear colour and clear the frame buffer
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set the modelview
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	// arc ball rotation specified next
	GLfloat mNow[16];
	Ball_Update(&ball);
	Ball_Value(&ball, mNow);
	glMultMatrixf(mNow);

	glDisable(GL_LIGHTING);

	if (showPlanes)
		{ // showPlanes
		// set the drawing colour
		glColor3f(0.25, 0.0, 0.25);

		// draw a ground plane
		glBegin(GL_LINES);
		for (i = -5; i <= 5; i+=2)
			{
			glVertex3f(i, 0, -5);
			glVertex3f(i, 0, 5);
			glVertex3f(-5, 0, i);
			glVertex3f(5, 0, i);
			}
		glEnd();

		glColor3f(0.0, 0.25, 0.25);
		glBegin(GL_LINES);
		for (i = -5; i <= 5; i+=2)
			{
			glVertex3f(0, i, -5);
			glVertex3f(0, i, 5);
			glVertex3f(0, -5, i);
			glVertex3f(0, 5, i);
			}
		glEnd();

		glColor3f(0.25, 0.25, 0.0);
		glBegin(GL_LINES);
		for (i = -5; i <= 5; i++)
			{
			glVertex3f(-5, i, 0);
			glVertex3f(5, i, 0);
			glVertex3f(i, -5, 0);
			glVertex3f(i, 5, 0);
			}
		glEnd();
		} // showPlanes

	// now render a sphere
	glColor3f(0.3, 0.3, 1.25);
	if (showSphere){
		if (useGLUTSphere)
		{
			if (useWireframe){
				glutWireSphere(3.0, 120, 120);
			}
			else
				{ // solid
				glEnable(GL_LIGHTING);
				glutSolidSphere(3.0, 120, 120);
				glDisable(GL_LIGHTING);
				} // solid
		}
		else
			{ // points
			glBegin(GL_POINTS);
			for (float phi = 0.0; phi < PI/2.0; phi += PI / 1000.0)
			for (float theta = 0.0; theta < PI/2.0; theta += PI / 1000.0)
				glVertex3f(3.0 * cos(phi) * cos(theta), 3.0 * cos(phi) * sin(theta), 3.0 * sin(phi));
			glEnd();
			} // points
		}
	// now render the control vertices
	if (showVertices)
		{ //  showVertices
		glMatrixMode(GL_MODELVIEW);
		// for each control vertex
		for (int id = 0; id < 16; id++)
			{ // for id
			if (id == activeVertex)
				glColor3f(1.0, 0.0, 0.0);
			else
				glColor3f(0.75, 0.75, 0.75);
			glPushMatrix();
			glTranslatef(controlPoints[id/4][id%4][0],
							controlPoints[id/4][id%4][1],
							controlPoints[id/4][id%4][2]);
			glutSolidSphere(0.125, 12, 12);
			glPopMatrix();
			} // for id
		} // showVertices

	if (showNet)
		{ // showNet
			// now draw control net
			drawControlNet();
		} // showNet

	if (showBezier)
		{ // showBezier
			glShadeModel(GL_SMOOTH);
			glEnable(GL_LIGHTING);
			drawSurfaceSmooth();
			glDisable(GL_LIGHTING);
		} // showBezier

// STAGE 1
	if(showCurves){
		drawCurves();
	}

// STAGE 2
	if(showPoints){
		drawPoints();
	}

// STAGE 3
	if(showWireframe){
		drawWireFrame();
	}

// STAGE 4
	if(showFlatShadedSurface){
		glShadeModel(GL_FLAT);
		glEnable(GL_LIGHTING);
		drawSurfaceFlat();
		glDisable(GL_LIGHTING);
	}

// STAGE 5
	if(showVertexShadedSurface){
		glShadeModel(GL_SMOOTH);
		glEnable(GL_LIGHTING);
		drawSurfaceSmooth();
		glDisable(GL_LIGHTING);
	}

	// make sure we pop our matrix
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// swap the buffers
	glutSwapBuffers();
	} // display()


//	main()
int main(int argc, char **argv)
	{ // main()
	// initialize GLUT
	glutInit(&argc, argv);
	// set initial values
	initInterface();
	// start the event loop
	glutMainLoop();
	// and return when done
	return 0;
	} //	end of main()

void printHelp()
	{ // printHelp()
	printf("<     Previous control point\n");
	printf(">     Next control point\n");
	printf("Left  Decrement control point's x coordinate\n");
	printf("Right Increment control point's x coordinate\n");
	printf("Up    Increment control point's y coordinate\n");
	printf("Down  Decrement control point's y coordinate\n");
	printf("+     Increment control point's z coordinate\n");
	printf("-     Decrement control point's z coordinate\n");
	printf("p     Toggle reference planes\n");
	printf("v     Toggle control vertices\n");
	printf("n     Toggle control net\n");
	printf("b     Toggle bezier patch\n");
	printf("s     Toggle sphere\n");
	printf("1     Task 1\n");
	printf("2     Task 2\n");
	printf("3     Task 3\n");
	printf("4     Task 4\n");
	printf("5     Task 5\n");
	printf("q     Quit\n");
	} // printHelp()
