# University of Leeds 2018-2019
# COMP 5812M Assignment 4 Bezier Patches

To compile on the University Linux machines, you will need to do the following:

[userid@machine A4_bez_patch_handout]$ module add qt
[userid@machine A4_bez_patch_handout]$ qmake -project QT+=opengl LIBS+=-lglut LIBS+=-lGLU QMAKE_CFLAGS+=-std=c99
[userid@machine A4_bez_patch_handout]$ qmake
[userid@machine A4_bez_patch_handout]$ make

You should see a compiler warning about an unused parameters, which can be ignored.

To execute the bezier renderer:

[userid@machine A4_bez_patch_handout]$ ./A4_bez_patch_handout

Program Functionality:
============
For each task through 1-5, keys 1-5 can be used to toggle that task.
  For example pressing 1 should toggle task 1.
Size of patch can be changed within define of line 68 of 'main.c'
