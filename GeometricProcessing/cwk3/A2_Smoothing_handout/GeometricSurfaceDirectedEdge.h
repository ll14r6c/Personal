///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	GeometricSurfaceDirectedEdge.h
//	------------------------
//
//	Base code for geometric assignments.
//
//	This is the minimalistic Face-based D/S for storing
//	surfaces, to be used as the basis for fuller versions
//
//	It will include object load / save code & render code
//
///////////////////////////////////////////////////

#ifndef _GEOMETRIC_SURFACE_DIRECTED_EDGE_H
#define _GEOMETRIC_SURFACE_DIRECTED_EDGE_H

#include <vector>
#include <queue>

// an index type - signed so we can use negative numbers as flags
typedef signed long indexType;

// define a macro for "not used" flag
#define NO_SUCH_ELEMENT -1

#include "Cartesian3.h"

// Data structure to be used within the priority queue
struct vertexQueue{
	indexType vertex;
	int neighbourhoodSize;

	vertexQueue(indexType vertex, int neighbourhoodSize)
		: vertex(vertex), neighbourhoodSize(neighbourhoodSize) {}
};

// Front of queue will be filled with largest neighbourhoods
struct CompareError {
    bool operator()(vertexQueue const& v1, vertexQueue const& v2)
    {
        return v1.neighbourhoodSize < v2.neighbourhoodSize;
    }
};


class GeometricSurfaceDirectedEdge
	{ // class GeometricSurfaceDirectedEdge
	public:
	// the vertex positions
	std::vector<Cartesian3> position;
	// the "first" directed edge for each vertex
	std::vector<indexType> firstDirectedEdge;
	// the face vertices - doubles as the "to" array for edges
	std::vector<indexType> faceVertices;
	// the other half of the directed edges
	std::vector<indexType> otherHalf;

	// array to hold the normals
	std::vector<Cartesian3> normal;

	// bounding sphere size
	float boundingSphereSize;

	int origionalSize, currentVSize, currentESize, currentFSize;

	// midpoint of object
	Cartesian3 midPoint;

	// constructor will initialise to safe values
	GeometricSurfaceDirectedEdge();

	// read routine returns true on success, failure otherwise
	// does *NOT* check consistency
	bool ReadFileDirEdge(char *fileName);

	// write routine
	bool WriteFileDirEdge(char *fileName);

	// routine to render
	void Render();

	// debug routine to dump arrays
	void DumpArrays();

	// Routine to decimate
	void applyDecimation(int value);

	// Removes a given vertex
	void halfEdgeCollapse(indexType heToRemove);

	// Methods to retrieve the next or previous edges
	int next(int value);
	int previous(int value);

	// Method to return the average of a vector of Cartesian3
	Cartesian3 average(std::vector<Cartesian3> list);
	Cartesian3 total(std::vector<Cartesian3> list);

	void fillQueue();
	std::priority_queue<vertexQueue, std::vector<vertexQueue>, CompareError> priorityQueue;

	bool checkManifold();
	int getGenus();

	int origionalGenus;

	}; // class GeometricSurfaceDirectedEdge

#endif
