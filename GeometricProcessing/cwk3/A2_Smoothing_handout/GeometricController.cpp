///////////////////////////////////////////////////
//
//	Hamish Carr
//	February, 2019
//
//	------------------------
//	GeometricController.cpp
//	------------------------
//
///////////////////////////////////////////////////

#include "GeometricController.h"

// constructor
GeometricController::GeometricController(GeometricSurfaceDirectedEdge *Surface, GeometricWindow *Window)
	: surface(Surface), window(Window)
	{ // constructor
	QObject::connect(	window->simplificationSlider,	SIGNAL(valueChanged(int)),
						this,						SLOT(simplificationSliderChanged(int)));
	} // constructor

// slot for responding to slider changes
void GeometricController::simplificationSliderChanged(int value)
	{ // smoothnessSliderChanged()
			surface->applyDecimation(value);
			window->geometricWidget->updateGL();
	} // smoothnessSliderChanged()
