# University of Leeds 2018-2019
# COMP 5812M Assignment 1 Tree Renderer

To compile on the University Linux machines, you will need to do the following:

[userid@machine handout]$ module add qt
[userid@machine handout]$ qmake -project QT+=opengl LIBS+=-lglut LIBS+=-lGLU
[userid@machine handout]$ qmake
[userid@machine handout]$ make

You should see a compiler warning about an unused parameter, which can be ignored.

To execute the renderer:

[userid@machine handout]$ ./handout


IMPLEMENTATION DETAILS
======================
I choose to implement the following tasks:
  1. D. Implement a recursive function to render a tree with an L-system
  2. G. Implement shadows on a ground plane

  I also added a very simple option to randomize the angle for the l-system

IMPLEMENTATION LOCATIONS
========================

L-System
- "Lsystem" .cpp .h for implementation of the lsystem
- "RenderWidget.cpp" line 81 for the rules supplied for the given lsystem
- "RenderWidget.cpp" line 192 for the render call to the lsystem

Shadow
- "RenderWidget.cpp" line 38 for the function to generate the 4x4 shadow matrix
- "RenderWidget.cpp" line 110 for the call to generate the above function
- "RenderWidget.cpp" line 181 for the render pass for the shadow

Simple Randomness
- "RenderWidget.cpp" uncomment line 179 for the simple randomness every frame
