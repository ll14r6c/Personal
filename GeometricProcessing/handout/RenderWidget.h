///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2019
//
//	------------------------
//	RenderWidget.h
//	------------------------
//
//	The main widget that shows the geometry
//
///////////////////////////////////////////////////

#ifndef _RENDER_WIDGET_H
#define _RENDER_WIDGET_H

#include <QGLWidget>
#include <QMouseEvent>
#include "Ball.h"

#include "LSystem.h"

class RenderWidget : public QGLWidget
	{ // class RenderWidget
	Q_OBJECT
	public:
	// arcball for storing light rotation
	BallData lightBall;

	// arcball for storing object rotation
	BallData objectBall;

	// translation in window x,y
	GLfloat translate_x, translate_y;
	GLfloat last_x, last_y;

	// which button was last pressed
	int whichButton;

	// Lsystem information stored in this class
	LSystem myLSystem;

	// constructor
	RenderWidget(QWidget *parent);

	// destructor
	~RenderWidget();

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	void renderLsystem();

	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);

	}; // class RenderWidget

#endif
