#include "LSystem.h"

LSystem::LSystem(std::string axiom) : axiom(axiom), currentString(axiom)
{
	// Set default angle to 25
	angle = 25.0;
	// Set deafult to not be random
	random = false;
}

LSystem::LSystem()
{
}

LSystem::~LSystem()
{
}

void LSystem::generate(int givenGenerations)
{
	// Set the starting string to the stored axiom
	currentString = axiom;

	generations = givenGenerations;

	float length = 1;

	// Default to a black color
	set_material(black);

	// Loop through the given amount of iterations
	for (int i = 0; i <= generations; i++) {
		length = length * 0.5;
		std::string nextString = "";

		// Loop through each letter in the current string
		for (char &x : currentString) {
			bool found = false;


			// Loop through each rule in the stores rules
			// If a rule is true, add to next string
			for (LSRule &rule : rules) {
				if (std::string(1, x) == rule.from) {
					found = true;
					nextString += rule.to;
					break;
				}
			}
			// Otherwise add the single letter to next string
			if (!found) {
				nextString += x;
			}
		}
		// Store the result into the current string
		currentString = nextString;
		renderLsystem(length, i);
	}
}

// Takes two inputs, s1 (a single letter) and s2(any length)
// Adds them as rules to the Lsystem
void LSystem::addRule(std::string from, std::string to)
{
	LSRule tempRule;
	tempRule.from = from;
	tempRule.to = to;
	rules.push_back(tempRule);
}

void LSystem::renderLsystem(float length, int generation)
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	// Lsystem render code
	for(char &c : currentString){
		if(c == 'F'){
			drawBranch(length, generation);
			glTranslatef(0.0, length, 0.0);
		}else if(c == '['){
			glPushMatrix();
		}else if(c == ']'){
			glPopMatrix();
		}else if(c == '+'){ // roll
			glRotatef(-angle , 0.0, 0.0, 1.0);
		}else if(c == '-'){ // roll
			glRotatef(angle , 0.0, 0.0, 1.0);
		}else if(c == '/'){ // yaw
			glRotatef(-angle , 0.0, 1.0, 0.0);
		}else if(c == '\\'){ // yaw
			glRotatef(angle , 0.0, 1.0, 0.0);
		}else if(c == '&'){ // pitch
			glRotatef(-angle , 1.0, 0.0, 0.0);
		}else if(c == '^'){ // pitch
			glRotatef(angle , 1.0, 0.0, 0.0);
		}else if(generation == generations){
			drawLeaf();
		}
	}
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void LSystem::drawBranch(float length, int generation)
{
	glPushMatrix();
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	GLUquadric *quad;
	quad = gluNewQuadric();
	if(!isShadow)set_material(brown);
	gluCylinder( quad, 0.05*length, 0.05*length, length, 20, 20);
	gluDeleteQuadric(quad);
	glPopMatrix();
}

void LSystem::drawLeaf()
{
	glPushMatrix();
		glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 1.0);
		if(!isShadow)set_material(leaf);
		glBegin(GL_TRIANGLE_FAN);

		// Set normal for the circular leafs
		glNormal3f(0,-1,0);

		glBegin(GL_TRIANGLE_FAN);
			glVertex2f(0.0, 0.0);

			for (float angle=0.0; angle < 360.0; angle += 2.0)
			{
			    float x = 0.0 + sin(angle) * 0.03;
			    float y = 0.0 + cos(angle) * 0.03;
			    glVertex2f(x, y);
			}
		glEnd();
		glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 0.0);
	glPopMatrix();

	// Reset normal for floor
	glNormal3f(0,1,0);
}

void LSystem::set_material(materialStruct mat) {
	glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT, mat.ambient);
	glMaterialfv (GL_FRONT_AND_BACK, GL_DIFFUSE, mat.diffuse);
	glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, mat.specular);
}

void LSystem::set_random(bool isRandom) {
	if(isRandom){
		// Random angle between 0.0 and 50.0
		float random = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/50));
		angle = random;
	}else{
		angle = 25.0;
	}
}
