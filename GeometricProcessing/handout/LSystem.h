#include <vector>
#include <string>
#include "LSRule.h"
#include <QGLWidget>
#include <iostream>
#include <GL/glut.h>
#include <stdlib.h> // rand
#include <math.h> // sin and cos

typedef struct materialStruct {
  GLfloat ambient [4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat normal;
} materialStruct;

class LSystem
{
public:
  LSystem(std::string axiom);
  LSystem();
  ~LSystem();

  std::vector<LSRule> rules;
  std::string axiom;
  std::string currentString;
  int generations;
  bool isShadow;
  float angle;
  bool random;

  void generate(int givenGenerations);
  void addRule(std::string from, std::string to);
  void renderLsystem(float length, int generation);
  void drawBranch(float length, int generations);
  void drawLeaf();
  void set_material(materialStruct mat);
  void set_random(bool isRandom);

  materialStruct leaf {
    {0.0, 0.8, 0.0, 1.0},
    {0.4, 0.8, 0.3, 1.0},
    {0.3, 0.9, 0.4, 1.0},
    90.0
  };

  materialStruct brown {
    {0.43, 0.12, 0.03, 1.0},
    {0.54, 0.27, 0.07, 1.0},
    {0.32, 0.28, 0.24, 1.0},
    27.8
  };

  materialStruct black {
    {0.0, 0.0, 0.0, 1.0},
    {0.0, 0.0, 0.0, 1.0},
    {0.1, 0.1, 0.1, 1.0},
    100.0
  };
};
