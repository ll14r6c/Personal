///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	main.cpp
//	------------------------
//
///////////////////////////////////////////////////

#include <QApplication>
#include "RenderWidget.h"
#include <stdio.h>

int main(int argc, char **argv)
	{ // main()

	glutInit(&argc, argv);
	// initialize QT
	QApplication app(argc, argv);

	//	create a window
	RenderWidget aWindow(NULL);

	// 	set the initial size
	aWindow.resize(600, 600);

	// show the window
	aWindow.show();

	// set QT running
	return app.exec();

	// paranoid return value
	exit(0);
	} // main()
