///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	RenderWidget.h
//	------------------------
//
//	The main widget that shows the geometry
//
///////////////////////////////////////////////////

#include <math.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include "RenderWidget.h"
#include <iostream>

// Shadow plane information
static GLfloat shadow[4][4];
static GLfloat light_position[4] = {10.0, 20.0, 10.0, 0.0};

materialStruct green {
{0.0, 0.5, 0.0, 1.0},
{0.2, 0.3, 0.2, 1.0},
{0.2, 0.35, 0.2, 1.0},
25.0
};

// Calculates the matrix which projects the from the light to the ground
void shadowMatrix(GLfloat shadow[4][4], GLfloat ground[4], GLfloat light[4])
{
  // Dot product between light position and ground plane normal
  GLfloat dot = ground[0] * light[0] + ground[1] * light[1] + ground[2] * light[2] + ground[3] * light[3];

  shadow[0][0] = dot - light[0] * ground[0];
  shadow[1][0] = 0.f - light[0] * ground[1];
  shadow[2][0] = 0.f - light[0] * ground[2];
  shadow[3][0] = 0.f - light[0] * ground[3];

  shadow[0][1] = 0.f - light[1] * ground[0];
  shadow[1][1] = dot - light[1] * ground[1];
  shadow[2][1] = 0.f - light[1] * ground[2];
  shadow[3][1] = 0.f - light[1] * ground[3];

  shadow[0][2] = 0.f - light[2] * ground[0];
  shadow[1][2] = 0.f - light[2] * ground[1];
  shadow[2][2] = dot - light[2] * ground[2];
  shadow[3][2] = 0.f - light[2] * ground[3];

  shadow[0][3] = 0.f - light[3] * ground[0];
  shadow[1][3] = 0.f - light[3] * ground[1];
  shadow[2][3] = 0.f - light[3] * ground[2];
  shadow[3][3] = dot - light[3] * ground[3];
}

// constructor
RenderWidget::RenderWidget(QWidget *parent)
	: QGLWidget(parent)
	{ // constructor

	// initialise arcballs to 80% of the widget's size
	Ball_Init(&lightBall);		Ball_Place(&lightBall, qOne, 0.80);
	Ball_Init(&objectBall);		Ball_Place(&objectBall, qOne, 0.80);

	// initialise translation values
	translate_x = translate_y = 0.0;

	// and set the button to an arbitrary value
	whichButton = -1;

	// Add some rules to the LSystem
	myLSystem = LSystem("X");
	myLSystem.addRule("X","F[+X][-X][&X][^X]FX");
	myLSystem.addRule("F","FF");

	} // constructor

// destructor
RenderWidget::~RenderWidget()
	{ // destructor
	// nothing yet
	} // destructor

// called when OpenGL context is set up
void RenderWidget::initializeGL()
	{ // RenderWidget::initializeGL()
	// enable Z-buffering
	glEnable(GL_DEPTH_TEST);

	// set lighting parameters
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	// background is pink
	glClearColor(1.0, 0.7, 0.7, 1.0);

  // Hardcoded equation of plane
	GLfloat plane[4] = { 0, 16, 0, 16};
  // Caclculate the shadow vector
	shadowMatrix(shadow, plane, light_position);
	} // RenderWidget::initializeGL()

// called every time the widget is resized
void RenderWidget::resizeGL(int w, int h)
	{ // RenderWidget::resizeGL()
	// reset the viewport
	glViewport(0, 0, w, h);

	// set projection matrix to be glOrtho based on zoom & window size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// compute the aspect ratio of the widget
	float aspectRatio = (float) w / (float) h;

	// depending on aspect ratio, set to accomodate a sphere of radius = diagonal without clipping
	if (aspectRatio > 1.0)
		glOrtho(-aspectRatio, aspectRatio, -1.0, 1.0, -1.0, 100.0);
	else
		glOrtho(-1.0, 1.0, -1.0/aspectRatio, 1.0/aspectRatio, -1.0, 100.0);

	} // RenderWidget::resizeGL()

// called every time the widget needs painting
void RenderWidget::paintGL()
	{ // RenderWidget::paintGL()
	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set lighting on
	glEnable(GL_LIGHTING);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// set light position first
	// retrieve rotation from arcball & apply
	GLfloat mNow[16];
	Ball_Value(&lightBall, mNow);
	glMultMatrixf(mNow);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	// apply translation for interface control
	glLoadIdentity();
	glTranslatef(translate_x, translate_y, 0.0);

	// apply rotation matrix from arcball
	Ball_Value(&objectBall, mNow);
	glMultMatrixf(mNow);

	// code for actual render goes here

	// render a green floor plane
	glPushMatrix();
		glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT, green.ambient);
		glMaterialfv (GL_FRONT_AND_BACK, GL_DIFFUSE, green.diffuse);
		glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, green.specular);
		glBegin(GL_TRIANGLE_STRIP);
			glVertex3f(3, -1.001, 3);
			glVertex3f(3, -1.001, -3);
			glVertex3f(-3, -1.001, 3);
			glVertex3f(-3, -1.001, -3);
		glEnd();
	glPopMatrix();

  // Setting random to true will generate a random angle for tree evrey frame
  // myLSystem.set_random(true);

	// One render pass for the shadow
	glPushMatrix();
  	glMultMatrixf((GLfloat *) shadow);
  	myLSystem.isShadow = true;
  	glTranslatef(0.0, -1, 0.0);
  	myLSystem.generate(3);
  	myLSystem.isShadow = false;
	glPopMatrix();

	// Another render for the tree
	glTranslatef(0.0, -1.0, 0.0);
	myLSystem.generate(3);

	} // RenderWidget::paintGL()

// mouse-handling
void RenderWidget::mousePressEvent(QMouseEvent *event)
	{ // RenderWidget::mousePressEvent()
	// store the button for future reference
	whichButton = event->button();

	// find the minimum of height & width
	float size = (width() > height()) ? height() : width();

	// convert to the ArcBall's vector type
	HVect vNow;

	// scale both coordinates from that
	vNow.x = (2.0 * event->x() - size) / size;
	vNow.y = (size - 2.0 * event->y() ) / size;

	// now either translate or rotate object or light
	switch(whichButton)
		{ // button switch
		case Qt::RightButton:
			// save the last x, y
			last_x = vNow.x; last_y = vNow.y;
			// and update
			updateGL();
			break;
		case Qt::MiddleButton:
			// pass the point to the arcball code
			Ball_Mouse(&lightBall, vNow);
			// start dragging
			Ball_BeginDrag(&lightBall);
			// update the widget
			updateGL();
			break;
		case Qt::LeftButton:
			// pass the point to the arcball code
			Ball_Mouse(&objectBall, vNow);
			// start dragging
			Ball_BeginDrag(&objectBall);
			// update the widget
			updateGL();
			break;
		} // button switch
	} // RenderWidget::mousePressEvent()

void RenderWidget::mouseMoveEvent(QMouseEvent *event)
	{ // RenderWidget::mouseMoveEvent()
	// find the minimum of height & width
	float size = (width() > height()) ? height() : width();

	// convert to the ArcBall's vector type
	HVect vNow;

	// scale both coordinates from that
	vNow.x = (2.0 * event->x() - size) / size;
	vNow.y = (size - 2.0 * event->y() ) / size;

	// now either translate or rotate object or light
	switch(whichButton)
		{ // button switch
		case Qt::RightButton:
			// subtract the translation
			translate_x += vNow.x - last_x;
			translate_y += vNow.y - last_y;
			last_x = vNow.x;
			last_y = vNow.y;
			// update the widget
			updateGL();
			break;
		case Qt::MiddleButton:
			// pass it to the arcball code
			Ball_Mouse(&lightBall, vNow);
			// start dragging
			Ball_Update(&lightBall);
			// update the widget
			updateGL();
			break;
		case Qt::LeftButton:
			// pass it to the arcball code
			Ball_Mouse(&objectBall, vNow);
			// start dragging
			Ball_Update(&objectBall);
			// update the widget
			updateGL();
			break;
		} // button switch
	} // RenderWidget::mouseMoveEvent()

void RenderWidget::mouseReleaseEvent(QMouseEvent *event)
	{ // RenderWidget::mouseReleaseEvent()
	// now either translate or rotate object or light
	switch(whichButton)
		{ // button switch
		case Qt::RightButton:
			// just update
			updateGL();
			break;
		case Qt::MiddleButton:
			// end the drag
			Ball_EndDrag(&lightBall);
			// update the widget
			updateGL();
			break;
		case Qt::LeftButton:
			// end the drag
			Ball_EndDrag(&objectBall);
			// update the widget
			updateGL();
			break;
		} // button switch
	} // RenderWidget::mouseReleaseEvent()
