#include "SceneWidget.h"

SceneWidget::SceneWidget(QWidget *parent)
	: QGLWidget(parent)
{
}

SceneWidget::~SceneWidget()
{
}

// called when OpenGL context is set up
void SceneWidget::initializeGL() 
{
	qglClearColor(QColor::QColor(61, 65, 76, 0.0));
	glShadeModel(GL_SMOOTH);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_CULL_FACE);
}

void SceneWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();             // Reset The Current Modelview Matrix

	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	gluLookAt(0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f);

	addFloor();
}

void SceneWidget::resizeGL(int w, int h)
{
	int side = qMin(w, h);
	glViewport((w - side) / 2, (h - side) / 2, side, side);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)w / (GLfloat)h, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
}

void SceneWidget::addFloor()
{
	glColor3f(0.93333f, 0.96471f, 1.00000f);
	glPushMatrix();
	glTranslatef(0.0f, -3, 0.0f);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-800, 0.0f,  800);
	glVertex3f( 800, 0.0f,  800);
	glVertex3f( 800, 0.0f, -800);
	glVertex3f(-800, 0.0f, -800);
	glEnd();
	glPopMatrix();
}
