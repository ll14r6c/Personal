#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QtCore/QVariant>
#include <QApplication>
#include <QDoubleSpinBox>
#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include "SceneWidget.h"


class MainWindow : public QWidget
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow();
	void retranslateUi(QWidget *MainWindow);

private:
	QWidget *horizontalLayoutWidget;
	QHBoxLayout *horizontalLayout;
	SceneWidget *openGLWidget;
	QVBoxLayout *verticalLayout;
	QHBoxLayout *horizontalLayout_3;
	QLabel *label;
	QDoubleSpinBox *doubleSpinBox;
	QHBoxLayout *horizontalLayout_4;
	QLabel *label_2;
	QDoubleSpinBox *doubleSpinBox_2;
	QHBoxLayout *horizontalLayout_5;
	QLabel *label_3;
	QDoubleSpinBox *doubleSpinBox_3;
	QFrame *line;
	QHBoxLayout *horizontalLayout_7;
	QLabel *label_4;
	QDoubleSpinBox *doubleSpinBox_4;
	QHBoxLayout *horizontalLayout_8;
	QLabel *label_5;
	QDoubleSpinBox *doubleSpinBox_5;
	QHBoxLayout *horizontalLayout_9;
	QLabel *label_6;
	QDoubleSpinBox *doubleSpinBox_6;
	QFrame *line_3;
	QHBoxLayout *horizontalLayout_6;
	QPushButton *pushButton_2;
	QFrame *line_2;
	QHBoxLayout *horizontalLayout_11;
	QLabel *label_7;
	QSlider *horizontalSlider;
	QHBoxLayout *horizontalLayout_10;
	QPushButton *pushButton_5;
	QPushButton *pushButton_4;
	QPushButton *pushButton_3;
	QPushButton *pushButton_6;
};