#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_EngineCwk2.h"

class EngineCwk2 : public QMainWindow
{
	Q_OBJECT

public:
	EngineCwk2(QWidget *parent = Q_NULLPTR);

private:
	Ui::EngineCwk2Class ui;
};
