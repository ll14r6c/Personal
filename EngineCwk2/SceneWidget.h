#pragma once

#include <QGLWidget>
#include <GL\GLU.h>


class SceneWidget : public QGLWidget
{
	Q_OBJECT

public:
	SceneWidget(QWidget *parent = Q_NULLPTR);
	~SceneWidget();

private:

protected:
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);

	void addFloor();
};