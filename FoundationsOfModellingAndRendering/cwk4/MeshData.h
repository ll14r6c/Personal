#include <vector>
#include "Vertex.h"


#pragma once
class MeshData
{
public:
	MeshData();
	~MeshData();
	std::vector<Vertex> getHardcodedData();
private:
	std::vector<Vertex> verticesArray;
};

