#include "Eye.h"



Eye::Eye() : imageWidth(128), imageHeight(128), FOV(90)
{
	// Set default eye values
	pos.x = 0;
	pos.y = 0;
	pos.z = 0;

	look.x = 0;
	look.y = 0;
	look.z = 1;

	up.x = 0;
	up.y = 1;
	up.z - 0;
}

Eye::Eye(int width, int height, float fov) : imageWidth(width), imageHeight(height), FOV(fov)
{
}


Eye::~Eye()
{
}
