#include <iostream>
#include "Image.h"

int main() {
	// Create a 128x128 image with output.ppm name
	Image image(128, 128, "output", "ppm");
	// FIll the image with yellow pixels
	image.fillRGB(255, 255, 192);
	// Ray cast the image
	image.rayCaster();
	// Write the data to file
	image.writeDataToFile();
	std::cout << "Done" << std::endl;
	return 0;
}
