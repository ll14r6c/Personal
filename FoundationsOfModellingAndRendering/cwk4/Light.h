#include "Cartesian3.h"
#include "RGB.h"

#pragma once
class Light
{
public:
	Light();
	~Light();

	Cartesian3 lightPosition;

	float lightPower;
	Cartesian3 ambient;
	Cartesian3 diffuse;
	Cartesian3 specular;
};
