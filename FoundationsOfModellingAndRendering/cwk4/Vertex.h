#include "RGB.h"
#include "Cartesian3.h"

#pragma once

struct UV {
	float u;
	float v;

	UV() : u(0.0), v(0.0) {}
};

class Vertex
{
public:
	Vertex();
	~Vertex();

	Cartesian3 pos;
	RGB color;
	UV uvCoords;
};

