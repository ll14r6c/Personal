#include "Cartesian3.h"

#pragma once
class Ray
{
public:
	Ray();
	Ray(Cartesian3 o, Cartesian3 d);
	~Ray();

	// Given 3 vertices on a triangle, see if the ray intersects the triangle
	bool triangleIntersect(Cartesian3 A, Cartesian3 B, Cartesian3 C, float &distance, Cartesian3 &point);

	Cartesian3 origin;
	Cartesian3 direction;
};
