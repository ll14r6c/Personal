#include "Ray.h"
#include "Cartesian3.h"


Ray::Ray()
{
}

Ray::Ray(Cartesian3 o, Cartesian3 d) : origin(o), direction(d)
{
}


Ray::~Ray()
{
}

// Moller-Trumbore intersections algorithm implementation
bool Ray::triangleIntersect(Cartesian3 A, Cartesian3 B, Cartesian3 C, float &t, Cartesian3 &point)
{
	Cartesian3 edge1 = B - A;
	Cartesian3 edge2 = C - A;
	Cartesian3 h = direction.cross(edge2);
	float a = edge1.dot(h);
	// If a is close to zero, the ray is parallel to the triangle
	if (a > -__FLT_EPSILON__ && a < __FLT_EPSILON__) {
		return false;
	}
	float f = 1.0 / a;
	Cartesian3 s = origin - A;
	float u = f * (s.dot(h));
	if (u < 0.0 || u > 1.0) {
		return false;
	}
	Cartesian3 q = s.cross(edge1);
	float v = f * direction.dot(q);
	if (v < 0.0 || u + v > 1.0) {
		return false;
	}
	// Find intersection point
	t = f * edge2.dot(q);
	if (t > __FLT_EPSILON__)
	{
		point = origin + direction * t;
		return true;
	}
	return false;
}
