#include "FaceMesh.h"
// constructor will initialise to safe values
FaceMesh::FaceMesh(){
	// force the size to nil
	vertices.resize(0);
	Vertex.resize(0);
	Face.resize(0);
}


// CODE PARTIALLY TAKEN FROM GIVEN CODE FILE: 'GeometricSurfaceFaceDS.cpp'
// read routine for .tri returns true on success, failure otherwise
bool FaceMesh::ReadFileTriangleSoup(std::string fileName){
  // open the input file
  std::ifstream inFile(fileName);
  if (inFile.bad()){
    return false;
  }

  // Parse then store the filename for output
  try{
    storedFileName = fileName.substr(10,50).c_str();
  	int startOfTri = storedFileName.find(".tri");
  	storedFileName.erase(startOfTri, 4);
  }catch(...){
    return false;
  }

  // set the number of vertices and faces
  long nTriangles = 0, nVertices = 0;

  // read in the number of vertices
  inFile >> nTriangles;
  nVertices = nTriangles * 3;

  // now allocate space for them all
  vertices.resize(nVertices);

  // now loop to read the vertices in
  for (int vertex = 0; vertex < nVertices; vertex++){
    inFile >> vertices[vertex].x >> vertices[vertex].y >> vertices[vertex].z;
  }
  return true;
}

// Computes the face index format and outputs a file with given filename
// returns true if successfully completed
bool FaceMesh::face2faceindex(){
	// ensure the vertices is filled with values
	if(vertices.size() == 0){
		return false;
	}
	// loop through the triangle soup, adding new vertices to Vertex vector
	for(int i = 0; i < vertices.size(); i++){
		// If its new, add it to Vertex vector and add the new index to faces
		// else just add the index it was found at to Faces
		int indexAt = getIndex(vertices[i]);
		if(indexAt == -1){
			Face.push_back(Vertex.size());
			Vertex.push_back(vertices[i]);
		}else{
			Face.push_back(indexAt);
		}
	}
	outfile = "../outputs/"+storedFileName+".face";
	writeFaceIndex(outfile);
	return true;
}

// gets the index of a face within the Vertex vector, if not in returns -1
int FaceMesh::getIndex(Cartesian3 &givenVertex){
	// Iterate over all elements of vector
	for (int i = 0; i < Vertex.size(); i++){
		// If the givenVertex is in the Vertex array, return its index
		if(Vertex[i] == givenVertex){
			return i;
		}
	}
	// otherwise return -1 for not being found
	return -1;
}

// Writes the Vertex and f2fFace vector arrays to a given file
bool FaceMesh::writeFaceIndex(std::string outfile){
	FILE * pFile;
	pFile = fopen(outfile.c_str(),"w");
	fprintf(pFile, "# University of Leeds 2018-2019\n");
	fprintf(pFile, "# COMP 5812M Assigment 2\n");
	fprintf(pFile, "# Ryan Carty\n");
	fprintf(pFile, "# 200972525\n");
	fprintf(pFile, "# \n");
	fprintf(pFile, "# Object Name: %s\n", storedFileName.c_str());
	fprintf(pFile, "# Vertices=%lu Faces=%lu\n", Vertex.size(), Face.size()/3);
	fprintf(pFile, "# \n");

	// Write Vertexs to file
	for (int i = 0; i < Vertex.size(); i++)
	{
		fprintf(pFile,"Vertex %d % .5f % .5f % .5f\n",i,Vertex[i].x,Vertex[i].y,Vertex[i].z);
	}
	// Write Faces to file
	for (int i = 0; i < Face.size(); i+=3)
	{
		fprintf(pFile,"Face %d % d % d % d\n",i/3,Face[i],Face[i+1],Face[i+2]);
	}
	fclose (pFile);
	return true;
}
