#include <iostream>
#include "FaceMesh.h"
#include "DirMesh.h"

// CODE PARTIALLY TAKEN FROM 'main.cpp' in Assigment2MeshDSHandout file
int main(int argc, char **argv){
  // Initialise the two mesh
  FaceMesh theFaceMesh;
  DirMesh theDirMesh;
  // check the args to make sure there's an input file
  if (argc == 2){
    // If it cannot read the file, throw error
    if (!theFaceMesh.ReadFileTriangleSoup(argv[1])){
        printf("Read failed for file %s\n", argv[1]);
        exit(0);
      }else{
        // If the file is read with no errors
        theFaceMesh.face2faceindex();
      }

    // If it cannot read the file, throw error
    // Use the outfile location from theFaceMesh for reading
    if (!theDirMesh.readFaceFile(theFaceMesh.outfile)){
        printf("Read failed for file %s\n", argv[1]);
        exit(0);
      }else{
        // If the file is read with no errors
        theDirMesh.faceindex2directededge();
      }
    // If not enough parameters given, tell user how to run
    }else{
    printf("Usage: %s filename\n", argv[0]);
    exit(0);
  }
  return 0;
}
