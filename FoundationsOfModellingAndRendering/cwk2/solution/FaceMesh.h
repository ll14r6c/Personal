#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include "Cartesian3.h"

class FaceMesh{
public:
  FaceMesh();
  // vectors to store vertex and triangle information
	std::vector<Cartesian3> vertices;
	// vectors to store the face2faceindex VERTEX values
	std::vector<Cartesian3> Vertex;
	// vectors to store the face2faceindex FACE values
	std::vector<int> Face;
  // Store filename for output
  std::string storedFileName;
  // Variable to store the .face location
  std::string outfile;
  // face2faceindex computes the face format and outputs to a corresponding file
	// outfile is a char of the filename without '.face'
	bool face2faceindex();
	// gets the index of a face within the f2fVertex vector, if not in returns -1
	int getIndex(Cartesian3 &face);
	// given an output filename, writes the stored data to .face file format
	bool writeFaceIndex(std::string outfile);
  // read routine for .tri returns true on success, failure otherwise
  bool ReadFileTriangleSoup(std::string fileName);
};
