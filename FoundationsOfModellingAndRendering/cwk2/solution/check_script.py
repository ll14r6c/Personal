import subprocess
import os

# For every file in the models directory run the solution with the file as arg

model_dir = "../models/"
file_list = os.listdir(model_dir)
for single_file in file_list:
    command = "./solution "+model_dir+single_file
    print("Checking: ", single_file)
    subprocess.call([command], shell=True)
    print("\n")
