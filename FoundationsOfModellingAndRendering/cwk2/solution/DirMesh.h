#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include "Cartesian3.h"

class DirMesh{
public:
  DirMesh();
	// vectors to store the faceindex2directededge VERTEX values
	std::vector<Cartesian3> VertexDE;
	// vectors to store the faceindex2directededge FACE values
	std::vector<int> FaceDE;
  // vectors of tuples to store the counter clockwise face fata
	std::vector<std::vector<int> > counterClockwise;
  // vectors to store the firstDirEdge data
	std::vector<int> firstDirEdge;
  // vectors to store the otherHalf data
	std::vector<int> otherHalf;
  // Store filename for output
  std::string storedFileName;
  
  // Reads in a .face file, computes the directed edge format and outputs to file
  bool faceindex2directededge();
  // read routine for .face returns true on success, failure otherwise
  bool readFaceFile(std::string fileName);
  // routine to store the counter clockwise face data, true on success
  bool storeCounterClockwiseLoops();
  // Computes the firstDirEdge values and store them in a vector
  bool computeFirstDirEdge();
  // Function to compute the other half values
  bool computeOtherHalf();
  // Writes the stored data to a .diredge file
  bool writeDiredgeToFile(std::string fileName);
  // Checks if the stored mesh is manifold or not
	bool checkManifold();
  // Returns the genus for the stored mesh using eulers formula
	int calcEuler();
};
