#include "DirMesh.h"
// constructor will initialise to safe values
DirMesh::DirMesh(){
	// force the size to nil
	FaceDE.resize(0);
	VertexDE.resize(0);
  firstDirEdge.resize(0);
  otherHalf.resize(0);
	counterClockwise.resize(0);
}

// Main function which will compute the directed edge format then write to file
// Also checks if the stored mesh is manifold, if it is prints out the genus
bool DirMesh::faceindex2directededge(){
  // Store the counter clockwise edge values
	storeCounterClockwiseLoops();
	// computes the directed edge format
	computeFirstDirEdge();
  // Compute the other half values
	computeOtherHalf();
	// outputs to a .diredge file
	std::string outFile = "../outputs/"+storedFileName+".diredge";
	writeDiredgeToFile(outFile);
  // If it is manifold, print out the genus
	if(checkManifold()){
		std::cout << "Genus: " << calcEuler() << std::endl;
	}
	return true;
}

// Writes the stored data to a .diredge file
// returns true if successful
bool DirMesh::writeDiredgeToFile(std::string outfile){
	FILE * pFile;
	pFile = fopen(outfile.c_str(),"w");
	fprintf(pFile, "# University of Leeds 2018-2019\n");
	fprintf(pFile, "# COMP 5812M Assigment 2\n");
	fprintf(pFile, "# Ryan Carty\n");
	fprintf(pFile, "# 200972525\n");
	fprintf(pFile, "# \n");
	fprintf(pFile, "# Object Name: %s\n", storedFileName.c_str());
	fprintf(pFile, "# Vertices=%lu Faces=%lu\n", VertexDE.size(), FaceDE.size()/3);
	fprintf(pFile, "# \n");

	// Write Vertexs to file
	for (int i = 0; i < VertexDE.size(); i++)
	{
		fprintf(pFile,"Vertex %d % .5f % .5f % .5f\n",i,VertexDE[i].x,VertexDE[i].y,VertexDE[i].z);
	}
	// Write Edge to file
	for (int i = 0; i < firstDirEdge.size(); i++)
	{
		fprintf(pFile,"FirstDirectedEdge %d % d\n",i,firstDirEdge[i]);
	}
	// Write Faces to file
	for (int i = 0; i < FaceDE.size(); i+=3)
	{
		fprintf(pFile,"Face %d % d % d % d\n",i/3,FaceDE[i],FaceDE[i+1],FaceDE[i+2]);
	}
	// Write Other Half to file
	for (int i = 0; i < otherHalf.size(); i++)
	{
		fprintf(pFile,"OtherHalf %d % d\n",i,otherHalf[i]);
	}
	fclose (pFile);
	return true;
}

// Computes the other half values
// Other half values are the values from the same edge but on opposite sides
// returns true if successful
bool DirMesh::computeOtherHalf(){
  // For each edge in the stored edges
	for(auto &edge: counterClockwise){
    // For each edge in the stored edges
		for(auto &edge2: counterClockwise){
      // if the opposing edges are equal, then they are the other half
			if(edge[2] == edge2[1] && edge[1] == edge2[2]){
				otherHalf.push_back(edge2[0]);
			}
		}
	}
	return true;
}

// Computes the firstDirEdge values and store them in a vector
bool DirMesh::computeFirstDirEdge(){
  // Set currentLowest variable to a really high number
	int currentLowest = 2147483647;
  // Loop through the vertices
	for(int i = 0; i < VertexDE.size(); i++){
    // Loop through the edges
		for(auto &edge : counterClockwise){
      // If the edge coming out of the vertex has the lowest value, save it as
      // the currentLowest
			if(edge[1] == i && edge[0] < currentLowest){
				currentLowest = edge[0];
			}
		}
    // Append to the vector the lowest value coming out of the vertex
		firstDirEdge.push_back(currentLowest);
    // Reset the currentLowest to a high value again for the next loop
		currentLowest = 2147483647;
	}
	return true;
}

// Stores the edge values in counterClockwise vector
bool DirMesh::storeCounterClockwiseLoops(){
	std::vector<int> tempVector;
	// Loop through all the faces, storing the 3 edges from each face
  // Edges are stored in the format: index, vertex1, vertex2
	for(int i = 0; i < FaceDE.size(); i+= 3){
		tempVector.push_back(i);
		tempVector.push_back(FaceDE[i+2]);
		tempVector.push_back(FaceDE[i  ]);
		counterClockwise.push_back(tempVector);
		tempVector.clear();
		tempVector.push_back(i+1);
		tempVector.push_back(FaceDE[i  ]);
		tempVector.push_back(FaceDE[i+1]);
		counterClockwise.push_back(tempVector);
		tempVector.clear();
		tempVector.push_back(i+2);
		tempVector.push_back(FaceDE[i+1]);
		tempVector.push_back(FaceDE[i+2]);
		counterClockwise.push_back(tempVector);
		tempVector.clear();
	}
	return true;
}

// read routine for face file, returns true on success, failure otherwise
bool DirMesh::readFaceFile(std::string fileName){
  // Parse then store the filename for output
  std::string faceInFile;
  try{
    // Get the file name from the file location
    storedFileName = fileName.substr(11,50).c_str();
    int startOfTri = storedFileName.find(".face");
    storedFileName.erase(startOfTri, 5);
    // reads a .face file
    faceInFile = "../outputs/"+storedFileName+".face";
  }catch(...){ // Catch any errors from parsing and return false
    return false;
  }
	// open the input file
	std::ifstream inFile(faceInFile.c_str());
	if (inFile.bad()){
		return false;
	}
	std::string line;
	// Read the file line by line
	while(std::getline(inFile, line)){
		std::istringstream iss(line);
		std::string type;
		int index;
		float x,y,z;
		// If a line is read with the correct format
		if((iss >> type >> index >> x >> y >> z)){
			// If the line is a vertex, add it to the vertex vector
			if(type == "Vertex"){
				Cartesian3 newLine(x,y,z);
				VertexDE.push_back(newLine);
			// If the line is a face, add it to the face vector
			}else if(type == "Face"){
				FaceDE.push_back(x);
				FaceDE.push_back(y);
				FaceDE.push_back(z);
			}
		}
	}
	return true;
}

// Checks if the stored mesh is manifold or not
// Returns true if its manifold, otherwise false
bool DirMesh::checkManifold(){
  // Loop through the firstDirEdge and check that the other half returns to it
  // If all return properly, its manifold, otherwise not
	for(auto &dirEdge : firstDirEdge){
		if(otherHalf[otherHalf[dirEdge]] != dirEdge){
			std::cout << "Not manifold, failed on vertex: " << dirEdge << std::endl;
			return false;
		}
	}
	std::cout << "Manifold" << std::endl;
	return true;
}

// Returns the genus for the stored mesh using eulers formula
int DirMesh::calcEuler(){
	int genus = (2.0+(int)(counterClockwise.size()/2)-(int)VertexDE.size()-(int)(FaceDE.size()/3))/2;
	return abs(genus);
}
