# Coursework Assigment 2
## Questions and their solution locations within my code
- a)
	- Code solution within FaceMesh class in method called 'face2faceindex'. Called from main.cpp.
	- Output file in output folder.
- b)
	- Code solution within DirMesh class in method called 'faceindex2directededge'. Called from main.cpp.
	- Output file in output folder.
- c)
	- Code solution within DirMesh class in method called 'checkManifold'. Called from 'faceindex2directededge' method.
	- Output ran for all files shown below under the 'Manifold and Genus' header
- d)
	- Code solution within DirMesh class in method calcEuler. Called from 'faceindex2directededge' method.
	- Output ran for all files shown below under the 'Manifold and Genus' header
- e) Solution immediately below:

## Algorithmic complexity of my code
- face2faceindex
  - O(n)   reading file
  - O(n^2) face2faceindex
  - O(n)   writing file
  - Total = O(2n) + O(n^2) = O(n^2)
- faceindex2directededge
  - O(n)   reading file
  - O(n)   counterClockwise
  - O(n^2) computeFirstDirEdge
  - O(n^2) computeOtherHalf
  - O(n)   checkManifold
  - O(n)   writing file
  - Total = O(4n) + O(2(n^2)) = O(n^2)

## Manifold and Genus output
```
rc@rc-XPS-13-9350:~/Documents/GitLab/Personal/FoundationsOfModellingAndRendering/cwk2/solution$ python3 check_script.py
Checking:  747.tri
Not manifold, failed on vertex: 0


Checking:  tetrahedron_bad.tri
Not manifold, failed on vertex: 1


Checking:  dodecahedron.tri
Manifold
Genus: 0


Checking:  legoman.tri
Not manifold, failed on vertex: 149


Checking:  2ballout.tri
Manifold
Genus: 1


Checking:  quadtorus.tri
Manifold
Genus: 4


Checking:  badcube.tri
Not manifold, failed on vertex: 1


Checking:  torus.tri
Manifold
Genus: 1


Checking:  hexahedron.tri
Manifold
Genus: 0


Checking:  many.tri
Manifold
Genus: 8


Checking:  fuelinject.tri
Not manifold, failed on vertex: 1391


Checking:  2torus.tri
Manifold
Genus: 1


Checking:  tritorus.tri
Manifold
Genus: 3


Checking:  bitorus.tri
Manifold
Genus: 2


Checking:  octahedron.tri
Manifold
Genus: 0


Checking:  tetrahedron.tri
Manifold
Genus: 0


Checking:  icosahedron.tri
Manifold
Genus: 0


Checking:  horse.tri
Manifold
Genus: 0


Checking:  5bout.tri
Manifold
Genus: 1
```

## How to run my code
>**File structure shown below**

- models
  - All models should be in here
- outputs
  - Folder ready for output files
- solution
  - Folder containing my solution
### Type the following commands in the terminal
1. `qmake solution.pro`
2. `make`
3. `./solution ../models/FILENAME.tri`
4. To run through all files I wrote a simple python script called check_script.py
   - `python3 check_script.py`

## File dataflow within my code
../models/input.tri --> FaceMesh --> ../outputs/output.face --> DirMesh --> ../outputs/output.diredge
