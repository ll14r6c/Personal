######################################################################
# Automatically generated by qmake (2.01a) Sun Dec 23 20:35:52 2018
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += Cartesian3.h Light.h Material.h Ray.h Sphere.h Triangle.h
SOURCES += Cartesian3.cpp \
           Light.cpp \
           main.cpp \
           Material.cpp \
           Ray.cpp \
           Sphere.cpp \
           Triangle.cpp
