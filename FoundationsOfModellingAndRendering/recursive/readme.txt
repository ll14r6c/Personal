CW5
===

Cornell Box (square):
	Bottom left = -30, -30, 30
	Top right = 30, 30, 90
Camera position = 0,0,0
Camera FOV = 90

Box white material is pearl:
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(1, 0.829, 0.829);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;

Box green material is green pearl:
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(0.1, 1.0, 0.1);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;

Box red material is red pearl:
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(1.0, 0.1, 0.1);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;

Glass ball:
	Position: -15, -15, 45
	Colour: 0.95, 0.95, 0.95
	Reflection: 50%
	Refraction: 100%
	Fresnel calculation combines these two to give an output
	Index of refraction = 1.25

Blue ball:
	Position: 15, -15, 75
	Colour: 0.2, 0.2, 0.9
	Reflection: 100%
	Refraction: 0%
	Fresnel calculation combines these two to give an output


NOTES:
=====
My light is not an area light, it is a simple point light placed in the position of where an area light should be.

I have commented out triangle on triangle shadows because there is a bug, but this is working in my cwk4. (Sphere on triangle shadows are working, and are the only shadows needed for the output image).

TO RUN:
======
$ qmake -project
$ qmake recursive
$ make
$ ./recursive
