#pragma once
#include "Cartesian3.h"
class Material
{
public:
	Material();
	~Material();

	Cartesian3 ambient;
	Cartesian3 diffuse;
	Cartesian3 specular;
	Cartesian3 emission;
	float shininess;

	void setEmerald();
	void setPearl();
	void setGreenPearl();
	void setRedPearl();
	void setObsidian();

};

