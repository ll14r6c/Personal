#pragma once
#include "Cartesian3.h"
#include "Material.h"
class Triangle
{
public:
	Triangle();
	~Triangle();

	Cartesian3 A;
	Cartesian3 B;
	Cartesian3 C;

	Cartesian3 normal;

	Material material;

	Cartesian3 calcNormal();
};

