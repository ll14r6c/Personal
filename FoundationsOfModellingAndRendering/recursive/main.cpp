#include <math.h>
#include <fstream>
#include <vector>
#include "Cartesian3.h"
#include "Ray.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Light.h"
#include "Material.h"
#define M_PI 3.14159265358979323846
#define MAX_RAY_DEPTH 5

void writeDataToFile(int width, int height, Cartesian3 *imageData) {
	std::ofstream myfile("./output.ppm", std::ios::out | std::ios::binary);
	myfile << "P3\n" << width << " " << height << "\n255\n";
	for (int i = 0; i < width * height; i++) {
		myfile << imageData[i].x << " " << imageData[i].y << " " << imageData[i].z << "  \n";
	}
	myfile.close();
}

// Adds a cornel box to the scene with hole in roof
std::vector<Triangle> getTriangles() {
	std::vector<Triangle> toReturn;

	// Floor 1
	Triangle A;
	A.A = Cartesian3(-30, -30, 30);
	A.B = Cartesian3(30, -30, 90);
	A.C = Cartesian3(-30, -30, 90);
	A.normal = Cartesian3(0, 1, 0);
	toReturn.push_back(A);
	// Floor 2
	Triangle B;
	B.A = Cartesian3(-30, -30, 30);
	B.B = Cartesian3(30, -30, 30);
	B.C = Cartesian3(30, -30, 90);
	B.normal = Cartesian3(0, 1, 0);
	toReturn.push_back(B);

	// Roof 1
	Triangle C;
	C.A = Cartesian3(-30, 30, 30);
	C.B = Cartesian3(-5, 30, 90);
	C.C = Cartesian3(-30, 30, 90);
	C.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(C);
	// Roof 2
	Triangle D;
	D.A = Cartesian3(-30, 30, 30);
	D.B = Cartesian3(-5, 30, 30);
	D.C = Cartesian3(-5, 30, 90);
	D.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(D);
	// Roof 3
	Triangle K;
	K.A = Cartesian3(5, 30, 30);
	K.B = Cartesian3(30, 30, 90);
	K.C = Cartesian3(5, 30, 90);
	K.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(K);
	// Roof 4
	Triangle L;
	L.A = Cartesian3(5, 30, 30);
	L.B = Cartesian3(30, 30, 30);
	L.C = Cartesian3(30, 30, 90);
	L.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(L);
	// Roof 5
	Triangle M;
	M.A = Cartesian3(-5, 30, 65);
	M.B = Cartesian3(5, 30, 90);
	M.C = Cartesian3(-5, 30, 90);
	M.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(M);
	// Roof 6
	Triangle N;
	N.A = Cartesian3(-5, 30, 65);
	N.B = Cartesian3(5, 30, 65);
	N.C = Cartesian3(5, 30, 90);
	N.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(N);
	// Roof 7
	Triangle O;
	O.A = Cartesian3(-5, 30, 30);
	O.B = Cartesian3(5, 30, 55);
	O.C = Cartesian3(-5, 30, 55);
	O.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(O);
	// Roof 8
	Triangle P;
	P.A = Cartesian3(-5, 30, 30);
	P.B = Cartesian3(5, 30, 30);
	P.C = Cartesian3(5, 30, 55);
	P.normal = Cartesian3(0, -1, 0);
	toReturn.push_back(P);

	// Back Wall 1
	Triangle E;
	E.A = Cartesian3(-30, -30, 90);
	E.B = Cartesian3(30, 30, 90);
	E.C = Cartesian3(-30, 30, 90);
	E.normal = Cartesian3(0, 0, -1);
	toReturn.push_back(E);
	// Back Wall 2
	Triangle F;
	F.A = Cartesian3(-30, -30, 90);
	F.B = Cartesian3(30, -30, 90);
	F.C = Cartesian3(30, 30, 90);
	F.normal = Cartesian3(0, 0, -1);
	toReturn.push_back(F);

	// Right Wall 1 (green)
	Triangle G;
	G.A = Cartesian3(30, -30, 90);
	G.B = Cartesian3(30, 30, 30);
	G.C = Cartesian3(30, 30, 90);
	G.normal = Cartesian3(-1, 0, 0);
	G.material.setGreenPearl();
	toReturn.push_back(G);
	// Right Wall 2 (green) 
	Triangle H;
	H.A = Cartesian3(30, -30, 90);
	H.B = Cartesian3(30, -30, 30);
	H.C = Cartesian3(30, 30, 30);
	H.normal = Cartesian3(-1, 0, 0);
	H.material.setGreenPearl();
	toReturn.push_back(H);

	// Left Wall 1 (red)
	Triangle I;
	I.A = Cartesian3(-30, -30, 30);
	I.B = Cartesian3(-30, 30, 90);
	I.C = Cartesian3(-30, 30, 30);
	I.normal = Cartesian3(1, 0, 0);
	I.material.setRedPearl();
	toReturn.push_back(I);
	// Left Wall 2 (red) 
	Triangle J;
	J.A = Cartesian3(-30, -30, 30);
	J.B = Cartesian3(-30, -30, 90);
	J.C = Cartesian3(-30, 30, 90);
	J.normal = Cartesian3(1, 0, 0);
	J.material.setRedPearl();
	toReturn.push_back(J);

	return toReturn;
}

// Adds two spheres to the scene
// One glass-like with refraction and reflection (front)
// And another blue coloured with just reflection (back)
std::vector<Sphere> getSpheres() {
	std::vector<Sphere> toReturn;

	// Front Sphere
	Sphere A(Cartesian3(-15, -15, 45), Cartesian3(0.95, 0.95, 0.95), 15, 1.0, 0.5);
	toReturn.push_back(A);

	// Back Sphere
	Sphere B(Cartesian3(15, -15, 75), Cartesian3(0.2, 0.2, 0.9), 15, 0.0, 1.0);
	B.material.setObsidian();
	toReturn.push_back(B);
	return toReturn;
}

// Given the input parameters calculate blinn phong lighting for the point
Cartesian3 calcBlinnPhong(Cartesian3 normal, Light light, Cartesian3 point, Material material, Ray ray)
{
	// Set the default colours with reagrd to the light and material properties
	Cartesian3 ambientColor = light.ambient * material.ambient;
	Cartesian3 diffuseColor = light.diffuse * material.diffuse;
	Cartesian3 specColor = light.specular * material.specular;
	// Ensure the normal is normalised
	normal = normal.normalise();
	// direction to the light
	Cartesian3 lightDir = (light.lightPosition - point).normalise();

	// Caclulate the diffuse
	float lambertian = lightDir.dot(normal);
	if (lambertian < 0) {
		lambertian = 0;
	}

	// Caclulate the specular
	float specular = 0.0;
	if (lambertian > 0.0) {
		Cartesian3 vl = (light.lightPosition - point).normalise();
		Cartesian3 ve = (ray.origin - point).normalise();
		// half vector
		Cartesian3 hv = ((vl + ve) / 2).normalise();
		float specAngle = hv.dot(normal);
		if (specAngle < 0) {
			specAngle = 0;
		}
		specular = pow(specAngle, material.shininess);
	}
	// Return the full result
	Cartesian3 result = ambientColor + (diffuseColor * lambertian * light.lightPower) + (specColor * specular * light.lightPower);
	// Convert from 0-1 to 0-255
	result = Cartesian3(result.x * 255, result.y * 255, result.z * 255);
	return result;
}

Cartesian3 trace(Ray ray, std::vector<Triangle> triangles, std::vector<Sphere> spheres, Light light, int depth) {
	float closest = INFINITY;
	float shadowClosest = INFINITY;
	float distance, shadowDistance;
	Cartesian3 intersection1, intersection2;
	float distance1, distance2;
	Cartesian3 point;
	Cartesian3 col(255, 255, 192);
	Ray shadow;

	// For each sphere, check intersections
	for (auto&sphere : spheres) {
		distance1 = INFINITY;
		distance2 = INFINITY;
		if (ray.sphereIntersect(sphere, distance1, distance2, intersection1, intersection2)) {
			// If the intersection point of the ray is behind the origin (inside circle)
			// Set the intersection point and distance to the second intersection
			if (distance1 < 0) {
				distance1 = distance2;
				intersection1 = intersection2;
			}
			// If the intersection is the closest intersection to the camera
			if (distance1 < closest) {
				closest = distance1;
				// Normal at intersection
				Cartesian3 sphereNormal = (intersection1 - sphere.pos).normalise();
				// Boolean to check if inside of sphere or not
				bool inside = false;
				// If inside the sphere, invert normal
				if (ray.direction.dot(sphereNormal) > 0) {
					sphereNormal = (sphereNormal*-1);
					inside = true;
				}
				if ((sphere.transparency > 0 || sphere.reflection > 0) && depth < MAX_RAY_DEPTH) {
					float raydotnorm = -ray.direction.dot(sphereNormal);
					// Calculate the fresnel value for use with sphere effect
					float fresneleffect = 1.0 * 0.05 + pow(1 - raydotnorm, 3) * (1 - 0.05);
					Cartesian3 reflectdir = ray.direction - sphereNormal * 2 * ray.direction.dot(sphereNormal);
					Ray reflectionRay(intersection1 + sphereNormal * 1e-4, reflectdir.normalise());
					Cartesian3 reflection = trace(reflectionRay, triangles, spheres, light, depth + 1);
					Cartesian3 refraction;
					// if the sphere has some transparency, compute the refraction
					if (sphere.transparency > 0) {
						// Set index of refraction to be nearby 1.5 (ior of glass)
						float ior = 1.25;
						// Check if inside or outside and change the ior accordingly
						if (inside != true) {
							ior = 1 / ior;
						}
						// Calculate the refraction ray
						float cosi = -sphereNormal.dot(ray.direction);
						float k = 1 - ior * ior * (1 - cosi * cosi);
						Cartesian3 refractiondir = ray.direction * ior + sphereNormal * (ior *  cosi - sqrt(k));
						Ray refractionRay(intersection1 - sphereNormal * 1e-4, refractiondir.normalise());
						refraction = trace(refractionRay, triangles, spheres, light, depth + 1);
					}
					// Compute the resulting colour based on recursive values
					col = (reflection * fresneleffect + refraction * (1 - fresneleffect) * sphere.transparency) * sphere.col;
				}
				// If the material of the sphere has no transparency or reflectance
				// Calculate a normal blinn phong colour for the sphere based on its material
				else {
					col = calcBlinnPhong((intersection1 - sphere.pos).normalise(), light, intersection1, sphere.material, ray);
				}
			}
		}
	}

	// For each triangle, check intersections
	for (auto&triangle : triangles) {
		if (ray.triangleIntersect(triangle.A, triangle.B, triangle.C, distance, point)) {
			// If the intersection is the closest intersection to the camera
			if (distance < closest) {
				// Calculate the lighting / shading based on material
				closest = distance;
				col = calcBlinnPhong(triangle.normal, light, point, triangle.material, ray);

				// Set the shadow ray
				shadow.origin = point + triangle.normal * 1e-4;
				shadow.direction = (light.lightPosition - shadow.origin).normalise();

				// Calculate shadows for spheres on triangles
				for (auto&sphere : spheres) {
					if (shadow.sphereIntersect(sphere, shadowDistance, distance2, intersection1, intersection2)) {
						if (shadowDistance > 0.001) {
							col = col * 0.15;
						}
					}
				}
				// Calculate the shadows for triangles on triangles
				//for (auto&triangle : triangles) {
				//	if (shadow.triangleIntersect(triangle.A, triangle.B, triangle.C, shadowDistance, point)) {
				//		if (shadowDistance < shadowClosest && shadowDistance > 0.001) {
				//			col = col * 0.15;
				//			shadowClosest = shadowDistance;
				//		}
				//	}
				//}
			}
		}
	}
	return col;
}

// Main render function
int main() {
	// Set image properties
	int width = 400;
	int height = 400;
	float fov = 90.0;
	float aspectRatio = (float)width / (float)height;
	float fovAngle = tan(M_PI * 0.5 * fov / (float)180);

	// Set image data
	Cartesian3 *image = new Cartesian3[width*height];
	Cartesian3 *pixel = image;

	// Load in the scene data
	std::vector<Triangle> triangles = getTriangles();
	std::vector<Sphere> spheres = getSpheres();

	// Set a light location (in hole of box)
	Light light;
	light.lightPosition = Cartesian3(0.0, 29.5, 60.0);

	float rx, ry;
	Cartesian3 tempPixel;
	Ray ray;
	ray.origin = Cartesian3(0.0, 0.0, 0.0);
	// Send a ray through every pixel
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++, pixel++) {
			// Calculate the ray x and y from pixel to world space
			rx = (2 * ((x + 0.5) * (1 / (float)width)) - 1) * fovAngle * aspectRatio;
			ry = (1 - 2 * ((y + 0.5) * (1 / (float)height))) * fovAngle;
			// Set the ray direction
			ray.direction = Cartesian3(rx, ry, 1).normalise();
			// Check for intersections
			tempPixel = trace(ray, triangles, spheres, light, 0);
			// Save calculated colour
			*pixel = tempPixel.gammaCorrect();
		}
	}

	// Save the data to file
	writeDataToFile(width, height, image);

	delete[] image;
	return 0;
}
