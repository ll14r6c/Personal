#include "Material.h"




Material::Material() : shininess(64)
{
}


Material::~Material()
{
}

// Set some emerald material values
void Material::setEmerald()
{
	ambient = Cartesian3(0.0215, 0.1745, 0.0215);
	diffuse = Cartesian3(0.07568, 0.61424, 0.07568);
	specular = Cartesian3(0.633, 0.727811, 0.633);
	shininess = 64.0f;
}

// Set some pearl material values
void Material::setPearl()
{
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(1, 0.829, 0.829);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;
}

// Set some green pearl material values
void Material::setGreenPearl()
{
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(0.1, 1.0, 0.1);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;
}

// Set some obsidian material values
void Material::setObsidian()
{
	ambient = Cartesian3(0.05375, 0.05, 0.06625);
	diffuse = Cartesian3(0.18275, 0.17, 0.22525);
	specular = Cartesian3(0.332741, 0.328634, 0.346435);
	shininess = 16.0f;
}

// Set some red pearl material values
void Material::setRedPearl()
{
	ambient = Cartesian3(0.25, 0.20725, 0.20725);
	diffuse = Cartesian3(1.0, 0.1, 0.1);
	specular = Cartesian3(0.296648, 0.296648, 0.296648);
	shininess = 32.0f;
}
