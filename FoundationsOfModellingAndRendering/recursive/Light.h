#pragma once
#include "Cartesian3.h"
class Light
{
public:
	Light();
	~Light();

	Cartesian3 lightPosition;

	float lightPower;
	Cartesian3 ambient;
	Cartesian3 diffuse;
	Cartesian3 specular;

};

