#include "Light.h"



Light::Light()
{
	// Set some default white light values
	ambient = Cartesian3(0.5, 0.5, 0.5);
	diffuse = Cartesian3(0.5, 0.5, 0.5);
	specular = Cartesian3(1.0, 1.0, 1.0);
	lightPower = 1.0;

}


Light::~Light()
{
}
