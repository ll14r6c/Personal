///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	Cartesian3.h
//	------------------------
//
//	A minimal class for a point in Cartesian space
//
///////////////////////////////////////////////////

#include "Cartesian3.h"
#include "math.h"

// constructors
Cartesian3::Cartesian3()
	: x(0.0), y(0.0), z(0.0)
{}

Cartesian3::Cartesian3(float X, float Y, float Z)
	: x(X), y(Y), z(Z)
{}

Cartesian3::Cartesian3(const Cartesian3 &other)
	: x(other.x), y(other.y), z(other.z)
{}

// equality operator
bool Cartesian3::operator ==(const Cartesian3 &other)
{ // Cartesian3::operator ==()
	return ((x == other.x) && (y == other.y) && (z == other.z));
} // Cartesian3::operator ==()

  // addition operator
Cartesian3 Cartesian3::operator +(const Cartesian3 &other)
{ // Cartesian3::operator +()
	Cartesian3 returnVal(x + other.x, y + other.y, z + other.z);
	return returnVal;
} // Cartesian3::operator +()

  // subtraction operator
Cartesian3 Cartesian3::operator -(const Cartesian3 &other)
{ // Cartesian3::operator -()
	Cartesian3 returnVal(x - other.x, y - other.y, z - other.z);
	return returnVal;
} // Cartesian3::operator -()

  // multiplication operator
Cartesian3 Cartesian3::operator *(float factor)
{ // Cartesian3::operator *()
	Cartesian3 returnVal(x * factor, y * factor, z * factor);
	return returnVal;
} // Cartesian3::operator *()

Cartesian3 Cartesian3::operator*(const Cartesian3 & other)
{
	Cartesian3 returnVal(x * other.x, y * other.y, z * other.z);
	return returnVal;
}

// division operator
Cartesian3 Cartesian3::operator /(float factor)
{ // Cartesian3::operator /()
	Cartesian3 returnVal(x / factor, y / factor, z / factor);
	return returnVal;
} // Cartesian3::operator /()

  // crossproduct routine
Cartesian3 Cartesian3::cross(const Cartesian3 &other)
{ // Cartesian3::operator ==()
	Cartesian3 returnVal(y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x);
	return returnVal;
} // Cartesian3::operator ==()

  // dot routine
float Cartesian3::dot(const Cartesian3 & other)
{
	return x * other.x + y * other.y + z * other.z;
}

// routine to find the length
float Cartesian3::length()
{ // Cartesian3::length()
	return (x*x + y * y + z * z);
} // Cartesian3::length()

  // normalisation routine
Cartesian3 Cartesian3::normalise()
{ // Cartesian3::normalise()
	float length = sqrt(x*x + y * y + z * z);
	Cartesian3 returnVal(x / length, y / length, z / length);
	return returnVal;
} // Cartesian3::normalise()

Cartesian3 Cartesian3::gammaCorrect()
{
	// Convert to rgb values
	float new_x = x * 255;
	float new_y = y * 255;
	float new_z = z * 255;

	// Clamp values within 0-255 range
	if (new_x > 255) { new_x = 255; }
	if (new_x < 0) { new_x = 0; }
	if (new_y > 255) { new_y = 255; }
	if (new_y < 0) { new_y = 0; }
	if (new_z > 255) { new_z = 255; }
	if (new_z < 0) { new_z = 0; }

	// Apply gamma correction
	new_x = (int)round(255 * pow(x / 255.0, (1 / 2.2)));
	new_y = (int)round(255 * pow(y / 255.0, (1 / 2.2)));
	new_z = (int)round(255 * pow(z / 255.0, (1 / 2.2)));

	// Clamp values within 0-255 range
	if (new_x > 255) { new_x = 255; }
	if (new_x < 0) { new_x = 0; }
	if (new_y > 255) { new_y = 255; }
	if (new_y < 0) { new_y = 0; }
	if (new_z > 255) { new_z = 255; }
	if (new_z < 0) { new_z = 0; }

	return Cartesian3(new_x, new_y, new_z);
}

// stream output
std::ostream & operator << (std::ostream &outStream, Cartesian3 value)
{ // stream output
	outStream << value.x << " " << value.y << " " << value.z;
	return outStream;
} // stream output
