#include "Cartesian3.h"
#include "Triangle.h"
#include "Sphere.h"
#include <math.h>

#pragma once
class Ray
{
public:
	Ray();
	Ray(Cartesian3 o, Cartesian3 d);
	~Ray();

	// Given 3 vertices on a triangle, see if the ray intersects the triangle
	bool triangleIntersect(Cartesian3 A, Cartesian3 B, Cartesian3 C, float &t, Cartesian3 &point);
	bool sphereIntersect(Sphere sphere, float &t1, float &t2, Cartesian3 &point1, Cartesian3 &point2);

	Cartesian3 origin;
	Cartesian3 direction;
};
