#pragma once
#include "Cartesian3.h"
#include "Material.h"
class Sphere
{
public:
	Sphere();
	Sphere(Cartesian3 pos, Cartesian3 col, float radius, float t, float r);
	~Sphere();

	Cartesian3 pos, col;
	float radius, transparency, reflection;
	Material material;
};

