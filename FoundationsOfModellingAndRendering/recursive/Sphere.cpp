#include "Sphere.h"



Sphere::Sphere() : pos(Cartesian3(0, 0, 0)), col(Cartesian3(0, 255, 0)), radius(1), transparency(0.0), reflection(0.0)
{
	material.setEmerald();
}

Sphere::Sphere(Cartesian3 pos, Cartesian3 col, float radius, float r, float t) : pos(pos), col(col), radius(radius), transparency(t), reflection(r)
{
	material.setEmerald();
}


Sphere::~Sphere()
{
}
