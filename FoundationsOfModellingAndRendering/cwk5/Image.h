#include <string>
#include <vector>
#include <fstream>
#include "RGB.h"
#include "MeshData.h"
#include "Eye.h"
#include <iostream>
#include "Cartesian3.h"
#include "Ray.h"
#include "Light.h"
#include "Material.h"
#include <math.h>

#pragma once
class  Image
{
public:
	Image(int pWidth, int pHeight, std::string pFilename, std::string pExtension);
	~Image();

	// Fill image with provided rgb values
	void fillRGB(int r, int g, int b);
	// Set a pixel to a colour
	void setPixel(int x, int y, RGB rgbVal);
	// Write the pixel data to an output.ppm file
	void writeDataToFile();

	// Caclulate the barycentric coordinates
	std::vector<float> barycentricCoordinates(Cartesian3 A, Cartesian3 B, Cartesian3 C, Cartesian3 p);
	// Get the rgb value within some barycentric coordinates
	RGB getRGB(std::vector<float> bary, RGB A, RGB B, RGB C);
	// Calcuate the blinn phong lighting of a given point 
	Cartesian3 calcBlinnPhong(Cartesian3 normal, Light light, Cartesian3 point, Material material, Ray ray);

	void rayCaster();

private:
	int width, height, maxRange;
	float aspectRatio, fov;
	std::string filename;
	std::string extension;
	std::vector<RGB> pixelArray;

	std::vector<Vertex> mesh;
	Eye camera;

};
