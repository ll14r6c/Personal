#include "Cartesian3.h"

#pragma once
class Material
{
public:
	Material();
	~Material();

	Cartesian3 ambient;
	Cartesian3 diffuse;
	Cartesian3 specular;
	float shininess;

	void setEmerald();
	void setPearl();
};
