#include "Cartesian3.h"

#pragma once
class Eye
{
public:
	Eye();
	Eye(int width, int height, float fov);
	~Eye();

	Cartesian3 pos;
	Cartesian3 look;
	Cartesian3 up;

	int imageWidth;
	int imageHeight;
	float FOV = 90.0;

};

