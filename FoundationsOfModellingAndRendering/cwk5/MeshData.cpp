#include "MeshData.h"



MeshData::MeshData()
{
}


MeshData::~MeshData()
{
}

// Store some hardcoded vertices data
std::vector<Vertex> MeshData::getHardcodedData()
{
	verticesArray.clear();

	Vertex A;
	Vertex B;
	Vertex C;

	A.pos.x = 10;
	A.pos.y = 20;
	A.pos.z = 100;

	B.pos.x = 100;
	B.pos.y = 30;
	B.pos.z = 150;

	C.pos.x = 40;
	C.pos.y = 100;
	C.pos.z = 200;

	A.color.r = 255;
	A.color.g = 0;
	A.color.b = 0;

	B.color.r = 0;
	B.color.g = 255;
	B.color.b = 0;

	C.color.r = 0;
	C.color.g = 0;
	C.color.b = 255;

	A.uvCoords.u = 0.5;
	A.uvCoords.v = 1.0;

	B.uvCoords.u = 0.6;
	B.uvCoords.v = 0.5;

	C.uvCoords.u = 0.44434;
	C.uvCoords.v = 0.87186;

	verticesArray.push_back(A);
	verticesArray.push_back(B);
	verticesArray.push_back(C);


	// Add some hardcoded floor values to the vertices array
	Vertex floor1, floor2, floor3;
	Vertex floor4, floor5, floor6;

	floor1.pos = Cartesian3(-99999,-100,-99999);
	floor2.pos = Cartesian3(99999,-100,99999);
	floor3.pos = Cartesian3(-99999,-100,99999);

	floor4.pos = Cartesian3(-99999,-100,-99999);
	floor5.pos = Cartesian3(99999,-100,-99999);
	floor6.pos = Cartesian3(99999,-100,99999);

	verticesArray.push_back(floor1);
	verticesArray.push_back(floor2);
	verticesArray.push_back(floor3);

	verticesArray.push_back(floor4);
	verticesArray.push_back(floor5);
	verticesArray.push_back(floor6);

	return verticesArray;
}
