#include "Image.h"


 Image:: Image(int pWidth, int pHeight, std::string pFilename, std::string pExtension)
{
  // Set image properties
	maxRange = 255;
	width = pWidth;
	height = pHeight;
	filename = pFilename;
	aspectRatio = (float)width / (float)height;
  fov = 90.0;
  camera = Eye(width, height, fov);
	// Check if the given extension is valid
	if (pExtension == "ppm")
	{
		extension = pExtension;
	}
	pixelArray.resize(width*height);

	// Store the hardcoded data into the vector
	MeshData meshWithData;
	mesh = meshWithData.getHardcodedData();
}

 Image::~ Image()
{
}

void  Image::fillRGB(int r, int g, int b)
{
	for (int i = 0; i < (int)pixelArray.size(); i++) {
		RGB newRGB;;
		newRGB.r = r;
		newRGB.g = g;
		newRGB.b = b;
		pixelArray.at(i) = newRGB;
	}

}

void  Image::setPixel(int x, int y, RGB rgbVal)
{
  // Gamma correct the RGB value
  rgbVal.gammaCorrect();
	pixelArray.at(height*((height - 1) - y) + x) = rgbVal;
}

void  Image::writeDataToFile()
{
	std::ofstream myfile;
	myfile.open((filename + "." + extension).c_str());
	myfile << "P3\n# " << filename << "\n" << width << " " << height << "\n" << maxRange << "\n";
	for (auto &row : pixelArray) {
		myfile << row.r << " " << row.g << " " << row.b << "  \n";
	}
	myfile.close();
}

void Image::rayCaster()
{
  // Variables to be used with the distance
	float closest = INFINITY;
  float shadowClosest = INFINITY;
	float distance, shadowDistance;
  // Ray variables for different camera types and shadows
	Ray perspective, shadow;
	std::vector<float> baryCoords;
  // Variables to store the intersection point and the normal
	Cartesian3 point, normal;
  // Create a light and set light position in the scene
	Light light;
	light.lightPosition = Cartesian3(100,150,180);
  // Field of view calculation for perspective camera
  float scale = tan((camera.FOV/2) * 3.14/180);
  aspectRatio = width/(float)height;
  // Create a material instance
	Material material;


	// for every pixel in the image
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			closest = INFINITY;
      shadowClosest = INFINITY;

      // raycast perspective camera, from eye through pixel
      perspective.origin = camera.pos;
      float pi = (2 * (i + 0.5) / (float)width - 1)  * aspectRatio * scale;
      float pj = -(1 - 2 * (j + 0.5) / (float)height) * scale;
      perspective.direction = (Cartesian3(pi, pj, 1).normalise()) - perspective.origin;

			// check intersections for each triangle
			for (int t = 0; t < (int)mesh.size(); t += 3) {
				if (perspective.triangleIntersect(mesh[t].pos, mesh[t+1].pos, mesh[t+2].pos, distance, point)) {
					// If the ray intersects the triangle, and is the shortest distance, get its colour and set the pixel
					if (distance < closest) {

            // Set the first triangle to emerald, otherwise pearl
            // This should be done as a property of the triangle, but for this
            // coursework I have hardcoded this
            if(t == 0){
              material.setEmerald();
            }else{
              material.setPearl();
            }

            // Compute barycentric and store into an RGB
						baryCoords = barycentricCoordinates(mesh[t].pos, mesh[t + 1].pos, mesh[t + 2].pos, point);
						RGB colour = getRGB(baryCoords, mesh[t].color, mesh[t + 1].color, mesh[t + 2].color);
            // Calculate the normal which is faceing upwards
            normal = (mesh[t + 1].pos - mesh[t].pos).cross(mesh[t + 2].pos - mesh[t].pos);
            normal = normal.normalise() * -1;
            // Calculate the Blinn Phong for the material at the point
						Cartesian3 specular = calcBlinnPhong(normal, light, point, material, perspective);
						Cartesian3 result = specular;

            // Shadow ray setup
            // give the origin a tiny increase in the normal direction to avoid shadow acne
            shadow.origin = point + normal * 1e-4;
            shadow.direction = (light.lightPosition - shadow.origin).normalise();
            // Check for intersections will all triangles
            for(int s = 0; s < (int)mesh.size(); s += 3){
              // If there is an intersection and it is not unreasonably close, set to black
              if(shadow.triangleIntersect(mesh[s].pos, mesh[s+1].pos, mesh[s+2].pos, shadowDistance, point)){
                if(shadowDistance < shadowClosest && t > 0.001){
                  result = Cartesian3(); // set point with no view to light black
                  shadowClosest = shadowDistance;
                }
              }
            }
            // Set the pixel to the calculated result
						setPixel(i, j, RGB(round(result.x*255), round(result.y*255), round(result.z*255)));
            // Set the new closest to the new shortest distance
						closest = distance;
					}
				}
			}
		}
	}
}

// Calculate the barycentricCoordinates of a given position p, in a triangle ABC
std::vector<float> Image::barycentricCoordinates(Cartesian3 A, Cartesian3 B, Cartesian3 C, Cartesian3 p) {
	std::vector<float> toReturn;
	float p_CB = (B.y - C.y)*(p.x - C.x) + (C.x - B.x)*(p.y - C.y);
	float A_CB = (B.y - C.y)*(A.x - C.x) + (C.x - B.x)*(A.y - C.y);
	float alpha = p_CB / A_CB;

	float p_AC = (C.y - A.y)*(p.x - C.x) + (A.x - C.x)*(p.y - C.y);
	float B_AC = (B.y - C.y)*(A.x - C.x) + (C.x - B.x)*(A.y - C.y);
	float beta = p_AC / B_AC;

  if(A_CB == 0){
    alpha = 0;
  }

  if(B_AC == 0){
    beta = 0;
  }

	float gamma = 1.0 - alpha - beta;

	toReturn.push_back(alpha);
	toReturn.push_back(beta);
	toReturn.push_back(gamma);

	return toReturn;
}

// Given a set of barycentric coords and some points' colours, return the RGB value
RGB Image::getRGB(std::vector<float> bary, RGB A, RGB B, RGB C) {
	RGB colour;
	colour.r = (int)(A.r * bary[0] + B.r * bary[1] + C.r * bary[2]);
	colour.g = (int)(A.g * bary[0] + B.g * bary[1] + C.g * bary[2]);
	colour.b = (int)(A.b * bary[0] + B.b * bary[1] + C.b * bary[2]);
	return colour;
}

// Given the input parameters calculate blinn phong lighting for the point
Cartesian3 Image::calcBlinnPhong(Cartesian3 normal, Light light, Cartesian3 point, Material material, Ray ray)
{
  // Set the default colours with reagrd to the light and material properties
  Cartesian3 ambientColor = light.ambient * material.ambient;
  Cartesian3 diffuseColor = light.diffuse * material.diffuse;
  Cartesian3 specColor = light.specular * material.specular;
  // Ensure the normal is normalised
  normal = normal.normalise();
  // direction to the light
  Cartesian3 lightDir = (light.lightPosition - point).normalise();

  // Caclulate the diffuse
  float lambertian = lightDir.dot(normal);
  if(lambertian < 0){
    lambertian = 0;
  }

  // Caclulate the specular
  float specular = 0.0;
  if(lambertian > 0.0){
    Cartesian3 vl = (light.lightPosition - point).normalise();
    Cartesian3 ve = (ray.origin - point).normalise();
    // half vector
    Cartesian3 hv = ((vl+ve)/2).normalise();
    float specAngle = hv.dot(normal);
    if(specAngle < 0){
      specAngle = 0;
    }
    specular = pow(specAngle, material.shininess);
  }
  // Return the full result
  Cartesian3 result = ambientColor+ (diffuseColor * lambertian * light.lightPower) + (specColor * specular * light.lightPower);
  return result;
}
