Answers
=======
Qa. black.ppm
Qb. barycentric.ppm
Qc. ambient             = ambient.ppm
    diffuse             = diffuse.ppm
    ambient and diffuse = ambient+diffuse.ppm
    specular            = specular.ppm
    full blinn-phong    = blinnphong.ppm
Qd. Light above triangle= ground+shadow1.ppm, light pos = (100, 150,180)
    Note: Shadow is cast on the above floor because the ground plane is far away
    Light far above     = ground+shadow2.ppm, light pos = (100,1000,100)

How to run
==========
$ qmake cwk4.pro
$ make
$ ./cwk4

Material Properties
===================

Emerald: used for initial triangle

ambient = Cartesian3(0.0215,0.1745,0.0215);
diffuse = Cartesian3(0.07568,0.61424,0.07568);
specular = Cartesian3(0.633,0.727811,0.633);
shininess = 64.0f;

Pearl: used for floor triangles

ambient = Cartesian3(0.25,0.20725,0.20725);
diffuse = Cartesian3(1,0.829,0.829);
specular = Cartesian3(0.296648,0.296648,0.296648);
shininess = 32.0f;

Light properties:

ambient =  Cartesian3(0.5, 0.5, 0.5);
diffuse =  Cartesian3(0.5, 0.5, 0.5);
specular = Cartesian3(1.0, 1.0, 1.0);
lightPower = 1.5;
