#include "Material.h"



Material::Material() : shininess(64)
{
}


Material::~Material()
{
}

// Set some emerald material values
void Material::setEmerald()
{
	ambient = Cartesian3(0.0215,0.1745,0.0215);
	diffuse = Cartesian3(0.07568,0.61424,0.07568);
	specular = Cartesian3(0.633,0.727811,0.633);
	shininess = 64.0f;
}

// Set some pearl material values
void Material::setPearl()
{
	ambient = Cartesian3(0.25,0.20725,0.20725);
	diffuse = Cartesian3(1,0.829,0.829);
	specular = Cartesian3(0.296648,0.296648,0.296648);
	shininess = 32.0f;
}
