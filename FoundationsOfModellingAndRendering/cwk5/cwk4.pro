######################################################################
# Automatically generated by qmake (2.01a) Thu Nov 29 13:04:32 2018
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += Cartesian3.h \
           Eye.h \
           Image.h \
           Light.h \
           Material.h \
           MeshData.h \
           Ray.h \
           RGB.h \
           Vertex.h
SOURCES += Cartesian3.cpp \
           Eye.cpp \
           Image.cpp \
           Light.cpp \
           Material.cpp \
           MeshData.cpp \
           Ray.cpp \
           RGB.cpp \
           Source.cpp \
           Vertex.cpp
