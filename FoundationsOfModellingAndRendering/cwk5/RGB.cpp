#include "RGB.h"



RGB::RGB(): r(0), g(0), b(0)
{
}

RGB::RGB(int r, int g, int b) : r(r), g(g), b(b)
{
}

RGB::~RGB()
{
}

void RGB::gammaCorrect()
{
  r = 255 * pow(((float)r/255), (1/2.2));
  g = 255 * pow(((float)g/255), (1/2.2));
  b = 255 * pow(((float)b/255), (1/2.2));
}
