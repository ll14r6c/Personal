#pragma once
#include <math.h>

class RGB
{
public:
	RGB();
	RGB(int r, int g, int b);
	~RGB();

	void gammaCorrect();

	int r;
	int g;
	int b;

};
