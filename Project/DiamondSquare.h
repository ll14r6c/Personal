#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glu.h>
#include <math.h>

#include "Array2D.h"
#include "Vector.h"

class DiamondSquare{

private:
  // Array2D*dataArray;
  void setCorners();
  GLfloat randNum();
  void doDiamondSquare(GLint);
  GLfloat roughness;
  void diamond(GLint, GLint, GLint, GLfloat);
  void square(GLint, GLint, GLint, GLfloat);
  GLboolean normal = true;
  void setVertices();
  void setTexCoords();
  void setTexCoords2D();
  void setIndices();
  void setNormals();
  void calcSmoothNormalsArray();
  GLfloat * calcSmoothNormal(GLfloat, GLfloat);
  void setIndicesArraySize();
  void setTempIndicesArray();
  GLfloat * verticesArray;
  GLfloat * smoothNormalsArray;
  GLfloat * texCoordsArray;
  GLfloat * texCoordsArrayTwoD;
  GLuint texOne = 0;
  GLuint texTwo = 0;
  unsigned int * indicesArray;
  GLint * tempIndicesArray;
  GLint indicesArraySize;
  GLint colorMethod = 2;
  void freeMemory();

public:
  Array2D*dataArray;
  GLint size, evenSize, tempSize;
  GLfloat tempRoughness;
  // Constructor
  DiamondSquare(int, float);
  // Deconstructor
  ~DiamondSquare();

  void render();

  void printIndices();
  void printTempIndices();
  void setRoughness(GLfloat);
  void toggleOneDColor();
  void toggleTwoDColor();
  void toggleNoColor();
  void toggleNormals();
  void change();
};
