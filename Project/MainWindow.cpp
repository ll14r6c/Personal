#include "MainWindow.h"

// constructor
MainWindow::MainWindow(QWidget * parent): QWidget(parent) {
    // create the window layout top to bottom
    windowLayout = new QBoxLayout(QBoxLayout::LeftToRight, this);
    optionsGroupBox = new QGroupBox("Options ('o' to toggle)");
    optionsLayout = new QBoxLayout(QBoxLayout::TopToBottom);

    // create a terrainWidget instance
    terrainWidget = new TerrainWidget(this);
    // Set the maximum and minimum height of the opengl widget
    terrainWidget->setMinimumHeight(700);
    terrainWidget->setMinimumWidth(1000);

	// add the terrainWidget to the window filling the top 90% of the window
    windowLayout->addWidget(terrainWidget, 90);

    // create a check box to toggle a wireframe and solid surface
    wireframeCheckBox = new QCheckBox("Wireframe",this);
    // Default state to unchecked
    wireframeCheckBox->setCheckState(Qt::Unchecked);
    // add a signal/slot to change the state of the terrain from wirerfame to slid
    connect(wireframeCheckBox, SIGNAL(stateChanged(int)), terrainWidget, SLOT(toggleWireframe(int)));
    // add this widget to the window
    optionsLayout->addWidget(wireframeCheckBox);

    // create a check box to toggle normals
    normalsCheckBox = new QCheckBox("Normals",this);
    // Default state to unchecked
    normalsCheckBox->setCheckState(Qt::Checked);
    // add a signal/slot to toggle if the normals are on or off
    connect(normalsCheckBox, SIGNAL(stateChanged(int)), terrainWidget, SLOT(toggleNormals()));
    // add this widget to the window
    optionsLayout->addWidget(normalsCheckBox);

    // create a group box to group the shading methods
    lightGroupBox = new QGroupBox("Light Position");
    vboxLight = new QVBoxLayout;
    lightSlider = new QSlider(Qt::Horizontal, this);
    lightSlider->setMinimum(-90);
    lightSlider->setMaximum(90);
    lightSlider->setValue(45);
    // add a signal/slot to toggle if the normals are on or off
    connect(lightSlider, SIGNAL(valueChanged(int)), terrainWidget, SLOT(changeLightPosition(int)));
    // add this widget to the window
    vboxLight->addWidget(lightSlider);
    lightGroupBox->setLayout(vboxLight);
    optionsLayout->addWidget(lightGroupBox);

    // create a group box to group the shading methods
    colorGroupBox = new QGroupBox("Color Method");
    // create two radio buttons for the group box
    oneDRadio = new QRadioButton("1D (height)");
    twoDRadio = new QRadioButton("2D (height, gradient)");
    noRadio = new QRadioButton("No color");
    twoDRadio->setChecked(true);
    // create a layout for the group box
    vboxColor = new QVBoxLayout;
    // add the radio buttons to the groupbox
    vboxColor->addWidget(oneDRadio);
    vboxColor->addWidget(twoDRadio);
    vboxColor->addWidget(noRadio);
    vboxColor->addStretch(1);
    // set the groupbox layout to the layout
    colorGroupBox->setLayout(vboxColor);
    optionsLayout->addWidget(colorGroupBox);
    // add signals/slots to tell the terrain widget which shading method to change to
    connect(oneDRadio,SIGNAL(clicked()), terrainWidget, SLOT(colTo1d()));
    connect(twoDRadio,SIGNAL(clicked()), terrainWidget, SLOT(colTo2d()));
    connect(noRadio,SIGNAL(clicked()), terrainWidget, SLOT(colToNone()));

    // create a group box to group the shading methods
    cameraGroupBox = new QGroupBox("Camera Method (Esc to join/exit)");
    // create two radio buttons for the group box
    staticRadio = new QRadioButton("Static");
    staticRadio->setChecked(true);
    spinRadio = new QRadioButton("Spin");
    fpRadio = new QRadioButton("First Person");
    freeRadio = new QRadioButton("Free Roam");
    // create a layout for the group box
    vboxCamera = new QVBoxLayout;
    // add the radio buttons to the groupbox
    vboxCamera->addWidget(staticRadio);
    vboxCamera->addWidget(spinRadio);
    vboxCamera->addWidget(fpRadio);
    vboxCamera->addWidget(freeRadio);
    vboxCamera->addStretch(1);
    // set the groupbox layout to the layout
    cameraGroupBox->setLayout(vboxCamera);
    optionsLayout->addWidget(cameraGroupBox);
    // add signals/slots to tell the terrain widget which shading method to change to
    connect(staticRadio,SIGNAL(clicked()), terrainWidget, SLOT(camToStatic()));
    connect(fpRadio,SIGNAL(clicked()), terrainWidget, SLOT(camToFp()));
    connect(freeRadio,SIGNAL(clicked()), terrainWidget, SLOT(camToFree()));
    connect(spinRadio, SIGNAL(clicked()), terrainWidget, SLOT(spinStatus()));

    // create a timer for use with spinning the world
    timer = new QTimer(this);
	// use signals/slots to update the angle each timer event
    connect(timer, SIGNAL(timeout()), terrainWidget, SLOT(updateAngle()));
	// set the time period of the spinning to 10ms
    timer->start(10);

    // create a spinbox instance to change the size of the terrain
    sizeLabel = new QLabel("Size of world");
    optionsLayout->addWidget(sizeLabel);
    sizeSpinBox = new QSpinBox(this);
    // set the range of the spinbox from 1 to 15
    sizeSpinBox->setRange(1,15);
    // set the default value to 4
    sizeSpinBox->setValue(8);
    // add a signal/slot to change the stored temp value of the terrain
    connect(sizeSpinBox, SIGNAL(valueChanged(int)), terrainWidget, SLOT(sizeChange(int)));
    optionsLayout->addWidget(sizeSpinBox);

    // create a double spinbox to change the roughness of the terrain
    roughnessLabel = new QLabel("Terrain Roughness");
    optionsLayout->addWidget(roughnessLabel);
    roughnessSpinBox = new QDoubleSpinBox(this);
    // set the reange of the spinbox from 0.01 to 5.00
    roughnessSpinBox->setRange(0.01,1.0);
    // set the change in step to 0.05
    roughnessSpinBox->setSingleStep(0.05);
    // set the default value to 0.3
    roughnessSpinBox->setValue(0.3);
    // add a signal/slot to change the stored temp value of the roughness
    connect(roughnessSpinBox, SIGNAL(valueChanged(double)), terrainWidget, SLOT(roughnessChange(double)));
    optionsLayout->addWidget(roughnessSpinBox);

    // Add a button, when pressed, changes the values of the terrain
    // to stored temp values. Then re-draws the new terrain
    generateButton = new QPushButton("Generate");
    connect(generateButton, SIGNAL(clicked()), terrainWidget, SLOT(generate()));
    optionsLayout->addWidget(generateButton);

    optionsGroupBox->setLayout(optionsLayout);
    windowLayout->addWidget(optionsGroupBox);

    // Set focus on the terrainWidget
    terrainWidget->setFocusPolicy(Qt::ClickFocus);
    optionsGroupBox->setFocusPolicy(Qt::ClickFocus);
    //terrainWidget->setFocus();
    terrainWidget->setMouseTracking(true);

  }

// Destructor
MainWindow::~MainWindow() {
    delete windowLayout;
    delete wireframeCheckBox;
    delete normalsCheckBox;
    delete terrainWidget;

    delete twoDRadio;
    delete oneDRadio;
    delete noRadio;
    delete vboxColor;
    delete colorGroupBox;

    delete staticRadio;
    delete spinRadio;
    delete fpRadio;
    delete freeRadio;
    delete vboxCamera;
    delete cameraGroupBox;

    delete lightSlider;
    delete vboxLight;
    delete lightGroupBox;

    delete timer;

    delete sizeSpinBox;
    delete sizeLabel;
    delete roughnessSpinBox;
    delete roughnessLabel;
    delete generateButton;

    delete optionsLayout;
    delete optionsGroupBox;
  }

// resets all the interface elements
void MainWindow::ResetInterface() {
    // now force refresh
    terrainWidget->update();
    update();
  }

void MainWindow::keyPressEvent(QKeyEvent *event){
  if(event->key() == Qt::Key_O){
    options = !options;
    optionsGroupBox->setVisible(options);
  }
}
