#include <GL/glu.h>
#include <stdio.h>
#include <math.h>
#include "Vector.h"
#include "Array2D.h"


class Camera{

private:
  // Variables for the position
  Vector * position;
  // Variables for the target
  Vector * target;

  // Variables to keep track of the mouse for use when rotating
  GLfloat rotX, rotY;

  // Variables to store the render distance of the frustrum
  GLfloat maxDistance;
  GLfloat minDistance = 0.1;

  // integer to keep track of the sizze of the terrain being rendered
  GLint sizeOfTerrain;

  GLint sensitivity = 250;
  GLfloat speed = 0.1;

  void initCamera();
  void stickYtoTerrain();

public:
  // Constructor
  Camera(GLint sizeOfTerrain);
  // Destructor
  ~Camera();

  void view();
  void change(GLint);

  void moveUp();
  void moveDown();
  void moveRight();
  void moveLeft();
  void moveForward();
  void moveBackward();

  void rotate(GLfloat, GLfloat);
  GLint fpCamera = 3;

  GLfloat getX();
  GLfloat getZ();
  void setY(GLfloat);

};
