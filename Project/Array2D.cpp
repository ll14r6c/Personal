#include "Array2D.h"

// Constructor
Array2D::Array2D(int givenSize){
  clock_t t;
  t = clock();
  size = givenSize;
  // Create a square 1d array from the given size,
  arrayofarray = new GLfloat[size*size];
  t = clock() - t;
  printf ("Array2D creation took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
  zeroArray2D();
}

// Deconstructor
Array2D::~Array2D(){
  // Delete the whole array
  delete [] arrayofarray;
}

// Function used when printing a arrayofarray
void Array2D::printArray2D(){
  for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      printf("%-10f", arrayofarray[i*size+j]);
    }
    std::cout << std::endl;
  }
}

// Function used to set all values of the arrayofarray to 0
void Array2D::zeroArray2D(){
  clock_t t;
  t = clock();
  for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      arrayofarray[i*size+j] = 0.0;
    }
  }
  t = clock() - t;
  printf ("Array2D zero-ing took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
}

// Function to set coordinates x,y to value z
void Array2D::set(int x, int y, GLfloat z){
  int indexNumber = (x*size+y);
  arrayofarray[indexNumber] = z;
}

// Function to return a value at a given x,y
GLfloat Array2D::get(int x, int y){
  int indexNumber = (x*size+y);
  return arrayofarray[indexNumber];
}

// Function to get the index of a given x,y
GLfloat Array2D::getIndex(int x, int y){
  return (x*size+y);
}

GLfloat Array2D::getInterpolated(GLfloat givenX, GLfloat givenY){
  GLfloat x = givenX+(size/2);
  GLfloat y = givenY+(size/2);
  GLfloat height;
  GLdouble xInt, yInt, xDec, yDec;
  GLint ax, ay, az, bx, by, bz, cx, cy, cz;
  xDec = modf(x, &xInt);
  yDec = modf(y, &yInt);
  if( xDec + yDec > 1 ){
    ax = (int)ceil(x);
    ay = (int)ceil(y);
  }else{
    ax = (int)floor(x);
    ay = (int)floor(y);
  }
  az = get(ax,ay);
  bx = (int)floor(x);
  by = (int)ceil(y);
  bz = get(bx,by);
  cx = (int)ceil(x);
  cy = (int)floor(y);
  cz = get(cx,cy);
  GLfloat ix, iy, iz, id;
  id = ( ( by-cy )*( ax-cx )+( cx-bx )*( ay-cy ) );
  ix = ( ( by-cy )*( x-cx )+( cx-bx )*( y-cy ) )/id;
  iy = ( ( cy-ay )*( x-cx )+( ax-cx )*( y-cy ) )/id;
  iz = 1-ix-iy;
  height = ( ix*az )+( iy*bz )+( iz*cz );
  return height;
}
