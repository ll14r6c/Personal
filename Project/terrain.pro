TEMPLATE = app
TARGET = Terrain
INCLUDEPATH += . /opt/local/include
QT += widgets opengl gui
LIBS +=  -lglut -lGLU
QMAKE_CXXFLAGS += -std=c++11

# Input
HEADERS += TerrainWidget.h MainWindow.h Array2D.h DiamondSquare.h Camera.h Vector.h
SOURCES += Main.cpp \
           TerrainWidget.cpp \
           MainWindow.cpp \
           Array2D.cpp \
           DiamondSquare.cpp \
           Camera.cpp \
           Vector.cpp
