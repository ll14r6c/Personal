#include "Camera.h"


// Constructor
Camera::Camera(GLint sizeTerrain){
	sizeOfTerrain = sizeTerrain;
	initCamera();
}

// Deconstructor
Camera::~Camera(){
	delete position;
	delete target;
}

void Camera::initCamera(){
	position = new Vector(-(sizeOfTerrain - 1)/2, sizeOfTerrain*0.5, -(sizeOfTerrain - 1)/2);
	target = new Vector((sizeOfTerrain - 1)/2, -sizeOfTerrain*0.5, (sizeOfTerrain - 1)/2);
	maxDistance = sqrt( pow(sizeOfTerrain, 2) + pow(sizeOfTerrain, 2)  ) + sizeOfTerrain;
}

void Camera::view(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-minDistance, minDistance, -minDistance, minDistance, minDistance, maxDistance);
	stickYtoTerrain();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if(fpCamera == 1){ // FIRST PERSON
		gluLookAt(position->x,position->y,position->z,position->x+target->x,position->y+target->y,position->z+target->z,0,1,0);
	}else if(fpCamera == 2){ // FREE CAMERA
		gluLookAt(position->x,position->y,position->z,position->x+target->x,position->y+target->y,position->z+target->z,0,1,0);
	}else{ // STATIC
		gluLookAt(-(sizeOfTerrain - 1)/2, sizeOfTerrain*0.5, -(sizeOfTerrain - 1)/2,(sizeOfTerrain - 1)/2, -sizeOfTerrain*0.5, (sizeOfTerrain - 1)/2,0,1,0);
	}
}

void Camera::change(GLint newSize){
	sizeOfTerrain = newSize;
	initCamera();
}

void Camera::moveUp(){
	position->y += speed;
}

void Camera::moveDown(){
	position->y -= speed;
}

void Camera::moveRight(){
	// move in the the right Vector
	position->x += (speed * -(target->z));
	position->z += (speed * target->x);
}

void Camera::moveLeft(){
	// move in the left Vector
	position->x += (speed * target->z);
	position->z += (speed * -(target->x));
}

void Camera::moveForward(){
	// move into the forward Vector
	position->x += (speed*target->x);
	position->y += (speed*target->y);
	position->z += (speed*target->z);
}

void Camera::moveBackward(){
	// move opposite the forward Vector
	position->x -= (speed*target->x);
	position->y -= (speed*target->y);
	position->z -= (speed*target->z);
}

void Camera::rotate(GLfloat rotationX, GLfloat rotationY){
	rotX += rotationX/sensitivity;
	rotY += rotationY/sensitivity;

	target->x = cos(rotX) * sin(rotY);
	target->y = cos(rotY);
	target->z = sin(rotX) * sin(rotY);
}

// Function to interpolate the x,z to get appropirate y (height) value
// for the camera when 'walking' across the terrain
void Camera::stickYtoTerrain(){
	if(position->x > (sizeOfTerrain/2) || position->x < -(sizeOfTerrain/2)){
		position->x = 0.0;
		printf("Player outside of limits, resetting to center of world\n");
	}
	if(position->z > (sizeOfTerrain/2) || position->z < -(sizeOfTerrain/2)){
		position->z = 0.0;
		printf("Player outside of limits, resetting to center of world\n");
	}
}

GLfloat Camera::getX(){
	return position->x;
}

GLfloat Camera::getZ(){
	return position->z;
}

void Camera::setY(GLfloat givenY){
	position->y = givenY;
}
