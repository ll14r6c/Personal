#include "DiamondSquare.h"

// Constructor
DiamondSquare::DiamondSquare(int givenSize, float givenRoughness){
	clock_t t, tStart;
	tStart = clock();
	roughness = givenRoughness;
	tempRoughness = roughness;
	evenSize = pow(2,givenSize);
	size = evenSize + 1;
	tempSize = size;
	dataArray = new Array2D(size);
	t = clock() - tStart;
	printf ("DiamondSquare set variable settings took     (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setCorners();
	doDiamondSquare(evenSize);
	t = clock() - t;
	printf ("DiamondSquare recursive method took          (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setVertices();
	t = clock() - t;
	printf ("DiamondSquare setting vertices array took    (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setIndices();
	t = clock() - t;
	printf ("DiamondSquare setting Indices array took     (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	calcSmoothNormalsArray();
	t = clock() - t;
	printf ("DiamondSquare normals array creation took    (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setTexCoords();
	t = clock() - t;
	printf ("DiamondSquare 1d texture array creation took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setTexCoords2D();
	t = clock() - t;
	printf ("DiamondSquare 2d texture array creation took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock() - tStart;
	printf ("DiamondSquare TOTAL creation took            (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	printf("%d triangles rendered\n\n", evenSize*evenSize*2);
}

// Deconstructor
DiamondSquare::~DiamondSquare(){
  freeMemory();
}

void DiamondSquare::freeMemory(){
	delete dataArray;
	delete verticesArray;
	delete smoothNormalsArray;
	delete texCoordsArray;
	delete texCoordsArrayTwoD;
	delete indicesArray;
	delete tempIndicesArray;
}

void DiamondSquare::change(){
	freeMemory();
	clock_t t, tStart;
	tStart = t = clock();
	dataArray = new Array2D(size);
	t = clock() - tStart;
	printf ("DiamondSquare set variable settings took     (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setCorners();
	doDiamondSquare(evenSize);
	t = clock() - t;
	printf ("DiamondSquare recursive method took          (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setVertices();
	t = clock() - t;
	printf ("DiamondSquare setting vertices array took    (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setIndices();
	t = clock() - t;
	printf ("DiamondSquare setting Indices array took     (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	calcSmoothNormalsArray();
	t = clock() - t;
	printf ("DiamondSquare normals array creation took    (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setTexCoords();
	t = clock() - t;
	printf ("DiamondSquare 1d texture array creation took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock();
	setTexCoords2D();
	t = clock() - t;
	printf ("DiamondSquare 2d texture array creation took (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	t = clock() - tStart;
	printf ("DiamondSquare TOTAL creation took            (%f seconds).\n",((float)t)/CLOCKS_PER_SEC);
	printf("%d triangles rendered\n\n", evenSize*evenSize*2);
}


void DiamondSquare::setCorners(){
	dataArray->set(0        ,0        , randNum());
  dataArray->set(evenSize ,0        , randNum());
  dataArray->set(0        ,evenSize , randNum());
  dataArray->set(evenSize ,evenSize , randNum());
}

// Function to toggle color on and off (1d)
void DiamondSquare::toggleOneDColor(){
	colorMethod = 1;
}

// Function to toggle color on and off (2d)
void DiamondSquare::toggleTwoDColor(){
	colorMethod = 2;
}

// Function to toggle color on and off
void DiamondSquare::toggleNoColor(){
	colorMethod = 3;
}

// Function to toggle normals
void DiamondSquare::toggleNormals(){
	normal = !normal;
}

// Method used to return a random number with range 0-x
GLfloat DiamondSquare::randNum(){
	return static_cast < GLfloat > (rand()) / static_cast < GLfloat > (RAND_MAX / 1);
}


// Recursive function to do the diamond square algorith,
void DiamondSquare::doDiamondSquare(GLint eSize){
	// Set the x,y and half equal to half the given eSize
  GLint x, y, half = eSize / 2;
	// set the scale to the fixed roughness * given eSize
  GLfloat scale = roughness * eSize;
	// If the size of half (half given parameter size) < 1, do not continue
	// This means the recursion is to not continue because the its got to the
	// smallest square value
  if (half < 1){
		return;
	}else{
		// Subdivide the sqaure into multiple sqaures by iterating over
		// the x and y, adding the step size (eSize) to get into each new square
		for (y = half; y < evenSize; y += eSize) {
	    for (x = half; x < evenSize; x += eSize) {
	      square(x, y, half, randNum()  * scale * 2 - scale);
	    }
	  }
		// Subdivide the sqaure into multiple sqaures by iterating over
		// the x and y, adding half of the step size to get to the
		// center of the square to pass to the diamond function
	  for (y = 0; y <= evenSize; y += half) {
	    for (x = (y + half) % eSize; x <= evenSize; x += eSize) {
	      diamond(x, y, half, randNum()  * scale * 2 - scale);
	    }
	  }
		// Recursivly do this until the squares are 1 in length
	  doDiamondSquare(eSize / 2);
	}
}

void DiamondSquare::diamond(GLint x, GLint y, GLint aSize, GLfloat offset) {
	// Initialize the values for each corner
	GLfloat top, right, bottom, left, average;
	top = right = bottom = left = average = 0.0;

	if(x-aSize < 0){ // top top
		left =     dataArray->get(x           ,y - aSize);
		bottom =   dataArray->get(x   + aSize,y);
		right =  dataArray->get(x ,y  + aSize       );
		average = ((right+bottom+left)/3.0);
		// printf("Diamond: %f + %f + %f =  %f\n\n", right, bottom, left, average+offset);
		dataArray->set(x,y, average + offset);
	}else if(y+aSize > evenSize){ // right right
		left =     dataArray->get(x           ,y - aSize);
		bottom =   dataArray->get(x   + aSize,y);
		top = 		dataArray->get(x - aSize ,y);
		average = ((top+right+bottom+left)/3.0);
		// printf("Diamond: %f + %f + %f =  %f\n\n", top, bottom, left, average+offset);
		dataArray->set(x,y, average + offset);
	}else if(x+aSize > evenSize){ // bottom bottom
		left =     dataArray->get(x           ,y - aSize);
		right =  dataArray->get(x ,y  + aSize       );
		top = 		dataArray->get(x - aSize ,y);
		average = ((top+right+left)/3.0);
		// printf("Diamond: %f + %f + %f =  %f\n\n", top, right, left, average+offset);
		dataArray->set(x,y, average + offset);
	}else if(y-aSize < 0){ // left left
		bottom =   dataArray->get(x   + aSize,y);
		right =  dataArray->get(x ,y  + aSize       );
		top = 		dataArray->get(x - aSize ,y);
		average = ((top+right+bottom)/3.0);
		// printf("Diamond: %f + %f + %f =  %f\n\n", top, right, bottom, average+offset);
		dataArray->set(x,y, average + offset);
	}else{
		// Get values for for the corners of the sqaure
		left =     dataArray->get(x           ,y - aSize);
		bottom =   dataArray->get(x   + aSize,y);
		right =  dataArray->get(x ,y  + aSize       );
		top = 		dataArray->get(x - aSize ,y);
		average = ((top+right+bottom+left)/4.0);
		// printf("Diamond: %f + %f + %f + %f =  %f\n\n", top, right, bottom, left, average+offset);
		dataArray->set(x,y, average + offset);
	}

}

void DiamondSquare::square(GLint x, GLint y, GLint aSize, GLfloat offset) {
	  // Initialize the values for each corner
	  GLfloat topL, topR, bottomL, bottomR, average;
	  topL = topR = bottomL = bottomR = average = 0.0;
	  // Get values for for the corners of the sqaure
	  topL =     dataArray->get(x-aSize,y-aSize);
	  topR =   dataArray->get(x-aSize ,y+aSize          );
	  bottomL =  dataArray->get(x+aSize,y-aSize);
	  bottomR = 		dataArray->get(x+aSize ,y+aSize);
		average = ((topL+topR+bottomL+bottomR)/4.0);
		// printf("Square: %f + %f + %f + %f =  %f + %f = %f\n\n", topL, topR, bottomL, bottomR, average, offset, average+offset);
		dataArray->set(x,y, average + offset);
}



void DiamondSquare::render(){
	glClear( GL_COLOR_BUFFER_BIT );
	if(colorMethod == 1){ // RENDER ONE DIMENSIONAL COLOR
		glEnable( GL_TEXTURE_1D );
		glBindTexture( GL_TEXTURE_1D, texOne );
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glTexCoordPointer( 1, GL_FLOAT, 0, texCoordsArray );
	}else if(colorMethod == 2){ // RENDER TWO DIMENSIONAL COLOR
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, texTwo );
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glTexCoordPointer( 2, GL_FLOAT, 0, texCoordsArrayTwoD );
	}
	if(normal){ // RENDER NORMALS
		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer( GL_FLOAT, 0, smoothNormalsArray );
	}

	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 3, GL_FLOAT, 0, verticesArray );
	glDrawElements( GL_TRIANGLES, indicesArraySize, GL_UNSIGNED_INT, indicesArray );
	glDisableClientState( GL_VERTEX_ARRAY );

	if(colorMethod == 1){ // DISABLE 1D COLOR
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		glBindTexture( GL_TEXTURE_1D, 0 );
		glDisable( GL_TEXTURE_1D );
	}else if(colorMethod == 2){ // DISABLE 2D COLOR
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		glBindTexture( GL_TEXTURE_2D, 0 );
		glDisable( GL_TEXTURE_2D );
	}
	if(normal){
		glDisableClientState( GL_NORMAL_ARRAY );
	}
}


void DiamondSquare::setVertices(){
  int counter = 0;
  int counter2 = 0;

  verticesArray = new GLfloat[((size*size)*3)];

  for(int i=0; i<size; ++i)
  {
      for(int j=0; j<size; ++j)
      {
        verticesArray[counter2] = i;
        verticesArray[counter2+1] = dataArray->get(i,j);
        verticesArray[counter2+2] = j;
				//printf("%f, %f, %f,  ", verticesArray[counter2], verticesArray[counter2+1], verticesArray[counter2+2]);
        counter = counter + 1;
        counter2 = counter2 + 3;
      }
			//printf("\n" );
  }
}


void DiamondSquare::setIndices(){

  setIndicesArraySize();
  setTempIndicesArray();

  indicesArray = new unsigned int[indicesArraySize];

  int numberToRemove = (size-2)*2;
  int multiplier = 2;
  int removeInt1;
  int removeInt2;
  int * indexToRemove = new int[numberToRemove];
  int counter = 0;

  while(numberToRemove > 0){
    removeInt1 = size * multiplier;
    removeInt2 = removeInt1 - 1;
    indexToRemove[counter] = removeInt2-1;
    indexToRemove[counter+1] = removeInt1-1;
    multiplier = multiplier + 2;
    numberToRemove = numberToRemove - 2;
    counter = counter + 2;
  }

  int removeCounter = 0;
  counter = 0;
  for(int i=0; i<(size*(size*2)-(2*size)); ++i)
  {
    if(i<(size*(size*2)-(2*size))-2 && i != indexToRemove[removeCounter]){
      indicesArray[counter] = tempIndicesArray[i];
      indicesArray[counter+1] = tempIndicesArray[i+1];
      indicesArray[counter+2] = tempIndicesArray[i+2];
      // BEFORE WALK YOU WERE LOOKING AT REMOVING INDICES AUTOMATICALLY FROM THE ABOVE LOOP
      counter = counter + 3;
    }else{
      //printf("removed: %d %u\n", i, indexToRemove[removeCounter]);
      removeCounter = removeCounter + 1;
    }
  }
  //removeEdgeIndices();
  // printIndices();
}


void DiamondSquare::setIndicesArraySize(){
  // The below loop finds the size of the indicesArray
  indicesArraySize = 0;
  for(int i=0; i<size*(2*(size-1)); ++i)
  {
    // if i is not past the 3rd last point, then add 3 to the size counter
    if(i<(size*(2*(size-1)))-2){
      indicesArraySize = indicesArraySize + 3;
    }
  }
  indicesArraySize = indicesArraySize - (((size-2)*2)*3);

  //indicesArraySize = indicesArraySize - 192;
}

void DiamondSquare::setTempIndicesArray(){
  // The below loop
  int counter = 0;
  int counter2 = 0;
  int tempIndicesArraySize = ((size*2)*evenSize);
  tempIndicesArray = new GLint[tempIndicesArraySize];

  for(int i=0; i<size; ++i)
  {
      for(int j=0; j<size; ++j)
      {
        // if i is not on the last row
        if( i < size-1){
          tempIndicesArray[counter2] = counter;
          tempIndicesArray[counter2+1] = counter+size;
        }
        counter = counter + 1;
        counter2 = counter2 + 2;
      }
  }
}

void DiamondSquare::printIndices(){
  printf("Printing Indices, size: %i\n", indicesArraySize);
  int counter = 0;
  for(int i = 0; i < indicesArraySize; ++i){
		printf("%i, ", indicesArray[i]);
    if(counter == 2){
      std::cout << "\n";
      counter = -1;
    }
    counter = counter + 1;
  }
}

void DiamondSquare::printTempIndices(){
  std::cout << "Printing tempIndicesArrayindices\n\n";
  int counter = 0;
  for(int i = 0; i < ((size*2)*evenSize); ++i){
    printf("%u ",tempIndicesArray[i]);
    if(counter == size-1){
      std::cout << "\n";
      counter = -1;
    }
    counter = counter + 1;
  }
}


void DiamondSquare::setRoughness(GLfloat x){
	roughness = x;
}

void DiamondSquare::calcSmoothNormalsArray(){

	smoothNormalsArray = new GLfloat[((size*size)*3)];
	GLfloat * vertexNormal;
	for(int i = 0; i < (size*size)*3; i+=3){
		vertexNormal = calcSmoothNormal(verticesArray[i],verticesArray[i+2]);
		smoothNormalsArray[i] = vertexNormal[0];
		smoothNormalsArray[i+1] = vertexNormal[1];
		smoothNormalsArray[i+2] = vertexNormal[2];
	}
}

// Function, given a vertex, calculates an returns a normal
GLfloat * DiamondSquare::calcSmoothNormal(GLfloat x, GLfloat z){
	// Initialize arrays to store the x and z of the given vertex
	// x = length
	// z = width
	// y = height
	GLint left[2] = 	{0};
	GLint right[2] = 	{0};
	GLint top[2] = 		{0};
	GLint bottom[2] = {0};

	// If statements to check if the vertex has a neighbour
	// If there is no neighbour, set the divider to 1
	// and set the x,z of the missing neighbour to the given vertex
	// This is to deal with edge cases


	// If the vertex is missing a neigbour square
	// set the horzontal or vertical divider to 1
	// Otherwise use the default of 2
	GLint vDivider, hDivider;
	vDivider = hDivider = 2;

	if(x-1 < 0){ // check if a top neighbour exists
		top[0] = static_cast<int>(x);
		top[1] = static_cast<int>(z);
		vDivider = 1;
	}else{
		top[0] = static_cast<int>(x)-1;
		top[1] = static_cast<int>(z);
	}

	if(x+1 > evenSize){ // check if a bottom neighbour exists
		bottom[0] = static_cast<int>(x);
		bottom[1] = static_cast<int>(z);
		vDivider = 1;
	}else{
		bottom[0] = static_cast<int>(x)+1;
		bottom[1] = static_cast<int>(z);
	}

	if(z-1 < 0){ // check if a left neighbour exists
		left[0] = static_cast<int>(x);
		left[1] = static_cast<int>(z);
		hDivider = 1;
	}else{
		left[0] = static_cast<int>(x);
		left[1] = static_cast<int>(z)-1;
	}

	if(z+1 > evenSize){ // check if a right neighbour exists
		right[0] = static_cast<int>(x);
		right[1] = static_cast<int>(z);
		hDivider = 1;
	}else{
		right[0] = static_cast<int>(x);
		right[1] = static_cast<int>(z)+1;
	}

	// get the height of the neighbouring vertices
	GLfloat heightLeft, heightRight, heightTop, heightBottom;
	heightLeft   = 	dataArray->get(left[0]  ,left[1]  );
	heightRight  = 	dataArray->get(right[0] ,right[1] );
	heightTop    = 	dataArray->get(top[0]   ,top[1]   );
	heightBottom =  dataArray->get(bottom[0],bottom[1]);
	//printf("left: %d %d \nright: %d %d\ntop: %d %d\nbot: %d %d\n\n", left[0]  ,left[1] , right[0]  ,right[1] , top[0]  ,top[1] , bottom[0]  ,bottom[1] );
	// use the vector class to get the calculated normalised normal values
	// The variables passed to the normalVec are central differenced
	Vector normalVec((heightLeft-heightRight)/hDivider, 2.0, (heightBottom-heightTop)/vDivider);
	static GLfloat vertexNormal[3];
	vertexNormal[0] = normalVec.nX;
	vertexNormal[1] = normalVec.nY;
	vertexNormal[2] = normalVec.nZ;
	return vertexNormal;
}

void DiamondSquare::setTexCoords(){
	GLfloat max, min;
	max = -100;
	min = 100;
	for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      if(dataArray->get(i,j) > max){
				max = dataArray->get(i,j);
			}else if(dataArray->get(i,j) < min){
				min = dataArray->get(i,j);
			}
    }
  }

	GLint counter = 0;
	texCoordsArray = new GLfloat[(size*size)];
	for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
			texCoordsArray[counter] = (dataArray->get(i,j) - min) / (max - min);
			counter += 1;
    }
  }

	GLfloat dataOne[] = {
    0.275, 0.510, 0.706, // water
    0.941, 0.902, 0.549, // sand
    0.471, 0.282, 0.000, // dirt
		0.420, 0.557, 0.137, // grass
    0.663, 0.663, 0.663, // rock
		1.000, 0.980, 0.980, // snow
  };

	glGenTextures( 1, &texOne );
  glBindTexture( GL_TEXTURE_1D, texOne );
  glTexImage1D( GL_TEXTURE_1D, 0, GL_RGB, 6, 0, GL_RGB, GL_FLOAT, dataOne );
  glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); // turns blue on top without this
  glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
}

void DiamondSquare::setTexCoords2D(){
	GLfloat max, min, normaliser;
	max = -100;
	min = 100;
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			if(dataArray->get(i,j) > max){
				max = dataArray->get(i,j);
			}else if(dataArray->get(i,j) < min){
				min = dataArray->get(i,j);
			}
		}
	}
	normaliser = max-min;
	GLint counter = 0;
	GLint normalCounter = 0;
	texCoordsArrayTwoD = new GLfloat[(size*size)*2];
	Vector vertexNormal(0,0,0);
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			texCoordsArrayTwoD[counter] = ((dataArray->get(i,j) - min) / normaliser) + (randNum()/50);
			//Vector vertexNormal(smoothNormalsArray[normalCounter],smoothNormalsArray[normalCounter+1],smoothNormalsArray[normalCounter+2]);
			vertexNormal.setAll(smoothNormalsArray[normalCounter],smoothNormalsArray[normalCounter+1],smoothNormalsArray[normalCounter+2]);
			texCoordsArrayTwoD[counter+1] = ( ( 90-vertexNormal.getSlope() ) / 90 );
			counter += 2;
			normalCounter += 3;
		}
	}

	GLfloat dataTwo[] = {
		0.275, 0.510, 0.706, // water
    0.941, 0.902, 0.549, // sand
    0.471, 0.282, 0.000, // dir	t
		0.420, 0.557, 0.137, // grass
    0.663, 0.663, 0.663, // rock
		1.000, 0.980, 0.980, // snow

		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
	};

	GLfloat dataThree[] = {
		0.0, 0.1, 0.13,
    0.13, 0.44, 0.55,
    0.47, 0.62, 0.24,
		0.47, 0.62, 0.24,
		0.91, 0.75, 0.51,
		1.000, 1.0, 1.0,

		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
		0.663, 0.663, 0.663, // rock
	};

	glGenTextures( 1, &texTwo );
	glBindTexture( GL_TEXTURE_2D, texTwo );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, 6, 2, 0, GL_RGB, GL_FLOAT, dataThree );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
}
