#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glu.h>
#include <time.h>
#include <math.h>

// Declaring a Array2D class. This will be used to store a 2d array dataset
// I have an overloaded operator which allows cleaner access in the 2d array

// Create a 5x5 Array2D:                       Array2D myMat(5)
// Access the second row and third column:    myMat(2,3);

#ifndef __ARRAY2D__H
#define __ARRAY2D__H

class Array2D{

private:
  int size;
  GLfloat*arrayofarray;

public:
  // Constructor
  Array2D(int size);
  // Deconstructor
  ~Array2D();
  // Overloaded operator() for single array
  // Given two integers, will return the value at that index
  // Example: myMat(2,3) will return 2nd row 3rd column value
  GLfloat operator()(int x, int y){
    int indexNumber = (x*size+y);
    std::cout<< arrayofarray[indexNumber] << "\n";
    return arrayofarray[indexNumber];
  }


  void printArray2D();
  void zeroArray2D();
  int getSize();
  void set(int x, int y, GLfloat z);
  GLfloat get(int x, int y);
  GLfloat getIndex(int x, int y);
  GLfloat getInterpolated(GLfloat x, GLfloat y);
};

#endif
