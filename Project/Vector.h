#include <GL/glu.h>
#include <stdio.h>
#include <math.h>

#ifndef __VECTOR__H
#define __VECTOR__H

class Vector{

public:

  GLfloat x;
  GLfloat y;
  GLfloat z;
  GLfloat nX;
  GLfloat nY;
  GLfloat nZ;
  GLfloat magnitude;


  // Constructor
  Vector(GLfloat x, GLfloat y, GLfloat z);
  // Destructor
  ~Vector();

  // Getters
  GLfloat getX();
  GLfloat getY();
  GLfloat getZ();

  // Setters
  void setX(GLfloat);
  void setY(GLfloat);
  void setZ(GLfloat);
  void setAll(GLfloat,GLfloat,GLfloat);

  // Function to normalise a vector
  void setNormalised();

  // Function to get the length of a vector
  GLfloat getMagnitude();

  GLfloat getSlope();

};

#endif
