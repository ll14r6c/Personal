#include <QApplication>
#include <QVBoxLayout>

#include "MainWindow.h"

int main(int argc, char * argv[]) {

    // create the application
    QApplication app(argc, argv);
	// Create a MainWindow instance
    MainWindow * window = new MainWindow(NULL);
    // resize the window
    window->resize(800, 800);
    // show the label
    window->show();
    // start it running
    app.exec();
    //	delete controller;
    delete window;
    // return to caller
    return 0;
  }
