#include <QGLWidget>
#include <math.h>
#include <stdio.h>
#include <QKeyEvent>
#include "DiamondSquare.h"
#include "Camera.h"

class TerrainWidget: public QGLWidget {
  Q_OBJECT
  public:
    // constructor/destructor
    TerrainWidget(QWidget * parent);
  	~TerrainWidget();

	// slots to be used with all the QWidgets
  public slots:
    void updateAngle();
    void spinStatus();
    void sizeChange(int x);
    void roughnessChange(double);
    void generate();
    void toggleWireframe(int x);
    void colTo1d();
    void colTo2d();
    void colToNone();
    void toggleNormals();
    void camToFp();
    void camToFree();
    void camToStatic();
    void changeLightPosition(int);

  protected:
    // called when OpenGL context is set up
    void initializeGL();
    // called every time the widget is resized
    void resizeGL(int w, int h);
    // called every time the widget needs painting
    void paintGL();

    void keyPressEvent(QKeyEvent *);
    void mouseMoveEvent(QMouseEvent *);

  private:
    int angle;
    bool worldSpin = GL_FALSE;
    DiamondSquare * ds;
    // 1 = smooth, 2 = flat
    int shadeMethod = 1;
    Camera * cam;
    GLfloat changeX;
    GLfloat changeY;
    GLboolean center = GL_FALSE;
    GLint lightRotation = 45;
    GLint frames = 0;
    GLfloat timecounter = 0;
    GLfloat totalFPS = 0;
    GLfloat totaltime = 0;

};
