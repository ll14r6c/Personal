#include <QGLWidget>
#include <QBoxLayout>
#include <QTimer>
#include <QCheckBox>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QSlider>
#include "TerrainWidget.h"

class MainWindow: public QWidget {

protected:
  void keyPressEvent(QKeyEvent *);

private:
  bool options = true;

public:

  // constructor / destructor
    MainWindow(QWidget * parent);
		~MainWindow();

  // window layout
  QBoxLayout * windowLayout;
  QGroupBox * optionsGroupBox;
  QBoxLayout * optionsLayout;

// beneath that, the main widget
  TerrainWidget * terrainWidget;

// Toggle switch for the wireframe
  QCheckBox * wireframeCheckBox;

// Toggle switch for the normals
  QCheckBox * normalsCheckBox;

// Options for changing the color method
  QGroupBox * colorGroupBox;
  QVBoxLayout * vboxColor;
  QRadioButton * twoDRadio;
  QRadioButton * oneDRadio;
  QRadioButton * noRadio;

// Options for changing the camera method
  QGroupBox * cameraGroupBox;
  QVBoxLayout * vboxCamera;
  QRadioButton * staticRadio;
  QRadioButton * spinRadio;
  QRadioButton * fpRadio;
  QRadioButton * freeRadio;

// Slider for changing the position of the light
  QGroupBox * lightGroupBox;
  QVBoxLayout * vboxLight;
  QSlider * lightSlider;

  // timer for animation
  QTimer * timer;

  // spinbox for size of the terrain
  QSpinBox * sizeSpinBox;
  QLabel * sizeLabel;
  QDoubleSpinBox * roughnessSpinBox;
  QLabel * roughnessLabel;

  // Button, when pressed, re-draws the terrain with the set constraints
  QPushButton * generateButton;

  // resets all the interface elements
  void ResetInterface();
};
