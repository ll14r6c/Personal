#include <GL/glu.h>
#include <QGLWidget>
#include <iostream>
#include "TerrainWidget.h"

// constructor
TerrainWidget::TerrainWidget(QWidget * parent): QGLWidget(parent),
  angle(0){
  ds = new DiamondSquare(8,0.3);
  cam = new Camera(ds->size);
}


// Destructor
TerrainWidget::~TerrainWidget() {
  delete ds;
  delete cam;
}

// called when OpenGL context is set up
void TerrainWidget::initializeGL() {
    // set the widget background colour to a mid-gray
    glClearColor(0.45, 0.65, 1.0, 0.0);
    glShadeModel(GL_SMOOTH);
    generate();
  }

void TerrainWidget::keyPressEvent(QKeyEvent *event){
  if(event->key() == Qt::Key_W){
    cam->moveForward();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_A){
    cam->moveLeft();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_S){
    cam->moveBackward();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_D){
    cam->moveRight();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_R){
    cam->moveUp();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_F){
    cam->moveDown();
    if(cam->fpCamera == 1){
      cam->setY( ds->dataArray->getInterpolated(cam->getX(), cam->getZ()) + 3 );
    }
  }else if(event->key() == Qt::Key_Escape){
    center = !center;
  }
  this->repaint();
}

void TerrainWidget::mouseMoveEvent(QMouseEvent *event){
  QCursor curs = cursor();
  if(center && (cam->fpCamera == 1 || cam->fpCamera == 2)){
    curs.setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
    curs.setShape(Qt::BlankCursor);
    setCursor(curs);
    changeX = event->x() - changeX;
    changeY = event->y() - changeY;
    cam->rotate(changeX, changeY);
    this->repaint();
    changeX = width()/2;
    changeY = height()/2;
  }else{
    curs.setShape(Qt::ArrowCursor);
    setCursor(curs);
  }
}

// Slot used to increase the rotation angle when spinning
void TerrainWidget::updateAngle() {
  if(worldSpin){
    // set the timeangle
    angle += 1.;
    if(angle == 360){
      angle = 0;
    }
    this->repaint();
  }
}

// Slot used to change the spin status
void TerrainWidget::spinStatus() {
  camToStatic();
  worldSpin = true;
  this->repaint();
}

// Slot used to change the camera to first person
void TerrainWidget::camToFp() {
  worldSpin = false;
  angle = 0;
  cam->fpCamera = 1;
  this->repaint();
}

// Slot used to change the camera to free roam
void TerrainWidget::camToFree() {
  worldSpin = false;
  cam->fpCamera = 2;
  this->repaint();
}

// Slot used to change the camera to static view
void TerrainWidget::camToStatic() {
  worldSpin = false;
  angle = 0;
  cam->fpCamera = 3;
  this->repaint();
}

void TerrainWidget::changeLightPosition(int x){
  lightRotation = x;
  this->repaint();
}

// Slot used when toggling the wireframe on and off
void TerrainWidget::toggleWireframe(int x){
  // 2 = on
  // 0 = off
  if(x == 2){
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  }else{
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  }
  this->repaint();
}

// Slot used to toggle one dimensional color on or off in the ds render
void TerrainWidget::colTo1d(){
  ds->toggleOneDColor();
  this->repaint();
}

// Slot used to toggle two dimensional color on or off in the ds render
void TerrainWidget::colTo2d(){
  ds->toggleTwoDColor();
  this->repaint();
}

// Slot used to toggle color off
void TerrainWidget::colToNone(){
  ds->toggleNoColor();
  this->repaint();
}

void TerrainWidget::toggleNormals(){
  ds->toggleNormals();
  this->repaint();
}

// Slot used for when the first slider increased
void TerrainWidget::sizeChange(int x) {
  ds->tempSize = pow(2,x) + 1;
}

// Slot used to change the tempRoughness to the user interface value
void TerrainWidget::roughnessChange(double x){
  ds->tempRoughness = (float) x;
}

// Slot used to generate with the new values
void TerrainWidget::generate(){
  totalFPS = 0;
  totaltime = 0;
  ds->setRoughness(ds->tempRoughness);
  ds->size = ds->tempSize;
  ds->evenSize = ds->size - 1;
  ds->change();
  cam->change(ds->size);
  angle = 0;
  this->repaint();
}

// called every time the widget is resized
void TerrainWidget::resizeGL(int w, int h) {
    // set the viewport to the entire widget
    glViewport(0, 0, w, h);
		// set the light position
    GLfloat light_pos[] = {0,1,0,0};
		// enable lighting
    glEnable(GL_LIGHTING);
		// enable the light source
    glEnable(GL_LIGHT0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

		// set the position of the light in the scene
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    //glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 360);
  }

// called every time the widget needs painting
void TerrainWidget::paintGL() {
    frames++;
    totaltime++;
    clock_t tEnd, tStart;
    tStart = clock();
    // clear the widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    cam->view();
    // You must set the matrix mode to model view directly before enabling the depth test
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glPushMatrix();
      glRotatef(lightRotation, 1, 0, 0);
	    GLfloat light_pos[] = {0,1,0,0};
	    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glPopMatrix();

    glPushMatrix();
      glRotatef(angle, 0.0, 1.0, 0.0);
      glTranslatef(-ds->size/2,0,-ds->size/2);
      ds->render();
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // flush to screen
    glFlush();
    tEnd = clock() - tStart;
    timecounter += tEnd/(double)CLOCKS_PER_SEC;
    totalFPS += frames/timecounter;
    if(timecounter > 0.01){
      // printf ("FPS: (%f) Av: (%f)\n",( frames/timecounter ), totalFPS/totaltime  );
      timecounter = 0;
      frames = 0;
    }
  }
