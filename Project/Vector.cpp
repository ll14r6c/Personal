#include "Vector.h"

// Constructor
Vector::Vector(GLfloat givenX, GLfloat givenY, GLfloat givenZ){
  x = givenX;
  y = givenY;
  z = givenZ;
  setNormalised();
}

// Deconstructor
Vector::~Vector(){
}

// Getters
GLfloat Vector::getX(){
  return x;
}

GLfloat Vector::getY(){
  return y;
}

GLfloat Vector::getZ(){
  return z;
}

// Setters
void Vector::setX(GLfloat givenX){
  x = givenX;
}

void Vector::setY(GLfloat givenY){
  y = givenY;
}

void Vector::setZ(GLfloat givenZ){
  z = givenZ;
}

void Vector::setAll(GLfloat givenX,GLfloat givenY,GLfloat givenZ){
  x = givenX;
  y = givenY;
  z = givenZ;
	setNormalised();
}

// Function to normalise a vector
void Vector::setNormalised(){
  magnitude = getMagnitude();
  nX = x/magnitude;
  nY = y/magnitude;
  nZ = z/magnitude;
}

// Function to get the length of a vector
GLfloat Vector::getMagnitude(){
  return ( sqrt( (pow(x,2) + pow(y,2) + pow(z,2)) ) );
}

GLfloat Vector::getSlope(){
	GLfloat slope, rise, run;
	rise = nY;
	run = sqrt( (pow(nX,2) + pow(nZ,2)) );
	slope = atan (rise/run) * 180 / 3.14;
	return slope;
}
