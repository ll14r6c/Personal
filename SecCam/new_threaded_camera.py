import cv2
from threading import Thread, Timer
import queue
from time import clock
from datetime import datetime
import numpy as np
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Function to read the queue and display the output to the user
# The output will only be updated if the recorder is not currently recording
def viewer():
    global is_currently_recording
    fgbg = cv2.createBackgroundSubtractorMOG2()
    while writer_thread.isAlive():
        if not is_currently_recording:
            im1 = fgbg.apply(currentFrame)
            cv2.imshow('frame',im1)
            # cv2.imshow('frame',queue_item)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    cv2.destroyAllWindows()

# Function to monitor the video for movement
# If it detects movement, recording state is set to true
def monitor():
    global is_currently_recording
    fgbg = cv2.createBackgroundSubtractorMOG2()
    while writer_thread.isAlive():
        if not is_currently_recording and mainRun:
            im1 = fgbg.apply(currentFrame)
            im2,contours,hierarchy = cv2.findContours(im1, 1, 2)
            if len(contours) != 0:
                c = max(contours, key = cv2.contourArea)
                if(cv2.contourArea(c) > sensitivity):
                    print("Movement detected, starting recording")
                    is_currently_recording = True
    cv2.destroyAllWindows()

# Function which writes the videos frames to the shared, thread safe queue
def writer():
    global currentFrame
    cap = cv2.VideoCapture(0)
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # If a frame is read, add new frame to queue
        if ret == True:
            currentFrame = frame
        else:
            break

# Funciton to record the camera only when the recording boolean is set to True
def recorder():
    global is_currently_recording
    fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    while writer_thread.isAlive():
        if is_currently_recording:
            video_name = str(datetime.now())+".avi"
            # Get current time
            current_time = clock()
            # Add x seconds to current time
            video_end_time = clock() + videoLength
            # set output file and resolution
            out = cv2.VideoWriter(video_name, fourcc, 19.0, (640,480))
            # record until current time > added time
            while(video_end_time > clock()):
                out.write(currentFrame)
            out.release()
            # set is_currently_recording to false
            is_currently_recording = False


# Funciton to record the camera only when the recording boolean is set to True
def viewer2():
    global is_currently_recording
    fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    while writer_thread.isAlive():
        if not is_currently_recording:
            # set output file and resolution
            out2 = cv2.VideoWriter("people-walking.avi", fourcc, 20.0, (640,480))
            # record until current time > added time
            while True:
                out2.write(currentFrame)
            out2.release()
            print("Finished Recording")
            # set is_currently_recording to false
            is_currently_recording = False

# Function to empty the queue before and after recording
# This stops buffered images from the past being process asif they
# were in the present
def clearFrame():
    global currentFrame
    currentFrame = np.zeros((480,640,3), np.uint8)


# Function to query the excel spreadsheet to check if:
# It should be running
# How long video capture should be
# Sensitivity
def queryServer():
    global mainRun, videoLength, sensitivity
    sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    sheet_creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', sheets_scope)
    sheets_client = gspread.authorize(sheet_creds)
    sheet = sheets_client.open('SecCam').sheet1
    data = sheet.row_values(1)
    mainRun = bool(int(data[0]))
    videoLength = int(data[1])
    sensitivity = int(data[2])
    Timer(3600, queryServer).start() # re-run every hour




# Set global settings
mainRun = False
videoLength = 5
sensitivity = 90000
# Get a single blank black frame for the default
currentFrame = np.zeros((480,640,3), np.uint8)
is_currently_recording = False

writer_thread = Thread(target=writer)
writer_thread.start()
Thread(target=monitor ).start()
Thread(target=viewer  ).start()
Thread(target=recorder).start()
queryServer()


# Put all of this into a class
# Try to display the queue in a webpage
