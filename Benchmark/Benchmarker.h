#include <stdio.h>
#include <time.h>
#include <vector>
#include <limits>

class Benchmarker
{
public:
    void timeFunction(void (*f)(), int numOfTimes);
    double binary_op(double left, double right, double (*f)(double, double));
    Benchmarker();
private:
    void printFunctionResults();
    void calculateBenchmarks(std::vector<float>* allTimes);
    int numberOfTimes;
    float averageSpeed, fastestSpeed, slowestSpeed;
};
