#include "Benchmarker.h"
using namespace std;

Benchmarker::Benchmarker()
{
  averageSpeed = 0.0;
  fastestSpeed = 0.0;
  slowestSpeed = 0.0;
}

// Function to display the results of the benchmarker
void Benchmarker::printFunctionResults()
{
    printf("Ran %d times\n", numberOfTimes);
    printf("Average speed: %fs\n", averageSpeed);
    printf("Fastest speed: %fs\n", fastestSpeed);
    printf("Slowest speed: %fs\n", slowestSpeed);
}

// Functiion which takes an int and a function
// The passed function will be ran the passed int number of times
// The average, fastest and slowest benchmarks for the passed function are calc
void Benchmarker::timeFunction(void (*f)(), int numOfTimes)
{
    std::vector<float> allTimes;
    float timeTaken;
    clock_t t;
    numberOfTimes = numOfTimes;

    // Loop through the given integer number of times
    for(int n = 0; n<numberOfTimes; n++)
    {
        t = clock();
        // Run the function and store the time taken in an array
        (*f)();
        t = clock() - t;
        timeTaken = ((float)t)/CLOCKS_PER_SEC;
        allTimes.push_back(timeTaken);
    }
    calculateBenchmarks(&allTimes);
    printFunctionResults();
}

// Function which passes a function which takes two doubles, returning a double
// EXPLANATION ================================================================
// To make this work with the timefunction, change the void(*f)()
// to : double(*f)(double, double)
double Benchmarker::binary_op(double left, double right, double (*f)(double, double)) {
    return (*f)(left, right);
}

// Function to calculate the benchmarks of the times given in a vector
void Benchmarker::calculateBenchmarks(std::vector<float>* allTimes)
{
    float runningTotal;
    float fastest = std::numeric_limits<float>::max();
    float slowest = std::numeric_limits<float>::min();
    int size = allTimes->size();
    for(int i = 0; i < size; i++)
    {
        float indexValue = allTimes->at(i);
        runningTotal += indexValue;

        if(indexValue < fastest)
        {
            fastest = indexValue;
        } else if( indexValue > slowest)
        {
            slowest = indexValue;
        }
    }
    averageSpeed = runningTotal/size;
    fastestSpeed = fastest;
    slowestSpeed = slowest;
}
