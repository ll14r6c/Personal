#include "Benchmarker.h"
#include <stdio.h>
using namespace std;

// Example function without any argurments
void print(){
  for(int i = 0; i < 100; i++){
    // do something
    printf("\n");
  }
}

double multiply(double left, double right) {
    return left * right;
}

// Main funciton to benchmark a function a number of times
int main()
{
  Benchmarker a;
  a.timeFunction(print, 90);
  // a.binary_op(5.0,10.0, multiply);
  return 0;
}
