#include "triangleRast.h"

int main()
{
  // Qa
  triangleRast abg(128,128, "abg", "ppm");
  abg.fillRGB(255, 255, 192);
  abg.abg();
  abg.writeDataToFile();
  // Qb
  triangleRast halfplane(128,128, "halfplane", "ppm");
  halfplane.fillRGB(255, 255, 192);
  halfplane.halfplane();
  halfplane.writeDataToFile();
  // Qc
  triangleRast triangle(128,128, "triangle", "ppm");
  triangle.fillRGB(255, 255, 192);
  triangle.halfplaneblack();
  triangle.writeDataToFile();
  // Qd
  triangleRast rgb(128,128, "rgb", "ppm");
  rgb.fillRGB(255, 255, 192);
  rgb.barycentric();
  rgb.writeDataToFile();
  // Qe
  triangleRast texture(128,128, "texture", "ppm");
  texture.fillRGB(255, 255, 192);
  texture.textureTriangle("earth(1).ppm");
  texture.writeDataToFile();

  // Qf
  // Svalbard

  return 0;
}
