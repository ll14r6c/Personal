#include <stdio.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <sstream>

struct RGB{
  int r;
  int g;
  int b;
};

struct Position{
  int x;
  int y;
};

struct UV{
  float u;
  float v;
};

struct Vertex{
  Position pos;
  RGB col;
  UV texcoords;
};

class triangleRast
{
public:
  triangleRast(int pWidth, int pHeight, std::string pFilename, std::string pExtension);
  void makeRandomRGB();
  void fillRGB(int r, int g, int b);
  void loadTexture(std::string texfilename);
  void setPixel(int x, int y, RGB rgbVal);
  void writeDataToFile();
  float distanceToLine(Position pos1, Position pos2, Position point);
  void abg();
  void halfplane();
  void halfplaneblack();
  void barycentric();
  void textureToRGB(std::vector<int> input);
  RGB getTexColour(int x, int y);
  RGB bilinearLookup(float s, float t);
  void textureTriangle(std::string filename);
  std::vector<float> barycentricCoordinates(Position A, Position B, Position C, Position p);
private:
  int width, height, maxRange;
  
  std::string filename;
  std::string extension;
  std::vector<RGB> pixelArray;
  std::vector<RGB> texArray;

  // Hardcoded texture width and height
  int heightTex = 256;
  int widthTex = 512;

  // Hardcoded vertex data
  Vertex A;
  Vertex B;
  Vertex C;
};
