#include "triangleRast.h"

// Create a blank pixel array of given size
triangleRast::triangleRast(int pWidth, int pHeight, std::string pFilename, std::string pExtension)
{
  maxRange = 255;
  width = pWidth;
  height = pHeight;
  filename = pFilename;
  // Check if the given extension is valid
  if(pExtension == "ppm")
  {
      extension = pExtension;
  }
  pixelArray.resize(width*height);

  A.pos.x = 10;
  A.pos.y = 20;
  A.col.r = 255;
  A.col.g = 0;
  A.col.b = 0;
  A.texcoords.u = 0.5;
  A.texcoords.v = 1.0;

  B.pos.x = 100;
  B.pos.y = 30;
  B.col.r = 0;
  B.col.g = 255;
  B.col.b = 0;
  B.texcoords.u = 0.6;
  B.texcoords.v = 0.5;

  C.pos.x = 40;
  C.pos.y = 100;
  C.col.r = 0;
  C.col.g = 0;
  C.col.b = 255;
  C.texcoords.u = 0.44434;
  C.texcoords.v = 0.87186;
}

// Write pixel array data to a file
void triangleRast::writeDataToFile()
{
  std::ofstream myfile;
  myfile.open((filename+"."+extension).c_str());
  myfile << "P3\n# "<<filename<<"\n"<<width << " " <<height<<"\n"<<maxRange<<"\n";
  for(auto &row: pixelArray){
    myfile << row.r << " " << row.g << " " << row.b << "  ";
  }
  myfile.close();
}

// Fill pixel array with random values (for debug)
void triangleRast::makeRandomRGB()
{
  for(int i = 0; i < pixelArray.size(); i++)
  {
    RGB newRGB;;
    newRGB.r =  0 + (rand() % static_cast<int>(maxRange - 0 + 1));
    newRGB.g =  0 + (rand() % static_cast<int>(maxRange - 0 + 1));
    newRGB.b =  0 + (rand() % static_cast<int>(maxRange - 0 + 1));
    pixelArray.at(i) = newRGB;
  }
}

// Fill pixel array with a given r g b value
void triangleRast::fillRGB(int r, int g, int b){
  for(int i = 0; i < pixelArray.size(); i++){
    RGB newRGB;;
    newRGB.r =  r;
    newRGB.g =  g;
    newRGB.b =  b;
    pixelArray.at(i) = newRGB;
  }
}

// Set a pixel at a given x,y to a given RGB
// Coordinate (0,0) is bottom left of the array
void triangleRast::setPixel(int x, int y, RGB givenRGB){
  pixelArray.at(height*((height-1)-y)+x) = givenRGB;
}

// Calculate the barycentricCoordinates of a given position p, in a triangle ABC
std::vector<float> triangleRast::barycentricCoordinates(Position A, Position B, Position C, Position p){
  std::vector<float> toReturn;
  float p_CB = (B.y-C.y)*(p.x-C.x)+(C.x-B.x)*(p.y-C.y);
  float A_CB = (B.y-C.y)*(A.x-C.x)+(C.x-B.x)*(A.y-C.y);
  float alpha = p_CB/A_CB;

  float p_AC = (C.y-A.y)*(p.x-C.x)+(A.x-C.x)*(p.y-C.y);
  float B_AC = (B.y-C.y)*(A.x-C.x)+(C.x-B.x)*(A.y-C.y);
  float beta = p_AC/B_AC;

  float gamma = 1.0-alpha-beta;

  toReturn.push_back(alpha);
  toReturn.push_back(beta);
  toReturn.push_back(gamma);

  return toReturn;
}

// Question one solution
// Image rendered with barycentric coordinates
void triangleRast::abg(){
  Position current;
  for(int x = 0; x < width; x++){
    for(int y = 0; y < height; y++){
      current.x = x;
      current.y = y;
      std::vector<float> baryCoords = barycentricCoordinates(A.pos, B.pos, C.pos, current);

      RGB colour;
      colour.r = 100 + (100*baryCoords[0]);
      colour.g = 100 + (100*baryCoords[1]);
      colour.b = 100 + (100*baryCoords[2]);
      // Clamp the values between [0,255]
      if(colour.r > 255){
        colour.r = 255;
      }
      if(colour.g > 255){
        colour.g = 255;
      }
      if(colour.b > 255){
        colour.b = 255;
      }
      if(colour.r < 0){
        colour.r = 0;
      }
      if(colour.g < 0){
        colour.g = 0;
      }
      if(colour.b < 0){
        colour.b = 0;
      }

      setPixel(x,y,colour);
    }
  }
}

// Question two solution
// halfplane test
void triangleRast::halfplane(){
  Position current;
  for(int x = 0; x < width; x++){
    for(int y = 0; y < height; y++){
      current.x = x;
      current.y = y;
      std::vector<float> baryCoords = barycentricCoordinates(A.pos, B.pos, C.pos, current);

      RGB colour;
      // alpha
      if(baryCoords[0] < 0){
        colour.r = 0;
      }
      if(baryCoords[0] >= 0){
        colour.r = 255;
      }
      // beta
      if(baryCoords[1] < 0){
        colour.g = 0;
      }
      if(baryCoords[1] >= 0){
        colour.g = 255;
      }
      // gamma
      if(baryCoords[2] < 0){
        colour.b = 0;
      }
      if(baryCoords[2] >= 0){
        colour.b = 255;
      }

      setPixel(x,y,colour);
    }
  }
}

// Question three solution
// fill triangle to black
void triangleRast::halfplaneblack(){
  Position current;
  for(int x = 0; x < width; x++){
    for(int y = 0; y < height; y++){
      current.x = x;
      current.y = y;
      std::vector<float> baryCoords = barycentricCoordinates(A.pos, B.pos, C.pos, current);
      // If outside of the traingle, continue to the next loop
      if ((baryCoords[0] < 0.0) || (baryCoords[1] < 0.0) || (baryCoords[2] < 0.0)){
        continue;
      }
      RGB colour;
      colour.r = 0;
      colour.g = 0;
      colour.b = 0;
      setPixel(x,y,colour);
    }
  }
}

// Question four solution
// Fill triangle with correct colouring using barycentric interpolation
void triangleRast::barycentric(){
  Position current;
  for(int x = 0; x < width; x++){
    for(int y = 0; y < height; y++){
      current.x = x;
      current.y = y;
      std::vector<float> baryCoords = barycentricCoordinates(A.pos, B.pos, C.pos, current);
      // If outside of the traingle, continue to the next loop
      if ((baryCoords[0] < 0.0) || (baryCoords[1] < 0.0) || (baryCoords[2] < 0.0)){
        continue;
      }
      RGB colour;
      colour.r = A.col.r * baryCoords[0] + B.col.r * baryCoords[1] + C.col.r * baryCoords[2];
      colour.g = A.col.g * baryCoords[0] + B.col.g * baryCoords[1] + C.col.g * baryCoords[2];
      colour.b = A.col.b * baryCoords[0] + B.col.b * baryCoords[1] + C.col.b * baryCoords[2];
      setPixel(x,y,colour);
    }
  }
}

// Load a given texture file into the texArray
void triangleRast::loadTexture(std::string texfilename)
{
  std::ifstream infile;
  std::string line;
  int stringToInt;
  std::vector<int> texIntData;
  infile.open(texfilename.c_str());
  if (infile.is_open()){
    while ( getline (infile,line) ){
      std::stringstream parser(line);
      stringToInt = 0;
      parser >> stringToInt;
      texIntData.push_back(stringToInt);
    }
    infile.close();
  }else{
    std::cout << "Unable to open file" << std::endl;
  }
  textureToRGB(texIntData);
}

// Convert an array of ints to an array of RGB values
void triangleRast::textureToRGB(std::vector<int> input){
  if(texArray.empty()){
    RGB temp;
    for(int i = 4; i < input.size(); i+=3){
      temp.r = input[i];
      temp.g = input[i+1];
      temp.b = input[i+2];
      texArray.push_back(temp);
    }
  }else{
    std::cerr << "Error: texArray already contains data" << std::endl;
  }
}

// Get the colour of a given texel
RGB triangleRast::getTexColour(int u, int v){
  return texArray[(heightTex*((heightTex-1)-v))+u];
}

// Bilinear interpolation for use when texturing
RGB triangleRast::bilinearLookup(float s, float t){

  // Get the surrounding colours for interpolation between
  int Uint = (int)floor(s-0.5);
  int Vint = (int)floor(t-0.5);
  RGB colour00 = getTexColour(Uint, Vint);
  RGB colour01 = getTexColour(Uint,Vint+1);
  RGB colour10 = getTexColour(Uint+1,Vint);
  RGB colour11 = getTexColour(Uint+1,Vint+1);

  // Get the leftover float for use when interpolating
  float u_frac = s - Uint-0.5;
  float v_frac = t - Vint-0.5;

  // Calculate the upper u values
  RGB max;
  max.r = colour01.r * (1-u_frac)+ (colour11.r*u_frac);
  max.g = colour01.g * (1-u_frac)+ (colour11.g*u_frac);
  max.b = colour01.b * (1-u_frac)+ (colour11.b*u_frac);

  // Calculate the lower u values
  RGB min;
  min.r = colour00.r * (1-u_frac)+ (colour10.r*u_frac);
  min.g = colour00.g * (1-u_frac)+ (colour10.g*u_frac);
  min.b = colour00.b * (1-u_frac)+ (colour10.b*u_frac);

  // Interpolate between the upper and lower u values
  RGB fin;
  fin.r = min.r * (1-v_frac)+ (max.r*u_frac);
  fin.g = min.g * (1-v_frac)+ (max.g*u_frac);
  fin.b = min.b * (1-v_frac)+ (max.b*u_frac);

  return fin;
}

// Question five solution
// Read the given texture then texture the given triangle using a bilinear lookup
void triangleRast::textureTriangle(std::string filename){
  loadTexture(filename);
  Position current;
  for(int x = 0; x < width; x++){
    for(int y = 0; y < height; y++){
      current.x = x;
      current.y = y;
      std::vector<float> baryCoords = barycentricCoordinates(A.pos, B.pos, C.pos, current);
      // If outside of the traingle, continue to the next loop
      if ((baryCoords[0] < 0.0) || (baryCoords[1] < 0.0) || (baryCoords[2] < 0.0)){
        continue;
      }else{
        // Calculate the uv coordinates using the barycentricCoordinates of the point
        UV uvCoords;
        uvCoords.u = baryCoords[0]*A.texcoords.u + baryCoords[1]*B.texcoords.u + baryCoords[2]*C.texcoords.u;
        uvCoords.v = baryCoords[0]*A.texcoords.v + baryCoords[1]*B.texcoords.v + baryCoords[2]*C.texcoords.v;
        float n_u = uvCoords.u*(widthTex-1);
        float n_v = uvCoords.v*(heightTex-1);
        setPixel(x,y,bilinearLookup( n_u, n_v ));
      }
    }
  }
}
