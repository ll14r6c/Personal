#include <iostream>
#include <thread>
#include <bitset>
#include <queue>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "myQueue.cpp"
#include "FGTSqueue.cpp"
typedef int chessboard;



// Initialize a thread queue
std::queue<std::vector<std::vector<int>>> subproblemQueue;
std::mutex m;
std::condition_variable cv;
int finalSolution = 0;
std::atomic<bool> solved(false);

/* Finds the number of solutions to the N-queen problem
 * ld: a bit pattern containing ones for positions under attack along left diagonals for this row
 * cols: a bit pattern containing ones for columns that are already accupied
 * rd: a bit pattern containing ones for positions under attack along right diagonals for this row
 * all: a bit pattern where the first N bits are set to one, where N is the number of queens
 *
 * ld, cols, and rd contain sufficient info about the current state of the board.
 * (ld | cols | rd) is a bit pattern containing ones in all positions under attack for this row
 */
int seq_nqueen(chessboard ld, chessboard cols, chessboard rd, const chessboard all){
  // std::cout << "New\n" << std::bitset<4>(ld) << std::endl;
  // std::cout << std::bitset<4>(cols) << std::endl;
  // std::cout << std::bitset<4>(rd) << std::endl;
  int sol = 0;

  if (cols == all){  // If all queens have been been assigned a column
    solved = true;
    return 1;
  }

  chessboard pos = ~(ld | cols | rd) & all;  // Possible positions for the queen on the current row
  // std::cout << "Possible: " << std::bitset<4>(pos) << std::endl;  // If nothing visiable, print out the bit pats
  chessboard next;
  while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
    next = pos  & (-pos);                    // next possible position
    // std::cout << std::bitset<4>(next) << std::endl;
    pos -= next;                             // update the possible position
    // std::cout << std::bitset<4>(pos) << std::endl;
    static const int s1[] = {(ld|next) << 1,cols|next,(rd|next)>>1,all};
    std::vector<int> initialState(s1, s1+sizeof(s1)/sizeof(s1[0]));
    std::vector<std::vector<int>> test;
    test.push_back(initialState);
    subproblemQueue.push(test);
    sol += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1, all); // recursive call for the `next' position
  }
  solved = true;
  return sol;
}

int par_nqueen(chessboard ld, chessboard cols, chessboard rd, const chessboard all, int sol){
  if (cols == all){  // If all queens have been been assigned a column
    solved = true;
    finalSolution+=1;
    return 1;
  }

  chessboard pos = ~(ld | cols | rd) & all;  // Possible positions for the queen on the current row
  // std::cout << "Possible: " << std::bitset<4>(pos) << std::endl;  // If nothing visiable, print out the bit pats
  chessboard next;
  while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
    next = pos  & (-pos);                    // next possible position
    // std::cout << std::bitset<4>(next) << std::endl;
    pos -= next;                             // update the possible position
    // std::cout << std::bitset<4>(pos) << std::endl;
    static const int s1[] = {(ld|next) << 1,cols|next,(rd|next)>>1,all};
    std::vector<int> initialState(s1, s1+sizeof(s1)/sizeof(s1[0]));
    std::vector<std::vector<int>> test;
    test.push_back(initialState);
    subproblemQueue.push(test);
    par_nqueen((ld|next) << 1, cols|next, (rd|next)>>1, all, sol); // recursive call for the `next' position
  }
  //solved = true;
  finalSolution+=sol;
  return sol;
}


int nqueen_solver(int n){
  chessboard all = (1 << n) - 1; // set N bits on, representing number of columns
	std::cout << all << std::endl; // all = (2^n)-1
  return seq_nqueen(0,0,0, all);
}

void thread_function(){
  std::vector<std::vector<int>> popped;
  while(solved!=true){
    std::cout << "running\n";
    // pop a state from the queue using locks
    m.lock();
    // cv.wait(lock, []{return subproblemQueue.empty();});
    popped = subproblemQueue.front();
    subproblemQueue.pop();
    //subproblemQueue.pop();
    m.unlock();
    // if the queue is empty, wait for mutex/condition variable

    // calculate given state
    par_nqueen(popped[0][0], popped[0][1], popped[0][2], popped[0][3], 0);
    //finalSolution += seq_nqueen(popped[0][0], popped[0][1], popped[0][2], popped[0][3]);

    // If the state has children, put them into the queue
    // pop a state again
  }
}


int main (int argc, char** argv){
	if (argc != 4){
     std::cout << "You have to provide : \n 1) Number of Queens \n 2) Maximum number of threads \n 3) Switching point to sequential" << std::endl;
     return 0;
  }

  int qn = std::stoi(argv[1]); // Number of queens
	int mt = std::stoi(argv[2]); // Maximum number of threads
	int sp = std::stoi(argv[3]); // Switching point to sequential

	// Lauch threads with mt threads
  std::thread allThreads[mt];
  // Set currentPoint to
  int currentPoint = 2;
  // Create the initial state and put this into the queue
  static const int s1[] = {0,0,0,(1<<qn)-1};
  std::vector<int> initialState(s1, s1+sizeof(s1)/sizeof(s1[0]));
  std::vector<std::vector<int>> test;
  test.push_back(initialState);
  subproblemQueue.push(test);
  // Create mt threads with the thread function
  for(int i = 0; i < mt; i++){
    allThreads[i] = std::thread(thread_function);
    std::cout << i << "Thread created\n";
  }

  // Join mt threads
  for(int i = 0; i < mt; i++){
    allThreads[i].join();
    std::cout << i << "Thread joined\n";
  }





  // While the current point is greater than the switching point
  // while(currentPoint > sp){
    // Pop from the queue (using locks)
    // subproblemQueue.pop();
    // If the queue is empty, wait until something comes in
    // run the popped thing from the queue
    //  If the popped thing has children, put them onto the queue
    //  If the popped thing has no children, continue the while
    // --currentPoint;
  // }

  // chessboard all = (1 << qn) - 1; // set N bits on, representing number of columns
  // int result = seq_nqueen(0,0,0, all);

	// Thread pool can choose a seq_nqueen sub-problem to complete
	// Unfolding sub-problems can be stored in a std::stack or std::queue
	// Should switch to the sequential mode for smaller values of N (e.g N < 13)
	// If the program fails due to reaching maximum stack size on platform
	//	-> Adjust the sp parameter

	// Platform specification:
	// Operating system/CPU/Compiler type + args

  // int sol = nqueen_solver(qn);
  std::cout << "Number of solutions to " << qn << "-Queen problem is : " << finalSolution << std::endl;
  // delete [] allThreads;
  return 0;
}
