#include <iostream>
#include <queue>
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>

typedef int chessboard;

// Global variables
// Queue for subproblem states
std::queue<std::vector<std::vector<int>>> subproblemQueue;
std::mutex m;
std::condition_variable cv;
int finalSolution = 0;
// atomic boolean to store if the problem is solved or not
std::atomic<bool> solved(false);

/* Finds the number of solutions to the N-queen problem
 * ld: a bit pattern containing ones for positions under attack along left diagonals for this row
 * cols: a bit pattern containing ones for columns that are already accupied
 * rd: a bit pattern containing ones for positions under attack along right diagonals for this row
 * all: a bit pattern where the first N bits are set to one, where N is the number of queens
 *
 * ld, cols, and rd contain sufficient info about the current state of the board.
 * (ld | cols | rd) is a bit pattern containing ones in all positions under attack for this row
 */
int seq_nqueen(chessboard ld, chessboard cols, chessboard rd, const chessboard all){
  int sol = 0;

  if (cols == all)                            // A solution is found
    return 1;

  chessboard pos = ~(ld | cols | rd) & all;  // Possible posstions for the queen on the current row
  chessboard next;
  while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
    next = pos  & (-pos);                    // next possible position
    pos -= next;                             // update the possible position

    // Add the new subproblem to the queue
    static const int s1[] = {(ld|next) << 1,cols|next,(rd|next)>>1,all};
    std::vector<int> initialState(s1, s1+sizeof(s1)/sizeof(s1[0]));
    std::vector<std::vector<int>> test;
    test.push_back(initialState);
    subproblemQueue.push(test);


    sol += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1, all); // recursive call for the `next' position
  }
  return sol;
}

int nqueen_solver(int n){
  chessboard all = (1 << n) - 1;            // set N bits on, representing number of columns
  return seq_nqueen(0,0,0, all);
}

// Function to be ran by all threads
// this function pops from the subproblem queue and solves the problem
// any additional problems are pushed onto the queue
// loops while the problem is not solved
void thread_function(){
  std::vector<std::vector<int>> popped;
  while(solved!=true){
    // pop a state from the queue using locks
    m.lock();
    popped = subproblemQueue.front();
    subproblemQueue.pop();
    m.unlock();

    // if the queue is empty, wait for condition variable

    // Calculate given state
    seq_nqueen(popped[0][0], popped[0][1], popped[0][2], popped[0][3]);
    // If the state has children, they will be put onto the queue
  }
}

int main (int argc, char** argv){
  if (argc != 4){
     std::cout << "You have to provide : \n 1) Number of Queens \n 2) Maximum number of threads \n 3) Switching point to sequential" << std::endl;
     return 0;
  }

  int qn = std::stoi(argv[1]); // Number of queens
	int mt = std::stoi(argv[2]); // Maximum number of threads
	int sp = std::stoi(argv[3]); // Switching point to sequential

	// Lauch threads with mt threads
  std::thread allThreads[mt];


  // Create the initial state and put this into the queue
  static const int s1[] = {0,0,0,(1<<qn)-1};
  std::vector<int> initialState(s1, s1+sizeof(s1)/sizeof(s1[0]));
  std::vector<std::vector<int>> test;
  test.push_back(initialState);
  subproblemQueue.push(test);

  // Create mt threads with the thread function
  for(int i = 0; i < mt; i++){
    allThreads[i] = std::thread(thread_function);
    std::cout << i << "Thread created\n";
  }

  // Let the threads calculate

  // Join mt threads
  for(int i = 0; i < mt; i++){
    allThreads[i].join();
    std::cout << i << "Thread joined\n";
  }

  // Sequential code
  int sol = nqueen_solver(qn);

  std::cout << "Number of solutions to " << qn << "-Queen problem is : " << sol << std::endl;

  return 0;
}
