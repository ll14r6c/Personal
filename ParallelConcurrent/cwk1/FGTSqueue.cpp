#include <queue>
#include <mutex>
#include <memory>
#include <condition_variable>
template<typename T>
class FGTSqueue{
private:
 struct node{
 std::shared_ptr<T> data;
 std::unique_ptr<node> next;
 };
 std::mutex head_m;
 std::mutex tail_m;
 std::unique_ptr<node> head;
 std::condition_variable cv;
 node *tail;
 node* get_tail() {
 std::lock_guard<std::mutex> gate(tail_m);
 return tail;
 }
 std::unique_ptr<node> pop_head() {
 std::unique_ptr<node> oh = std::move(head);
 head = std::move(oh->next);
 return oh;
 }
 std::unique_lock<std::mutex> wait_for_data() {
 std::unique_lock<std::mutex> gate(head_m);
 cv.wait(head_m,
 [&]{return head.get() != get_tail(); } );
 return std::move(gate);
 }
 std::unique_ptr<node> wait_pop_head() {
 std::unique_lock<std::mutex>
 gate(wait_for_data());
 return pop_head();
 }
 std::unique_ptr<node> wait_pop_head(T &val) {
 std::unique_lock<std::mutex>
 gate(wait_for_data());
 val = std::move(*head->data);
 return pop_head();
}
public:
 FGTSqueue()
 : head(new node), tail(head.get()) {}
 FGTSqueue(const FGTSqueue & other)
 = delete;
 FGTSqueue& operator=(const FGTSqueue & other)
 = delete;
 std::shared_ptr<T> try_pop(){
 std::unique_ptr<node> old_head = pop_head();
 return old_head ? old_head->data
 : std::shared_ptr<T>();
 }
 void push(T val){
 std::shared_ptr<T> d(
 std::make_shared<T>(std::move(val)));
 std::unique_ptr<node> p(d);
 // create block to exploit lock_guard
 {
 std::lock_guard<std::mutex> gate(tail_m);
 tail->data = d;
 node* const new_tail = p.get();
 tail->next = std::move(p);
 tail = new_tail;
 }
 cv.notify_one();
 }
 std::shared_ptr<T> wait_and_pop(){
 std::unique_ptr<node> const oh
 = wait_pop_head();
 return oh->data;
 }
 void wait_and_pop(T & val){
 std::unique_ptr<node> const oh
 = wait_pop_head(val);
 }
};
