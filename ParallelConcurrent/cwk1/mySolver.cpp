// int main (int argc, char** argv){
//   if (argc < 3){
//      std::cout << "You have to provide : \n 1) Number of Queens \n 2) Maximum number of threads \n 3) Switching point to sequential" << std::endl;
//      return 0;
//   }
//
// 	// stoi: Parses str interpreting its content as an integral number of the
// 	// specified base, which is returned as an int value.
//
//   int qn = std::stoi(argv[1]); // Number of queens
// 	int mt = std::stoi(argv[2]); // Maximum number of threads
// 	int sp = std::stoi(argv[3]); // Switching point to sequential
//
//   int sol = nqueen_solver(qn);
//
//   std::cout << "Number of solutions to " << qn << "-Queen problem is : " << sol << std::endl;
//
//   return 0;
// }
