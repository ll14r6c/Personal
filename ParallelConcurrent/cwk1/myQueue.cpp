#include <queue>
#include <mutex>
#include <condition_variable>

template<typename T>
class myQueue
{
private:
  std::queue<T> queue;
  std::mutex m;
  std::condition_variable cv;
public:
    void push(T const& t)
    {
        m.lock();
        queue.push(t);
        m.unlock();
        cv.notify_one();
    }

    bool empty()
    {
        m.lock();
        return queue.empty();
    }

    bool try_pop(T& popped_value)
    {
        m.lock();
        if(queue.empty())
        {
            return false;
        }

        popped_value=queue.front();
        queue.pop();
        return true;
    }

    void wait_and_pop(T& popped_value)
    {
        std::unique_lock<std::mutex> lock(m);
        while(queue.empty())
        {
            cv.wait(lock, [&]{return queue.empty() != true;});
        }

        popped_value=queue.front();
        queue.pop();
    }

};
