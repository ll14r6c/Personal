
#include <stdio.h>

#define CHECK(e) { int res = (e); if (res) printf("CUDA ERROR %d\n", res); }

#define CHANNEL 3

struct Image {
  int width;
  int height;
  unsigned int bytes;
  unsigned char *img;
  unsigned char *dev_img;
  unsigned char *dev_img_t;

  unsigned char *inImage;
  unsigned char *transposeImage;
  unsigned char *outImage;
};

__device__
void transpose(unsigned char *in, unsigned char *out, int idx, int idy, int width, int height){
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;
    int i = floor((double)index_in/3);
    int t = floor((double)(index_in/(height*3)));
    out[index_in] = in[index_in%3 + (i * width % (width*height)) + 3 * t];
  }
}

__device__
void reorder(unsigned char *in, unsigned char *out, int idx, int idy, int width, int height){
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;

    int current_column = floor((double)idy/3);
    int output_column = (width/3) - current_column - 1;
    int output_y = 3 * output_column + (idy % 3);

    int index_out = idx * width + output_y;

    out[index_out] = in[index_in];
  }
}

__global__
void rotate(unsigned char* in, unsigned char* tpose, unsigned char* out, int width, int height) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  transpose(in, tpose, idx, idy, width*3, height);
  __syncthreads();
  reorder(tpose, out, idx, idy, height*3, width);
}

// Reads a color PPM image file (name provided), and
// saves data in the provided Image structure.
// The max_col_val is set to the value read from the
// input file. This is used later for writing output image.
int readInpImg (const char * fname, Image & source, int & max_col_val){

  FILE *src;

  if (!(src = fopen(fname, "rb")))
  {
      printf("Couldn't open file %s for reading.\n", fname);
      return 1;
  }

  char p,s;
  fscanf(src, "%c%c\n", &p, &s);
  if (p != 'P' || s != '6')   // Is it a valid format?
  {
      printf("Not a valid PPM file (%c %c)\n", p, s);
      exit(1);
  }

  fscanf(src, "%d %d\n", &source.width, &source.height);
  fscanf(src, "%d\n", &max_col_val);

  int pixels = source.width * source.height;
  source.bytes = pixels * 3;  // 3 => colored image with r, g, and b channels
  source.img = (unsigned char *)malloc(source.bytes);
  source.dev_img = (unsigned char *)malloc(source.bytes);

  source.inImage = (unsigned char *)malloc(source.bytes);
  source.transposeImage = (unsigned char *)malloc(source.bytes);
  source.outImage = (unsigned char *)malloc(source.bytes);
  if (fread(source.img, sizeof(unsigned char), source.bytes, src) != source.bytes)
    {
       printf("Error reading file.\n");
       exit(1);
    }
  fclose(src);
  return 0;
}

// Write a color image into a file (name provided) using PPM file format.
// Image structure represents the image in the memory.
int writeOutImg(const char * fname, const Image & roted, const int max_col_val){

  FILE *out;
  if (!(out = fopen(fname, "wb")))
  {
      printf("Couldn't open file for output.\n");
      return 1;
  }
  fprintf(out, "P6\n%d %d\n%d\n", roted.width, roted.height, max_col_val);
  if (fwrite(roted.img, sizeof(unsigned char), roted.bytes , out) != roted.bytes)
  {
      printf("Error writing file.\n");
      return 1;
  }
  fclose(out);
  return 0;
}



int main(int argc, char **argv)
{

  if (argc != 2)
  {
      printf("Usage: exec filename\n");
      exit(1);
  }
  char *fname = argv[1];

  //Read the input file
  Image source;
  int max_col_val;
  if (readInpImg(fname, source, max_col_val) != 0)  exit(1);

  // Complete the code

  // Allocate array on the GPU
  CHECK( cudaMalloc( (void**)&source.dev_img, source.bytes*sizeof(unsigned char)) );
  CHECK( cudaMalloc( (void**)&source.dev_img_t, source.bytes*sizeof(unsigned char)) );

  CHECK( cudaMalloc( (void**)&source.inImage, source.bytes*sizeof(unsigned char)) );
  CHECK( cudaMalloc( (void**)&source.transposeImage, source.bytes*sizeof(unsigned char)) );
  CHECK( cudaMalloc( (void**)&source.outImage, source.bytes*sizeof(unsigned char)) );

  // Copy the input array from host to device
  CHECK( cudaMemcpy( source.dev_img, source.img, source.bytes*sizeof(unsigned char), cudaMemcpyHostToDevice) );
  CHECK( cudaMemcpy( source.inImage, source.img, source.bytes*sizeof(unsigned char), cudaMemcpyHostToDevice) );

  // Given I have 1024 max threads per block,
  // Ensure the array dimensions are easily divisible
  dim3 g_sz ((source.width+31)/32, ((source.width*3)+31)/32);
  dim3 b_sz (32, 32);

  cudaEvent_t start, stop;
  cudaEventCreate (&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0); // arg 0 refers to CUDA stream

  // transpose on GPU
  // Perform reverse on the GPU
  // transpose3<<<g_sz,b_sz>>>( source.dev_img, source.dev_img_t, source.width*3, source.height);
  // // Copy the results back from device to host array
  // CHECK( cudaMemcpy( source.img, source.dev_img_t, source.bytes*sizeof(unsigned char), cudaMemcpyDeviceToHost) );
  // // Copy the input array from host to device
  // CHECK( cudaMemcpy( source.dev_img, source.img, source.bytes*sizeof(unsigned char), cudaMemcpyHostToDevice) );
  rotate<<<g_sz,b_sz>>>( source.inImage, source.transposeImage, source.outImage, source.width, source.height);
  // Copy the results back from device to host array
  CHECK( cudaMemcpy( source.img, source.outImage, source.bytes*sizeof(unsigned char), cudaMemcpyDeviceToHost) );

  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop); // all work prior to stop is done.
  float t;
  cudaEventElapsedTime(&t, start, stop);
  cudaEventDestroy(start); // clean-up.
  cudaEventDestroy(stop);
  printf("Elapsed Time: %f(s)\n", t);

  // Swap the width and height
  int temp;
  temp = source.width;
  source.width = source.height;
  source.height = temp;

  // Write the output file
  if (writeOutImg("roted.ppm", source, max_col_val) != 0) // For demonstration, the input file is written to a new file named "roted.ppm"
   exit(1);

  free(source.img);
  cudaFree( source.dev_img );
  cudaFree( source.dev_img_t );


  exit(0);
}
