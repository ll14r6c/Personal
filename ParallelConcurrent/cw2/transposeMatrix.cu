#include <stdio.h>
#define CHECK(e) { int res = (e); if (res) printf("CUDA ERROR %d\n", res); }
// dimensions of the input matrix.
// Note that host input/output matrix (h_m, h_mt) are allocated on the stack.
// To work with large matrices, you need to allocate memory on the heap.
// This code is just for demenstration.
#define N 640

__global__
void transpose(int *m, int *mt){
  // compute x/y-index of thread within the grid
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  int idy = blockIdx.y*blockDim.y + threadIdx.y;

  int tidM, tidT;
  if (idx < N && idy < N) {
    // from the x/y index, compute the offset of element at (idy, idx)
    // within the input matrix
    tidM = idx * N + idy;
    // similarly, compute the offset of element at (idx, idy)
    // wihin the transposed matrix
    tidT = idy * N  + idx;

    mt[tidT] = m[tidM];   // copy value from matrix to transpose
  }
}


int main(int argc, char **argv){
  // source and transpose matrixes.  For convenience we
  // declare these as 2D arrays, but remember that when
  // we copy to device, we are just interested in these
  // as contiguous blocks of N^2 integers, i.e. CUDA
  // kernel does not know about the 2D structure.
  // In general, to process multi-dimensional arrays
  // in CUDA kernels, it is better to flatten the array
  // and treat it in a linear fashion.
  int h_m[N][N], h_mt[N][N];

  // pointers for the matrices on the device.
  int *dev_m, *dev_mt;

  int matrix_sz = N * N;
  // Allocate arrays on the GPU.  Note the size of allocation N*N integers.
  CHECK( cudaMalloc( (void**)&dev_m, matrix_sz*sizeof(int)) );
  CHECK( cudaMalloc( (void**)&dev_mt, matrix_sz*sizeof(int)) );

  // Initialise the source array on the host.  For convenience
  // in testing we just initialise each entry to be its index
  // in a flattened array.
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      h_m[i][j] = i*N + j;
    }
  }

  // Copy the input array from host to device
  CHECK( cudaMemcpy( dev_m, h_m, matrix_sz*sizeof(int), cudaMemcpyHostToDevice) );

  // My GPU has max 1024 threads per block, so I'm specifying a square grid
  // of 32*32 = 1024.  The number of blocks allocated in each row/column of
  // the grid should then be the width of the input array divided by 32 (nr of
  // threads in a row/column) of a block.  However, if the array dimension
  // is not evenly divisible by the block widdth/height, we need to round
  // up.  The computation below ensures this.
  dim3 b_sz (32, 32);
  dim3 g_sz ((N+31)/32, (N+31)/32);

  // Perform reverse on the GPU
  transpose<<<g_sz,b_sz>>>( dev_m, dev_mt );

  // Copy the results back from device to host array
  CHECK( cudaMemcpy( h_mt, dev_mt, matrix_sz*sizeof(int), cudaMemcpyDeviceToHost) );

  // Output result - of course oly useful for small values of N.
  for (int i = 0; i < N; i++){
    for (int j = 0; j < N; j++)
      printf( "%d\t", h_mt[i][j]);
    printf("\n");
  }


  // Clean up device.
  cudaFree( dev_m );
  cudaFree( dev_mt );

  return 0;
}
