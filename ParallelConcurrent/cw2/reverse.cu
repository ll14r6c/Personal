#include <stdio.h>
#define CHECK(e) { int res = (e); if (res) printf("CUDA ERROR %d\n", res); }
// dimensions of the input matrix.
// Note that host input/output matrix (h_m, h_mt) are allocated on the stack.
// To work with large matrices, you need to allocate memory on the heap.
// This code is just for demenstration.
#define W 6
#define H 3
#define N 5
#define CHANNEL 3

__global__
void transpose(int *m, int *mt){
  // compute x/y-index of thread within the grid
  int idx = blockIdx.x*blockDim.x + threadIdx.x;
  int idy = blockIdx.y*blockDim.y + threadIdx.y;

  int tidM, tidT;
  if (idx < H && idy < W) {
    // from the x/y index, compute the offset of element at (idy, idx)
    // within the input matrix
    tidM = idx * W + idy;
    // similarly, compute the offset of element at (idx, idy)
    // wihin the transposed matrix
    tidT = idy * H  + idx;

    mt[tidT] = m[tidM];   // copy value from matrix to transpose
  }
}

__global__
void reverseColumns(int *in, int *out){
  // compute x/y-index of thread within the grid
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;

  if (idx < W && idy < H) {
    int reversed = abs(idx-(W-1));
    int index_in = idy * W + idx;
    int index_out = idy * W  + reversed;
    out[index_out] = in[index_in];
  }
}

__global__
void rotate90(int *in, int *out){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;

  if (idx < W && idy < H) {
    int reversed = abs(idx-(W-1));
    int index_in = idx * W + idy;
    int index_out = idy * H  + reversed;
    out[index_out] = in[index_in];
  }
}

__global__
void transpose3(int *in, int *out, int width, int height){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;
    int i = floor((double)index_in/3);
    int t = floor((double)(index_in/(height*3)));
    out[index_in] = in[index_in%3 + (i * width % (width*height)) + 3 * t];
    // out[index_out + 1] = in[index_in + 1];
    // out[index_out + 2] = in[index_in + 2];
    // printf("%d %d -> %d %d (%d)\n", idy, idx, idx, idy, in[index_in]);
  }
}

__global__
void transposeReverse(int *in, int *out, int width, int height){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  if (idx < height && idy < width) {
    int reversed = abs(idx-(width-1))-2;
    int index_in = reversed * width + idy;
    int i = floor((double)index_in/3);
    int t = floor((double)(index_in/(height*3)));
    out[index_in] = in[index_in%3 + (i * width % (width*height)) + 3 * t];
    // out[index_out + 1] = in[index_in + 1];
    // out[index_out + 2] = in[index_in + 2];
    // printf("%d %d ->(%d)\n", idx, idy, index_in%3 + (i * width % (width*height)) + 3 * t);
  }
}

__global__
void reverseColumns3(int *in, int *out, int width, int height){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  if (idx < width && idy < height && idx % 3 == 0) {
    int reversed = abs(idx-(width-1))-2;
    int index_in = idy * width + idx;
    int index_out = idy * width  + reversed;
    out[index_out + 0] = in[index_in + 0];
    out[index_out + 1] = in[index_in + 1];
    out[index_out + 2] = in[index_in + 2];
  }
}

__global__
void reverseColumns32(int *in, int *out, int width, int height){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;

    int current_column = floor((double)idy/3);
    int output_column = (width/3) - current_column - 1;
    int output_y = 3 * output_column + (idy % 3);

    int index_out = idx * width + output_y;

    out[index_out] = in[index_in];
  }
}

__device__
void t(int *in, int *out, int idx, int idy, int width, int height){
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;
    int i = floor((double)index_in/3);
    int t = floor((double)(index_in/(height*3)));
    out[index_in] = in[index_in%3 + (i * width % (width*height)) + 3 * t];
  }
}
__device__
void r(int *in, int *out, int idx, int idy, int width, int height){
  if (idx < height && idy < width) {
    int index_in = idx * width + idy;
    int current_column = floor((double)idy/3);
    int output_column = (width/3) - current_column - 1;
    int output_y = 3 * output_column + (idy % 3);
    int index_out = idx * width + output_y;
    out[index_out] = in[index_in];
  }
}
__global__
void rotate(int *in, int *out, int width, int height){
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  t(in, out, idx, idy, width, height);
  __syncthreads();
  r(out, in, idx, idy, height*3, width/3);
  // IN TRANSPOSED TO OUT.
  // OUT FLIPPED TO IN.
  // FINAL OUTPUT WOULD ACTUALLY BE IN
}

int main(int argc, char **argv){
  // source and transpose matrixes.  For convenience we
  // declare these as 2D arrays, but remember that when
  // we copy to device, we are just interested in these
  // as contiguous blocks of N^2 integers, i.e. CUDA
  // kernel does not know about the 2D structure.
  // In general, to process multi-dimensional arrays
  // in CUDA kernels, it is better to flatten the array
  // and treat it in a linear fashion.
  int h_m[W*H], h_mt[W*H];

  // pointers for the matrices on the device.
  int *dev_m, *dev_mt;

  int matrix_sz = W * H;
  // Allocate arrays on the GPU.  Note the size of allocation N*N integers.
  CHECK( cudaMalloc( (void**)&dev_m, matrix_sz*sizeof(int)) );
  CHECK( cudaMalloc( (void**)&dev_mt, matrix_sz*sizeof(int)) );

  // Initialise the source array on the host.  For convenience
  // in testing we just initialise each entry to be its index
  // in a flattened array.
  for (int j = 0; j < H; j++) {
    for (int i = 0; i < W; i++) {
      h_m[j*W+i] = j*W+i;
      printf( "%d\t", h_m[j*W+i]);
    }
    printf("\n");
  }
  printf("\n\n");

  // Copy the input array from host to device
  CHECK( cudaMemcpy( dev_m, h_m, matrix_sz*sizeof(int), cudaMemcpyHostToDevice) );

  // My GPU has max 1024 threads per block, so I'm specifying a square grid
  // of 32*32 = 1024.  The number of blocks allocated in each row/column of
  // the grid should then be the width of the input array divided by 32 (nr of
  // threads in a row/column) of a block.  However, if the array dimension
  // is not evenly divisible by the block widdth/height, we need to round
  // up.  The computation below ensures this.
  dim3 b_sz (32, 32);
  dim3 g_sz ((W+31)/32, (H+31)/32);
  printf("Grid : {%d, %d, %d} blocks. Blocks : {%d, %d, %d} threads.\n",g_sz.x, g_sz.y, g_sz.z, b_sz.x, b_sz.y, b_sz.z);

  // Perform reverse on the GPU
  // transpose3<<<g_sz,b_sz>>>( dev_m, dev_mt, W, H);
  // // Copy the results back from device to host array
  // CHECK( cudaMemcpy( h_mt, dev_mt, matrix_sz*sizeof(int), cudaMemcpyDeviceToHost) );
  // // Copy the input array from host to device
  // CHECK( cudaMemcpy( dev_m, h_mt, matrix_sz*sizeof(int), cudaMemcpyHostToDevice) );
  rotate<<<g_sz,b_sz>>>( dev_m, dev_mt, W, H);
  // Copy the results back from device to host array
  CHECK( cudaMemcpy( h_mt, dev_m, matrix_sz*sizeof(int), cudaMemcpyDeviceToHost) );

  // Output result - of course oly useful for small values of N.
  // TRANSPOSE PRINT OUT
  // for (int j = 0; j < W/3; j++){
  //   for (int i = 0; i < H*3; i++)
  //     printf( "%d\t", h_mt[j*H+i]);
  //   printf("\n");
  // }


  // COMPLETED PRINT OUT
  for(int i = 0; i < W*H; i++){
    if(i % (H*3) == 0){
      printf("\n");
    }
    printf("%d\t", h_mt[i]);
  }

  // REVERSE COLUMNS PRINT OUT
  // for (int j = 0; j < H; j++){
  //   for (int i = 0; i < W; i++)
  //     printf( "%d\t", h_mt[j*W+i]);
  //   printf("\n");
  // }


  // Clean up device.
  cudaFree( dev_m );
  cudaFree( dev_mt );

  return 0;
}
