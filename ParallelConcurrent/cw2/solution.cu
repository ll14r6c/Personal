
#include <stdio.h>

#define CHECK(e) { int res = (e); if (res) printf("CUDA ERROR %d\n", res); }

#define CHANNEL 3

struct Image {
  int width;
  int height;
  unsigned int bytes;
  unsigned char *img;
  unsigned char *dev_img;
};

// Kernel to transpose a single r, g or b value
// Parameters:
//  in =  array of unsigned char containing the input image data
//  out = blank array of unsigned char to store transpose of in
//  width = width of the in image
//  height = height of the in image
__global__
void transpose(unsigned char *in, unsigned char *out, int width, int height){
  // Calcualte the location
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  // Ensure the location is within the image limits
  if (idx < height && idy < width) {
    // Calcualte the array out index
    int index_out = idx * width + idy;
    // Calculate if its the r, g or b value
    int i = floor((double)index_out/CHANNEL);
    // Calculate the transposed output row
    int t = floor((double)(index_out/(height*CHANNEL)));
    // Calculate the input index pixel which correspondes to its transpose
    int index_in = index_out%CHANNEL + (i * width % (width*height)) + CHANNEL * t;
    // Set the calculated input to the calculated output
    out[index_out] = in[index_in];
  }
}

// Kernal to reverse the columns of a given image (vertical flip)
// Parameters:
//  in =  array of unsigned char containing the input image data
//  out = blank array of unsigned char to store reverse of in
//  width = width of the in image
//  height = height of the in image
__global__
void reverseColumns(unsigned char *in, unsigned char *out, int width, int height){
  // Calcualte the location
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int idy = blockIdx.y * blockDim.y + threadIdx.y;
  // Ensure the location is within the image limits
  if (idx < height && idy < width) {
    // Calcualte the array out index
    int index_in = idx * width + idy;
    // Calcuate the values current column
    int current_column = floor((double)idy/CHANNEL);
    // Calculate the values output column
    int output_column = (width/CHANNEL) - current_column - 1;
    // Calculate the values new y value
    int output_y = CHANNEL * output_column + (idy % CHANNEL);
    // Calcualte the values new value within the array
    int index_out = idx * width + output_y;
    // Set the calculated input to the calculated output
    out[index_out] = in[index_in];
  }
}


// Reads a color PPM image file (name provided), and
// saves data in the provided Image structure.
// The max_col_val is set to the value read from the
// input file. This is used later for writing output image.
int readInpImg (const char * fname, Image & source, int & max_col_val){

  FILE *src;

  if (!(src = fopen(fname, "rb")))
  {
      printf("Couldn't open file %s for reading.\n", fname);
      return 1;
  }

  char p,s;
  fscanf(src, "%c%c\n", &p, &s);
  if (p != 'P' || s != '6')   // Is it a valid format?
  {
      printf("Not a valid PPM file (%c %c)\n", p, s);
      exit(1);
  }

  fscanf(src, "%d %d\n", &source.width, &source.height);
  fscanf(src, "%d\n", &max_col_val);

  int pixels = source.width * source.height;
  source.bytes = pixels * 3;  // 3 => colored image with r, g, and b channels
  source.img = (unsigned char *)malloc(source.bytes);
  if (fread(source.img, sizeof(unsigned char), source.bytes, src) != source.bytes)
    {
       printf("Error reading file.\n");
       exit(1);
    }
  fclose(src);
  return 0;
}

// Write a color image into a file (name provided) using PPM file format.
// Image structure represents the image in the memory.
int writeOutImg(const char * fname, const Image & roted, const int max_col_val){

  FILE *out;
  if (!(out = fopen(fname, "wb")))
  {
      printf("Couldn't open file for output.\n");
      return 1;
  }
  fprintf(out, "P6\n%d %d\n%d\n", roted.width, roted.height, max_col_val);
  if (fwrite(roted.img, sizeof(unsigned char), roted.bytes , out) != roted.bytes)
  {
      printf("Error writing file.\n");
      return 1;
  }
  fclose(out);
  return 0;
}



int main(int argc, char **argv)
{

  if (argc != 2)
  {
      printf("Usage: exec filename\n");
      exit(1);
  }
  char *fname = argv[1];

  //Read the input file
  Image source;
  int max_col_val;
  if (readInpImg(fname, source, max_col_val) != 0)  exit(1);


  // Complete the code
  unsigned char *dev_temp_img;
  CHECK( cudaMalloc( (void**)&dev_temp_img, source.bytes*sizeof(unsigned char)) );
  CHECK( cudaMalloc( (void**)&source.dev_img, source.bytes*sizeof(unsigned char)) );

  // Copy the input array from host to device
  CHECK( cudaMemcpy( source.dev_img, source.img, source.bytes*sizeof(unsigned char), cudaMemcpyHostToDevice) );

  // Given I have 1024 max threads per block,
  // Ensure the array dimensions are easily divisible
  dim3 g_sz ((source.width+31)/32, ((source.width*CHANNEL)+31)/32);
  dim3 b_sz (32, 32);


  // Start a timing event
  cudaEvent_t start, stop;
  cudaEventCreate (&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0); // arg 0 refers to CUDA stream

  // Perform 90 degree rotation by transpose then reorder

  // Transpose the img to dev_img
  transpose<<<g_sz,b_sz>>>( source.dev_img, dev_temp_img, source.width*CHANNEL, source.height);
  // Reverse the columns from dev_img back to img
  reverseColumns<<<g_sz,b_sz>>>( dev_temp_img, source.dev_img, source.height*CHANNEL, source.width);

  // Copy the results back from device to host array
  CHECK( cudaMemcpy( source.img, source.dev_img, source.bytes*sizeof(unsigned char), cudaMemcpyDeviceToHost) );

  // Stop the timing event and print the time
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop); // all work prior to stop is done.
  float t;
  cudaEventElapsedTime(&t, start, stop);
  cudaEventDestroy(start); // clean-up.
  cudaEventDestroy(stop);
  printf("Elapsed Time: %f\n", t);

  // Swap the width and height
  int temp;
  temp = source.width;
  source.width = source.height;
  source.height = temp;

  // Write the output file
  if (writeOutImg("roted.ppm", source, max_col_val) != 0) // For demonstration, the input file is written to a new file named "roted.ppm"
   exit(1);

   // Free the image arrays on host and device
  free(source.img);
  cudaFree( source.dev_img );
  cudaFree( dev_temp_img );

  exit(0);
}
